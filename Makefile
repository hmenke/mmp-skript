LATEX = dvilualatex -enable-write18 -shell-escape -interaction=nonstopmode
PDFLATEX = lualatex -enable-write18 -shell-escape -interaction=nonstopmode
DVIPS = dvips
PS2PDF = ps2pdf
BIBER = biber
INDEX = makeindex
VIEWER = xdg-open

A5 = 
A4 = MMP-a4-skript

all: MMP-a5-skript MMP-a4-skript MMP-a5-booklet

quick:
	$(LATEX) "MMP-a5-skript.tex"
	$(DVIPS) "MMP-a5-skript.dvi"
	$(PS2PDF) "MMP-a5-skript.ps"

MMP-a5-skript:
	$(LATEX) "MMP-a5-skript.tex"
	$(INDEX) "MMP-a5-skript.idx"
	$(LATEX) "MMP-a5-skript.tex"
	$(LATEX) "MMP-a5-skript.tex"
	$(DVIPS) "MMP-a5-skript.dvi"
	$(PS2PDF) "MMP-a5-skript.ps"
	7z a -tgzip -mx=9 "MMP-a5-skript.ps.gz" "MMP-a5-skript.ps"

MMP-a4-skript:
	$(LATEX) "MMP-a4-skript.tex"
	$(INDEX) "MMP-a4-skript.idx"
	$(LATEX) "MMP-a4-skript.tex"
	$(LATEX) "MMP-a4-skript.tex"
	$(DVIPS) "MMP-a4-skript.dvi"
	$(PS2PDF) "MMP-a4-skript.ps"

MMP-a5-booklet: MMP-a5-skript
	$(PDFLATEX) "MMP-a5-booklet.tex"

view:
	$(VIEWER) "MMP-a5-skript.pdf" &

# All clean commands will preserve output
.PHONY : clean
clean:
	$(RM) *.dvi *.ps

distclean:
	$(RM) *.aux *.bbl *.bcf *.blg *.dvi *.lof *.log *.fdb_latexmk *.fls *.idx *.ilg *.ind *.out *.ps *.run.xml *.thm *.toc
