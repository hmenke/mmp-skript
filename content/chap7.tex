% Holger Cartarius, Henri Menke, 2012 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

\chapter{Differential- und Integralrechung für Funktionen mehrer Veränderlicher}
\index{Funktionen mehrer Veränderlicher|(}

Die Differential- und Integralrechung für Funktionen \emph{mehrer Veränderlicher} unterscheidet sich gegenüber der herkömmlichen Infinitesimalrechnung im Hinblick auf die Anzahl der Variablen und den damit verbundenen Besonderheiten und veränderten Regeln.

\section{Differentialrechnung}
\index{Funktionen mehrer Veränderlicher!Differntialrechnung|(}
\subsection{Partielle Ableitung}
\index{Funktionen mehrer Veränderlicher!Differntialrechnung!Partielle Ableitung}
\subsubsection{Einführung}

Die partielle Ableitung ist die Verallgemeinerung von \eqref{eq:1.3} und hat für eine Funktion $f(x_1 , \ldots , x_n)$ die Form
\begin{align}
  \frac{\partial f}{\partial x_i} = \lim\limits_{x_i \rightarrow \infty} \frac{f(x_1 , \ldots , x_i + \Delta x_i, \ldots , x_n) - f(x_1 , \ldots , x_i, \ldots , x_n)}{\Delta x_i}
  \label{eq:7.1}
\end{align}
Sie entspricht der Ableitung \eqref{eq:1.3}, wenn wir alle anderen $x_j$ festhalten (als konstant betrachten).

\textbf{Beispiel:}
\begin{align}
  f(x,y,z) = f(\underline{r}) = \sqrt{x^2 + y^2 + z^2} = | \underline{r} | \nonumber\\
  \frac{\partial f}{\partial y} = \frac{1}{2} \frac{2y}{\sqrt{x^2 + y^2 + z^2}} = \frac{y}{| \underline{r} |}
  \label{eq:7.2}
\end{align}

Es gibt mehrere Schreibweisen:
\begin{align}
  \frac{\partial f}{\partial x_i} = \partial_{x_{i}} f = f_{x_{i}}
  \label{eq:7.3}
\end{align}
oder $\left( \frac{\partial f}{\partial x} \right)_{y}$, heißt: Ableitung von $f$ nach $x$, wobei $y$ festgehalten wird.

Auch höhere Ableitungen folgen dem Schema \eqref{eq:7.1}.

\textbf{Beispiel:}
\begin{align*}
  f(x,y) &= \sqrt{x^2 + y^2} \\
  \frac{\partial f}{\partial x} &= \frac{x}{\sqrt{x^2 + y^2}} \ , \ \frac{\partial f}{\partial y} = \frac{y}{\sqrt{x^2 + y^2}} \\
  \frac{\partial^2 f}{\partial x^2} &= \frac{1}{\sqrt{x^2 + y^2}} - \frac{1}{2} \frac{x  2 x}{\sqrt{x^2 + y^2}^3} = \frac{y^2}{\sqrt{x^2 + y^2}^3} \\
  \frac{\partial^2 f}{\partial y^2} &= \frac{x^2}{\sqrt{x^2 + y^2}^3} \\
  \frac{\partial^2 f}{\partial x \partial y} &= \frac{\partial^2 f}{\partial y \partial x} = -\frac{xy}{\sqrt{x^2 + y^2}^3}
\end{align*}
Die Reihenfolge der partiellen Ableitungen lässt sich immer vertauschen!

\subsubsection{Kettenregel und totale Ableitung}
Betrachte eine Funktion $f(x,y,t)$, wobei $x$ und $y$ selbst wieder von $t$ anbhängen.
\begin{align*}
  x = x(t) \ , \ y=y(t)
\end{align*}
Dann:
\begin{align}
  &\frac{\dx{}}{\dx{t}} f(x(t), y(t), t) \nonumber\\
  = & \frac{\partial f}{\partial x} \frac{\dx{x}}{\dx{t}} + \frac{\partial f}{\partial y} \frac{\dx{y}}{\dx{t}} + \frac{\partial f}{\partial t}
  \label{eq:7.4}
\end{align}
\eqref{eq:7.4} heißt \emph{totale Ableitung}. \index{Funktionen mehrer Veränderlicher!Differntialrechnung!Totale Ableitung}

\textbf{Beispiel:}
\begin{align*}
  f(x,y) &= x\ e^{-y} \ \text{ mit } x=1+t,\ y=t^3 \\
  \frac{\dx{}}{\dx{t}} f(x(t), y(t)) &\overset{\eqref{eq:7.4}}{=} \underbrace{\frac{\partial f}{\partial x}}_{e^{-y}} \underbrace{\frac{\dx{x}}{\dx{t}}}_{1} + \underbrace{\frac{\partial f}{\partial y}}_{-x\ e^{-y}} \underbrace{\frac{\dx{y}}{\dx{t}}}_{3t^2} + \underbrace{\frac{\partial f}{\partial t}}_{0} \\
  &= e^{-y(t)} - 3t^2\ x(t)\ e^{-y(t)} \\
  &= (-3t^3 - 3t^2 + 1)\ e^{-t^3}
\end{align*}

\subsubsection{Zum Unterschied zwischen partieller und totaler Ableitung}

Die Definition besagt:
%
\begin{align*}
  \frac{\partial}{\partial x} f(x,y) &= \lim\limits_{\varepsilon \rightarrow 0} \frac{f(x+\varepsilon,y) - f(x,y)}{\varepsilon}
\end{align*}
%
Alle Variablen außer $x$ werden in diesem Fall festgehalten. Das gilt auch, wenn $y$ selbst wieder von $x$ abhängt:
%
\begin{align*}
  \frac{\partial}{\partial x} f(\underbrace{x}_{\mathclap{\text{Wirkt nur auf dieses $x$}}},\overbrace{y(x)}^{\mathclap{\text{muss festgehalten werden}}})
\end{align*}
%
Warum ist das so?

Das ist die \textit{Definition} der partiellen Ableitung. Sie wirkt nur auf die expliziten Ab\-hän\-gig\-keit\-en. Die totale Ableitung wirkt auf die expliziten \textit{und} impliziten Beiträge:

\begin{minipage}{0.3\textwidth}
\begin{align*}
  \frac{\partial}{\partial x} f({\color{purple} x(t)},t) \\
  \frac{\partial}{\partial t} f(x(t), {\color{purple} t}) \\
  \frac{\dx{}}{\dx{t}} f({\color{purple} x(t)}, {\color{purple} t})
\end{align*}
\end{minipage}
\vline
\hspace*{1em}
\begin{minipage}{0.6\textwidth}
\begin{align*}
  &\ \frac{\dx{}}{\dx{t}} \Bigg( \frac{\partial^2}{\partial x \partial t} \underbrace{\frac{\exp(t\ x(t))}{t}}_{f(x(t),t)} \Bigg) \\
  = &\ \frac{\dx{}}{\dx{t}} \left( \frac{\partial}{\partial t} \exp(t\ x(t)) \right) \\
  = &\ \frac{\dx{}}{\dx{t}} \left( x(t)\ \exp(t\ x(t)) \right) \\
  = &\ \dot{x}(t)\ \exp(t\ x(t)) + x(t)\ \exp(t\ x(t)) (x(t)+ t\ \dot{x}(t))
\end{align*}
\end{minipage}


\subsection{Differentiale}
\subsubsection{Totales Differential}
Wir betrachten eine Funktion $f(x, y)$ unter kleinen Änderungen $\Delta x$ und $\Delta y$. Dann reicht eine lineare Näherung.
\begin{align*}
  \Delta t &= f(x+\Delta x, y+\Delta y) - f(x,y) \\
  &= f(x+\Delta x, y+\Delta y) - f(x, y+\Delta y) + f(x, y+\Delta y) - f(x,y) \\
  &= \frac{f(x+\Delta x, y+\Delta y) - f(x, y+\Delta y)}{\Delta x} \Delta x + \frac{f(x, y+\Delta y) - f(x,y)}{\Delta y} \Delta y \\
  & \overset{\eqref{eq:7.1}}{\approx} \frac{\partial f}{\partial x} \Delta x + \frac{\partial f}{\partial y} \Delta y
\end{align*}
Im Grenzübergang $\Delta x \rightarrow 0$, $\Delta y \rightarrow 0$ schreibt man:
\begin{align}
  \dx{f} = \frac{\partial f}{\partial x} \dx{x} + \frac{\partial f}{\partial y} \dx{y}
  \label{eq:7.5}
\end{align}
$\dx{f}$ heißt \emph{totales Differential}. \index{Funktionen mehrer Veränderlicher!Differntialrechnung!Totales Differential}

In mehreren Dimensionen allgemein:
\begin{align}
  \dx{f} = \sum\limits_{i=1}^{n} \frac{\partial f}{\partial x_i} \dx{x}_i
  \label{eq:7.6}
\end{align}

\subsubsection{Vollständiges Differential}
\index{Funktionen mehrer Veränderlicher!Differntialrechnung!Vollständiges Differential}

\begin{subequations}
In Verallgemeinerung von \eqref{eq:7.6} nennt man jede Form:
\begin{align}
  \dx{f} = \sum\limits_{i=1}^{n} a_i (x_1, \ldots , x_n) \dx{x}_i
  \label{eq:7.7a}
\end{align}
ein Differential.

Es heißt \emph{vollständig} oder \emph{exakt}, wenn es eine Funktion $f(x_1, \ldots , x_n)$ so gibt, dass
\begin{align}
  a_i = \frac{\partial f}{\partial x_i}
  \label{eq:7.7b}
\end{align}

Notwendige Bedingung für ein vollständiges Differential ist:
\begin{align}
  \frac{\partial a_i}{\partial x_j} = \frac{\partial a_j}{\partial x_i}
  \label{eq:7.7c}
\end{align}
\end{subequations}

\textbf{Beispiele:}
%\begin{align*}
%  y_1 &= y \dx{x} + x \dx{y} \text{ ist vollständiges Differential} \\
%  y_2 &= y \dx{x} +3x \dx{y}
%\end{align*}

$y_1 = y \dx{x} + x \dx{y}$ ist ein vollständiges Differential, denn es existiert:
\begin{align*}
  f(x,y) = x  y + c
\intertext{sodass}
  \frac{\partial f}{\partial x} = y\ \text{ und }\ \frac{\partial f}{\partial y} = x
\end{align*}

$y_2 = y \dx{x} +3x \dx{y}$ ist kein vollständiges Differential.

\subsubsection{Variablentransformation}
\index{Funktionen mehrer Veränderlicher!Differntialrechnung!Variablentransformation}

Haben wir eine Funktion $f(x_1, \ldots , x_n)$ und wollen die Variablen $x_i$ durch neue $u_j$ ausdrücken gilt:
\begin{align*}
  x_i = x_i (u_1, \ldots , u_n)
\end{align*}

Was passiert mit der Ableitung?
\begin{align}
  \frac{\partial f}{\partial u_j} = \sum\limits_{i=1}^{n} \frac{\partial f}{\partial x_i} \frac{\partial x_i}{\partial u_j} = \sum\limits_{i=1}^{n} \left( \frac{\partial x_i}{\partial u_j} \right) \frac{\partial f}{\partial x_i}
  \label{eq:7.8}
\end{align}

\textbf{Beispiel:} In 2 Dimensionen transformieren wir kartesische Koordinaten in ebene Polarkoordinaten:
\begin{align*}
  x &= r\ \cos(\varphi) \\
  y &= r\ \sin(\varphi) \\
  r &= \sqrt{x^2 + y^2} \\
  \varphi &= \arctan\left(\frac{y}{x}\right)
\end{align*}

\begin{figure}[H]
  \centering
  \begin{pspicture}(0,0)(6,3.5)
    \psaxes[labels=none,labelFontSize=\scriptstyle\color{gdarkgray} ,xAxis=true,yAxis=true,Dx=1,Dy=1,ticksize=0,subticks=1]{->}(0,0)(0,0)(6,3.5) %[$\color{gdarkgray} x$,-90][$\color{gdarkgray} y$,0]
    \psline[linecolor=darkblue](0,0)(5,2.8)
    \psline[linestyle=dotted](0,2.8)(5,2.8)
    \psline[linestyle=dotted](5,0)(5,2.8)
    \psarc[linecolor=redorange](0,0){2}{0}{29}
    \psdots[dotscale=1.3,fillcolor=black](5,2.8)
    \uput[0](1,0.4){$\color{redorange} \varphi$}
    \uput[90](3,2){$\color{darkblue} r$}
    \uput[180](0,2.8){$\color{gdarkgray} y$}
    \uput[-90](5,0){$\color{gdarkgray} x$}
  \end{pspicture}
\end{figure}

Dann ist z.B. $(f(x(r,\varphi)),y(r,\varphi))$
\begin{align*}
  \frac{\partial f}{\partial r} &\overset{\eqref{eq:7.8}}{=} \left( \frac{\partial x}{\partial r} \right) \frac{\partial f}{\partial x} + \left( \frac{\partial y}{\partial r} \right) \frac{\partial f}{\partial y} = \cos\varphi \frac{\partial f}{\partial x} + \sin\varphi \frac{\partial f}{\partial y} \\
  &= \frac{x}{\sqrt{x^2+y^2}} \frac{\partial f}{\partial x} + \frac{y}{\sqrt{x^2+y^2}} \frac{\partial f}{\partial y}
\end{align*}

\index{Funktionen mehrer Veränderlicher!Differntialrechnung|)}

\section{Integralrechnung}
\index{Funktionen mehrer Veränderlicher!Integralrechnung|(}

\subsection{Definition des mehrdimensionalen Integrals}

Wir haben das $1$-dimensionale Integral als Grenzwertprozess eingeführt:
%
\begin{align*}
  \int\limits_{a}^{b} f(x) \dx{x} = \lim\limits_{n \rightarrow \infty} \sum\limits_{i=1}^{n} f(x_i) \Delta x \text{ ,\ mit } \Delta x = \frac{b-a}{n}
  \tag*{\eqref{eq:1.14}}
\end{align*}
%
Ganz analog können wir nun bei einer Funktion vorgehen, die von zwei Variablen abhängt: $f(x,y)$

\begin{figure}[H]
  \centering
  \begin{pspicture}(-1,-1)(4,3)
    \psaxes[labels=none,labelFontSize=\scriptstyle\color{gdarkgray} ,xAxis=true,yAxis=true,Dx=1,Dy=1,ticksize=0,subticks=1]{->}(0,0)(-1,-1)(4,3)[$\color{gdarkgray} x$,-90][$\color{gdarkgray} y$,0]
    \psbezier(0.74878585,1.3859484)(0.74878585,0.58594835)(1.2169458,0.4184597)(2.2087858,0.5459484)(3.200626,0.673437)(3.8093395,0.88611)(3.528786,1.8459483)(3.2482321,2.8057866)(3.179247,1.884691)(2.2087858,2.1259484)(1.2383246,2.3672059)(1.6226894,3.1069484)(0.80878586,2.5259483)(-0.0051177037,1.9449483)(0.77507436,2.3856027)(0.74878585,1.3859484)
    \psframe[dimen=outer](1.5,1.5)(2,1)
    \psline[linestyle=dotted](1.8,1.3)(3.8,2.1)
    \uput[0](3.8,2.1){$\color{gdarkgray} \Delta A$}
    \psline[linestyle=dotted](1.5,2)(2.5,2.8)
    \uput[45](2.5,2.8){$\color{gdarkgray} A$}
  \end{pspicture}
\end{figure}

Wir greifen kleine Flächenelemente $\Delta A$ heraus und summieren über diese:
%
\begin{align}
  I = \int \dx{x} \dx{y} f(x,y) = \lim\limits_{n \rightarrow \infty} \sum\limits_{n} f(x_n,y_n) \Delta A_n, \label{eq:7.9 }
\end{align}
%
wobei der Punkt $(x_n,y_n)$ in $\Delta A$ liegen soll.

Existiert der Grenzwert und ist er unabhängig von der Wahl eines  $(x_n,y_n)$ in $\Delta A$, so existiert das Integral \eqref{eq:7.9 }.

Analog können wir weitere Dimensionen hinzufügen, z.B. ein \emph{Volumenintegral} $\Delta V_n$.

\subsection{Berechnung}
\index{Funktionen mehrer Veränderlicher!Integralrechnung!Berechnung|(}

Die praktische Berechnung erfolgt nicht über beliebige Flächenelemente $\Delta A_n$, sondern über geschickt gewählte Reihenfolgen. In zwei Dimensionen kann das folgende Vorgehen gewählt werden:

\begin{figure}[H]
  \centering
  \begin{pspicture}(-2,-1.5)(6.5,3.5)
    \psaxes[labels=none,ticks=none]{->}(0,0)(-2,-1.5)(6.5,3.5)[$\color{gdarkgray} x$,-90][$\color{gdarkgray} y$,0] %,ticks=none
    \psline[linestyle=dotted](-1.05,-0.1)(-1.05,2)
    \psline[linestyle=dotted](5.15,-0.1)(5.15,2)
    \psline[linestyle=dotted](-1,2.7)(3,2.7)
    \psline[linestyle=dotted](-0.5,-1.1)(5,-1.1)
    \uput[115](-0.2,2.7){\color{gdarkgray} \scriptsize $y_{\text{max}}$}
    \uput[-115](-0.2,-1){\color{gdarkgray} \scriptsize $y_{\text{min}}$}
    \uput[-115](-1.05,0){\color{redorange} \scriptsize $x_1(y)$}
    \uput[-60](5.15,0){\color{redorange} \scriptsize $x_2(y)$}
    \pspolygon[linecolor=redorange,fillstyle=hlines,hatchcolor=redorange](-1,0.5)(-1,1)(5.1,1)(5,0.5)
    \uput[0](5,0.75){\color{redorange} \scriptsize $\Big\rbrace \dx{y}$ }
    \psline[linecolor=darkblue](-0.9,1.5)(5,1.5)
    \uput[0](0.5,1.25){\color{darkblue} \scriptsize 2. Streifen}
    \psline[linecolor=darkblue](-0.65,2)(4.6,2)
    \uput[0](0.5,1.75){\color{darkblue} \scriptsize 3. Streifen}
    \psccurve(-1,1)(-0.5,-0.5)(1,-0.8) %(2.5,-0.25)
             (4,-0.8)(5,0.5)(5,1.5)(3.5,2.5) %(2,2.2)
             (1,2.6)(-0.15,2.5)
  \end{pspicture}
\end{figure}

\begin{itemize}
  \item Summiere (bzw. Integriere) zuerst über einen Streifen parallel zur $x$-Achse, d.h. bei festem $y$. \emph{Achtung:} die Grenzen hängen vom gewählten $y$ ab. Dies ist ein eindimensionales Integral, das wir mit den bekannten Methoden berechnen können.
  \item Summiere dann über alle Streifen $y$, dies ist jetzt auch ein eindimensionales Integral.
\end{itemize}

In einer Gleichung können wir das folgendermaßen ausdrücken:
%
\begin{align*}
  I = \underbrace{\int\limits_{y_{\text{min}}}^{y_{\text{max}}} \underbrace{\left[ \int\limits_{x_1(y)}^{x_2(y)} f(x,y) \dx{x} \right]}_{\parbox{9em}{\footnotesize Streifen parallel zur $x$-Achse bei festem $y$, übrig bleibt eine Funktion, die nur noch von $y$ abhängt.}} \dx{y}}_{\parbox{12em}{\footnotesize Integration über alle Streifen für das Integral, das nur noch von $y$ abhängt.}}
\end{align*}
%
Die Reihenfolge ist nicht entscheidend, man kann auch erst Streifen parallel zur $y$-Achse wählen.

In mehreren Dimension funktioniert das völlig analog: Erst Streifen bilden, dann die Streifen zur Fläche aufsummieren, dann die Fläche zum Volumen aufsummieren, usw.

\subsection{Beispiele}

\subsubsection{Dreieck}

Wir berechnen die Fläche dieses Dreiecks:

\begin{figure}[H]
  \centering
  \begin{pspicture}(-1,-1)(3,3.2)
    \pspolygon[fillstyle=hlines,linecolor=redorange,hatchcolor=redorange](0,0)(0,2)(2,0)
    \psaxes[labels=none,labelFontSize=\scriptstyle\color{gdarkgray} ,xAxis=true,yAxis=true,Dx=2,Dy=2,ticksize=-2pt 2pt,subticks=1]{->}(0,0)(-1,-1)(3,3)[$\color{gdarkgray} x$,-90][$\color{gdarkgray} y$,180]
    \psline[linecolor=redorange](-1,3)(3,-1)
    \uput[180](0,2){$\color{gdarkgray} \scriptstyle 1$}
    \uput[-90](2,0){$\color{gdarkgray} \scriptstyle 1$}
    \uput[45](1,1){$\color{redorange} \scriptstyle y(x) = 1 - x$}
  \end{pspicture}
  \vspace*{-2em}
\end{figure}
%
\begin{align*}
  A &= \int_A 1 \dx{x} \dx{y} = \int\limits_{0}^{1} \dx{x} \int\limits_{0}^{y(x)} \dx{y} \ 1 \\
    &= \int\limits_{x=0}^{1} \int\limits_{y=0}^{y(x)} 1 \dx{x}  \dx{y} = \int\limits_{0}^{1} \left[ y \right]_{0}^{y(x)} \dx{x} \\
    &= \int\limits_{x=0}^{1} y(x) \dx{x} = \int\limits_{0}^{1} (1-x) \dx{x} = \left[ x - \frac{1}{2} x^2 \right]_{0}^{1} = \frac{1}{2}
\end{align*}
%
Integriere $xy^2$ über diese Fläche: {\color{gray}(Das ist völlig exemplarisch und hat keinerlei praktische Anwendung)}
%
\begin{align*}
  I &= \int\limits_{0}^{1} \dx{x} \int\limits_{0}^{1-x} \dx{y} \ xy^2 \\
    &= \dfrac{1}{3} \int\limits_{0}^{1} \dx{x}\ x \left[ y^3 \right]_{0}^{1-x} \\
    &= \dfrac{1}{3} \int\limits_{0}^{1} \dx{x}\ x (1-x)^3 \\
    &= -\frac{1}{12} \underbrace{\left[ x(1-x)^4 \right]_{0}^{1}}_{=0} + \frac{1}{12} \int\limits_{0}^{1} (1-x)^4 \dx{x} \\
    &= -\frac{1}{60} \left[ (1-x)^5 \right]_{0}^{1} = \frac{1}{60}
\end{align*}
%

\subsubsection{Kegel}

Wir berechnen das Volumen des Kegels:

\begin{figure}[H]
  \centering
  \psset{Alpha=45}
  \begin{pspicture}(-2,-1)(2,4)
    \pstThreeDCoor[linecolor=gdarkgray,xMax=2,yMax=2,zMax=4,nameX=$\color{DimGray} x$,nameY=$\color{DimGray} y$,nameZ=$\color{DimGray} z$]
    \pstThreeDCircle[linecolor=darkblue](0,0,0)(1,0,0)(0,1,0)
    \pstThreeDLine[linecolor=redorange](0,0,0)(-0.707,0.707,0)
    \pstThreeDLine[linecolor=redorange](0,0,1)(-0.47,0.47,1)
    \pstThreeDLine[linecolor=darkblue](-0.707,0.707,0)(0,0,3)
    \pstThreeDLine[linecolor=darkblue](0.707,-0.707,0)(0,0,3)
    \pstThreeDPut(-0.8,0.8,0.2){$\color{redorange} \scriptstyle R$}
    \pstThreeDPut(-0.8,0.8,1){$\color{redorange} \scriptstyle r(z)$}
  \end{pspicture}
  \vspace*{-2em}
\end{figure}
%
Das Volumen ist:
\begin{align*}
  V &= \iiint_{V} 1 \dx{x} \dx{y} \dx{z}
\intertext{Wahl der Grenzen}
  z &: \quad z_{\text{min}} = 0 \; , \quad z_{\text{max}} = h
\intertext{$x$ und $y$: Für jedes $z$ ist die Fläche $(x,y)$ ein Kreis mit dem Radius}
  r(z) &= R \left( 1-\frac{z}{h} \right)
\end{align*}
In dieser Ebene:
%
\begin{figure}[H]
  \centering
  \begin{pspicture}(-1.5,-1.5)(1.5,1.5)
    \psaxes[labels=none,labelFontSize=\scriptstyle\color{gdarkgray} ,xAxis=true,yAxis=true,Dx=2,Dy=2,ticksize=-2pt 2pt,subticks=1]{->}(0,0)(-1.5,-1.5)(1.5,1.5)[$\color{gdarkgray} x$,-90][$\color{gdarkgray} y$,0]
    \pscircle[linecolor=darkblue](0,0){0.8}
    \psline[linecolor=redorange](0,0)(0.707,0.707)
    \uput[45](0.707,0.707){$\color{gdarkgray} \scriptstyle r(z)$}
  \end{pspicture}
  \vspace*{-3em}
\end{figure}
%
\begin{align*}
  y_{\text{min}} &= -r(z) \ ,\ \quad y_{\text{max}} = r(z)
\intertext{Aus der Kreisgleichung: $x^2 + y^2 = r^2$}
  x_{\text{min}} &= - \sqrt{r(z)^2 - y^2} \; , \quad x_{\text{max}} = \sqrt{r(z)^2 - y^2} \\
  V &= \int\limits_{0}^{h} \dx{z} \int\limits_{-r(z)}^{r(z)} \dx{y} \int\limits_{-\sqrt{r(z)^2 - y^2}}^{\sqrt{r(z)^2 - y^2}} \dx{x} \ 1 = \ldots
\end{align*}

\subsubsection{Kugel}
%
\begin{align*}
  V &= \int\limits_{-R}^{R} \dx{z} \int\limits_{-\sqrt{R^2 - z^2}}^{\sqrt{R^2 - z^2}} \dx{y} \int\limits_{-\sqrt{R^2 - z^2 - y^2}}^{\sqrt{R^2 - z^2 - y^2}} \dx{x} \ f(x,y,z) = \ldots
\intertext{In Kugelkoordinaten:}
  x &= r \sin\theta \cos\varphi \\
  y &= r \sin\theta \sin\varphi \\
  z &= r \cos\theta
  \\
  V &= \int\limits_{0}^{R} \dx{r} \int\limits_{0}^{\pi} \dx{\theta} \int\limits_{0}^{2\pi} \dx{\varphi} \ \sin\theta r^2 f(r,\theta,\varphi) = \ldots
\end{align*}

\index{Funktionen mehrer Veränderlicher!Integralrechnung!Berechnung|)}
\index{Funktionen mehrer Veränderlicher!Integralrechnung|)}
\index{Funktionen mehrer Veränderlicher|)}
