% Holger Cartarius, Henri Menke, 2012 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

\chapter{\texorpdfstring{$\delta$}{Delta}-Funktion}
\index{$\delta$-Funktion|(}

Die \emph{$\delta$-Funktion} ist eigentlich gar keine Funktion, sondern eine spezielle irreguläre Distribution mit kompaktem Träger, die in der Mathematik und Physik von grundlegender Bedeutung ist.

\section{Auftreten und Definition}
Die Dirac'sche-$\delta$-Funktion hat viele Anwendungen in der Physik, z.B.
\begin{itemize}
  \item Hilfsgröße beim Rechnen, vgl. \autoref{sec:Greensche Funktion}
  \item Beschreibung von Stößen
  \item In Pseudo-Potentialen in der Quantenmechanik
\end{itemize}
Sie ist definiert über \index{$\delta$-Funktion!Definition}
%
\begin{align}
  \int\limits_{a}^{b} f(x) \delta(x-x_0) \dx{x} &=
  \begin{cases}
    f(x_0) & \text{falls } a < x_0 < b \\
    0 & \text{sonst}
  \end{cases}
  \label{eq:3.1} \\
  \delta(x-x_0) &= 0 \text{, falls } x \neq x_0. \nonumber
\end{align}
%
und sie ergibt erst in Zusammenhang mit einer Funktion $f(x)$ einen Sinn.

Die $\delta$-Funktion ist trotz ihres Namens keine Funktion sondern eine Distribution. Distributionen sind stetige, lineare Funktionale auf dem Raum $\mathcal{D}$ der Testfunktion, das heißt sie sind Abbildungen:
\begin{align}
  T: \mathcal{D} \rightarrow \mathbb{R}\ ;\ \alpha f +\beta g \mapsto T(\alpha f +\beta g) = \alpha T(f) +\beta T(g) \in \mathbb{R} \label{eq:3.2}
\end{align}
Dabei enthält $\mathcal{D}$ die Funktion $f$, deren Träger kompakt ist und die beliebig oft differenzierbar sind $(f \in C^\infty)$.

Zusammenhang mit der $\delta$-Funktion: $T_\delta(f)=\int\limits f(x) \delta(x-x_0) \dx{x} =f(x_0)$, das heißt die Funktion $f$ wird abgebildet auf ihren Wert bei $x=x_0$.

\section{Approximation der \texorpdfstring{$\delta$}{Delta}-Funktion}
\index{$\delta$-Funktion!Approximationen}
Die $\delta$-Funktion lässt sich nur über \eqref{eq:3.1} angeben. Man kann keine explizite Funktion nennen. Es gibt aber Approximationen, zum Beispiel:
\begin{subequations}
\begin{align}
  \delta_n(x) &= \begin{dcases} n & \text{falls }-\frac{1}{2n}<x<\frac{1}{2n} \\ 0 & \text{sonst} \end{dcases} \label{eq:3.3a} \\
  \delta_n(x) &= \frac{n}{\sqrt{\pi}} e^{-nx^2} \label{eq:3.3b} \\
  \delta_n(x) &= \frac{n}{\pi}\ \frac{1}{1+n^2 x^2} \label{eq:3.3c} \\
  \delta_n(x) &= \frac{\sin(nx)}{\pi x} \label{eq:3.3d}
\end{align}
\end{subequations}
Für alle Funktionen gilt:
\begin{align}
  \lim_{n \rightarrow \infty} \delta_n(0) \rightarrow \infty \nonumber \\
  \lim_{n \rightarrow \infty} \delta_n(x \neq 0) \rightarrow 0 \label{eq:3.4}
\end{align}
Die Grenzwerte existieren also nicht für $x=0$. Trotzdem kann man sie als Approximation der $\delta$-Funktion verstehen in dem Sinn, dass
\begin{align}
  \lim_{n \rightarrow \infty} \int\limits_{x_0-\varepsilon}^{x_0+\varepsilon} f(x) \delta_n(x-x_0) \dx{x} = f(x_0) \label{eq:3.5}
\end{align}

\textsc{Beweis} für \eqref{eq:3.3a}:
\begin{align*}
\intertext{Sei $a>\frac{1}{2n}$:}
  &\lim_{n \rightarrow \infty} \int\limits_{x_0-a}^{x_0+a} f(x) \delta_n(x) \dx{x} \\
  \overset{\eqref{eq:3.3a}}{=}& \lim_{n \rightarrow \infty} \int\limits_{x_0-\frac{1}{2n}}^{x_0+\frac{1}{2n}} n\ f(x) \dx{x} = \lim_{n \rightarrow \infty} n \left( F\left(x_0+\frac{1}{2n}\right) - F\left(x_0-\frac{1}{2n}\right)\right) \\
  \overset{\varepsilon = \frac{1}{n}}{=}& \lim_{\varepsilon \rightarrow 0} \frac{\left( F\left(x_0+\frac{\varepsilon}{2}\right) - F\left(x_0-\frac{\varepsilon}{2}\right)\right)}{\varepsilon} = F'(x_0) = f(x_0)
\end{align*}
Oft schreibt man: $\displaystyle \lim_{n \rightarrow \infty} \delta_n(x) = \delta(x)$.

Aber Vorsicht: Das setzt die Vertauschbarkeit von Integration und Grenzwert in \eqref{eq:3.5} vorraus! Der Grenzwert existiert aber nicht für $x=0$! Diese Schreibweise darf nicht wörtlich genommen werden. Sie steht symbolisch für \eqref{eq:3.5}.

Visualisierung der Approximation $\frac{n}{\sqrt{\pi}} e^{-nx^2}$ mit $n \in \lbrace{\color{darkgreen}1},{\color{darkblue}3},{\color{orangered}6}\rbrace$:
\begin{figure}[H]
  \centering
  \begin{pspicture}(-3.5,-0.5)(3.5,4.2)
    \psaxes[labelFontSize=\scriptstyle\color{gdarkgray} ,xAxis=true,yAxis=true,Dx=1,Dy=1,ticksize=2pt -2pt,subticks=1]{->}(0,0)(-3.5,-0.5)(3.5,4)[$\color{gdarkgray} x$,-90][$\color{gdarkgray} y$,180]
    \psplot[linecolor=darkgreen,plotpoints=200]{-3}{3}{1/sqrt(3.1415926)*2.71828182^(-1*x^2)}
    \psplot[linecolor=darkblue,plotpoints=200]{-3}{3}{3/sqrt(3.1415926)*2.71828182^(-3*x^2)}
    \psplot[linecolor=orangered,plotpoints=200]{-3}{3}{6/sqrt(3.1415926)*2.71828182^(-6*x^2)}
  \end{pspicture}
\end{figure}

\section{Eigenschaften der \texorpdfstring{$\delta$}{Delta}-Funktion}
\index{$\delta$-Funktion!Eigenschaften}
\begin{align}
  \int\limits_{-\infty}^{\infty} \delta(x) \dx{x} \overset{\eqref{eq:3.1}}{=} 1 \label{eq:3.6}
\end{align}
\begin{align}
  \int\limits_{-\infty}^{\infty} \delta(x) f(x) \dx{x} \overset{\eqref{eq:3.1}}{=} f(0) \label{eq:3.7}
\end{align}
\begin{align}
  \delta(x) x = 0 \label{eq:3.8}
\end{align}
\textsc{Beweis:}
\begin{align}
  &\int\limits_{-\infty}^{\infty} \delta(x) f(x) x \dx{x} \overset{\eqref{eq:3.1}}{=} 0 f(0) = 0 \nonumber\\
  = &\int\limits_{-\infty}^{\infty} \dx{x}\ 0\ f(0) \nonumber\\
  &\delta(g(x)) = \sum\limits_{n} \frac{1}{|g'(x_n)|} \delta(x-x_n), \label{eq:3.9}
\end{align}
wobei die $x_n$ alle (einfachen) Nullstellen von $g(x)$ sind.

Insbesondere folgt aus \eqref{eq:3.9}:
\begin{align}
  \text{Für }g(x) &= -x \nonumber\\
  \delta(-x) &= \delta(x) \label{eq:3.10}
\end{align}
\begin{align}
  \text{Für }g(x) &= bx \nonumber\\
  \delta(bx) &= \frac{1}{|b|}\delta(x) \label{eq:3.11}
\end{align}

\section{Beispiele für die Anwendung der \texorpdfstring{$\delta$}{Delta}-Funktion}
\index{$\delta$-Funktion!Anwendungen}
\begin{subequations}
\begin{align}
  \int\limits_{-\infty}^{\infty} f(x) \left(\delta(x-1) - \delta(x+1)\right) \dx{x} &= f(1)-f(-1) \label{eq:3.12a} \\
  \int\limits_{-1}^{2} x^2 \delta(x-1) \dx{x} &= 1 \label{eq:3.12b} \\
  \int\limits_{-1}^{2} x^2 \delta(x+2) \dx{x} &= 0 \label{eq:3.12c} \\
  \int\limits_{-\infty}^{\infty} e^{-x} \delta(x-y^2) \dx{x} &= e^{-y^2} \label{eq:3.12d} \\
  \int\limits_{-\infty}^{\infty} f(x) \delta(x-x_0) \dx{x} &= f(x_0) \label{eq:3.12e} \\
  \int\limits_{-\infty}^{\infty} 3x^2 \delta(x+2) \dx{x} &= 12 \label{eq:3.12f}
\end{align}
\end{subequations}

\index{$\delta$-Funktion|)}
