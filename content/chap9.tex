% Holger Cartarius, Henri Menke, 2012 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

\chapter{Fourierreihen und Fouriertransformation}
\index{Fourier\ldots|(}

\section{Fourierreihen}
\index{Fourier\ldots!Fourierreihen|(}

In der Natur kommen viele periodische Abläufe vor, z.B.
\begin{itemize}
  \item in der Zeit: Schwingungen, Umlaufbahnen
  \item in Zeit und Raum: Wellen
\end{itemize}

Oft ist man an den vorkommenden Frequenzen interessiert und will das Signal in seine Teilfrequenzen zerlegen. Dies ist die Aufgabe der Fourieranalyse.

\subsection{Voraussetzung und Definition} \label{sec:9.1.1}

Für jede Funktion $f(x)$, die 
\begin{itemize}
  \item periodisch ist mit der Periode $L$,
  \item eindeutig und stetig ist, {\color{gray} (Ausnahme: endlich viele Unstetigkeitsstellen)},
  \item innerhalb einer Periode nur endlich viele Extrema besitzt,
  \item für die das Integral von $|f(x)|$ über eine Periode konvergiert,
\end{itemize}
kann eine Fourierreihe
\begin{align}
  f(x) &= \frac{a_0}{2} + \sum_{k=1}^\infty \left [ a_k \cos \left ( \frac{2\pi k}{L} x \right ) + b_k \sin \left ( \frac{2\pi k}{L} x \right ) \right ] \label{eq:9.1}
\end{align}
mit den Fourierkoeffizienten
\begin{subequations}
  \begin{align}
    a_k &= \frac{2}{L} \int\limits_{x_0}^{x_0 + L} f(x) \cos \left( \frac{2\pi k}{L} x \right) \dx{x} \; , \quad k \in \mathbb{N}_0 \label{eq:9.2a} \\
    b_k &= \frac{2}{L} \int\limits_{x_0}^{x_0 + L} f(x) \sin \left( \frac{2\pi k}{L} x \right) \dx{x} \; , \quad k \in \mathbb{N} \label{eq:9.2b}
  \end{align}
\end{subequations}
entwickelt werden, die an allen Stetigkeitsstellen von $f(x)$ konvergiert ($[x_0,x_0+L]$ ist ein beliebiges Intervall der Länge $L$).

Das heißt, jede periodische Funktion, die die oben genannten Dirichlet-Bedingungen erfüllt, kann durch eine Summe von Sinus- und Cosinusfunktionen dargestellt werden.

\begin{figure}[H]
  \centering
  \begin{pspicture}(-0.5,-0.5)(5,2)
  \psaxes[labels=none,labelFontSize=\scriptstyle\color{gdarkgray} ,xAxis=true,yAxis=true,Dx=1,Dy=1,ticks=none]{->}(0,0)(-0.5,-0.5)(5,2)[$\color{gdarkgray} x$,-90][$\color{gdarkgray} f(x)$,180]
  \psplot[linecolor=darkblue,plotpoints=200]{0}{4}{(4/3.14159/1*sin(0.5*3.14159*x*1)+4/3.14159/3*sin(0.5*3.14159*x*3)+4/3.14159/5*sin(0.5*3.14159*x*5)+4/3.14159/7*sin(0.5*3.14159*x*7)+4/3.14159/9*sin(0.5*3.14159*x*9))^2}
  \psline(2,0.1)(2,-0.1)
  \uput[-90](2,0){$\color{gdarkgray} L$}
  \end{pspicture}
\end{figure}

Die Fourierreihen entsprechen der Zerlegung der Funktion in ihre elementaren Schwing\-ung\-en mit den Frequenzen
\begin{align}
  \omega_k &= \frac{2\pi k}{L} \; , \quad k \in \mathbb{N}.
\end{align}
Die Fourierkoeffizienten $a_k$ und $b_k$ drücken aus, wie stark jede Frequenz $\omega_k$ an der periodischen Funktion beteiligt ist:
\begin{align*}
  \omega_1 &= \frac{2\pi}{L} \quad \text{Grundfrequenz} \\
  \omega_k &= \frac{2\pi k}{L} = \omega_1\ k \; , \quad k > 1 \ \text{höhere Harmonische}
\end{align*}

Die Fourierreihe \eqref{eq:9.1} mit den Koeffizienten \eqref{eq:9.2a} und \eqref{eq:9.2b} ist möglich, weil für $k,\ell \geq 0$:
\begin{subequations}
  \begin{align}
    \int\limits_{x_0}^{x_0 + L} \sin \left( \frac{2\pi k}{L} x \right) \cos \left( \frac{2\pi \ell}{L} x \right) \dx{x} &= 0 \label{eq:9.4a}
  \\
    \int\limits_{x_0}^{x_0 + L} \cos \left( \frac{2\pi k}{L} x \right) \cos \left( \frac{2\pi \ell}{L} x \right) \dx{x} &=
    \begin{cases}
      L & \text{für } k = \ell = 0 \\
      \frac{L}{2} \delta_{k,\ell} & \text{sonst}
    \end{cases} \label{eq:9.4b}
  \\
    \int\limits_{x_0}^{x_0 + L} \sin \left( \frac{2\pi k}{L} x \right) \sin \left( \frac{2\pi \ell}{L} x \right) \dx{x} &= \frac{L}{2} \delta_{k,\ell} \; , \quad k,\ell \neq 0 \label{eq:9.4c}
  \end{align}
\end{subequations}
mit dem Kronecker-Delta:
\begin{align}
  \delta_{n,m} =
  \begin{cases}
    1 & \text{für } n = m \\
    0 & \text{sonst}
  \end{cases}
\end{align}

\subsubsection*{\textsc{Beweis} von \eqref{eq:9.4a}}
\begin{description}
  \item[Fall 1: $k=\ell=0$] 
    \begin{align*}
      & \int\limits_{x_0}^{x_0 + L} \sin \left( 0 \right) \cos \left( 0 \right) \dx{x} = 0
    \end{align*}
  \item[Alle anderen Fälle:]
    \begin{align*}
      & \int\limits_{x_0}^{x_0 + L} \sin \left( \frac{2\pi k}{L} x \right) \cos \left( \frac{2\pi \ell}{L} x \right) \dx{x}
    \\
      =& \int\limits_{x_0}^{x_0 + L} \frac{1}{2 i} \left( e^{\frac{2 \pi k i}{L} x} - e^{-\frac{2 \pi k i}{L} x} \right) \frac{1}{2} \left( e^{\frac{2 \pi \ell i}{L} x} + e^{-\frac{2 \pi \ell i}{L} x} \right) \dx{x}
    \\
      =& \int\limits_{x_0}^{x_0 + L} \frac{1}{4 i} \left( e^{\frac{2 \pi (k+\ell) i}{L} x} + e^{\frac{2 \pi (k-\ell) i}{L} x} - e^{-\frac{2 \pi (k+\ell) i}{L} x} - e^{-\frac{2 \pi (k-\ell) i}{L} x} \right) \dx{x}
    \\
      =& \int\limits_{x_0}^{x_0 + L} \frac{1}{4 i} \left( e^{\frac{2 \pi (k+\ell) i}{L} x} - e^{-\frac{2 \pi (k+\ell) i}{L} x} \right) + \frac{1}{4 i} \left(  e^{\frac{2 \pi (k-\ell) i}{L} x} - e^{-\frac{2 \pi (k-\ell) i}{L} x} \right) \dx{x}
    \\
      =& \underbrace{\frac{1}{2} \int\limits_{x_0}^{x_0 + L} \sin\left( \frac{2 \pi (k+\ell)}{L} x \right) \dx{x}}_{\parbox{12em}{\footnotesize Integrand führt immer volle Perioden aus, also $=0$}} + \underbrace{\frac{1}{2} \int\limits_{x_0}^{x_0 + L} \sin\left( \frac{2 \pi (k-\ell)}{L} x \right) \dx{x}}_{\parbox{12em}{\footnotesize Integrand führt außer für $k=\ell$ volle Perioden aus (also $=0$), für $k=\ell$ ist das Integral auch $=0$.}}
    \intertext{Also:}
      =& \frac{1}{2} \int\limits_{x_0}^{x_0 + L} \sin\left( \frac{2 \pi (k+\ell)}{L} x \right) \dx{x} + \frac{1}{2} \int\limits_{x_0}^{x_0 + L} \sin\left( \frac{2 \pi (k-\ell)}{L} x \right) \dx{x} = 0
    \end{align*}
\end{description}
Daraus folgt \eqref{eq:9.4a}.

\subsubsection*{\textsc{Beweis} von \eqref{eq:9.4b}}
\begin{description}
  \item[Fall 1: $k=\ell=0$] 
    \begin{align*}
      \int\limits_{x_0}^{x_0 + L} \underbrace{\cos\left( 0 \right) \cos\left( 0 \right)}_{1} \dx{x} = L
    \end{align*}
  \item[Alle anderen Fälle:]
    \begin{align*}
      & \int\limits_{x_0}^{x_0 + L} \cos \left( \frac{2\pi k}{L} x \right) \cos \left( \frac{2\pi \ell}{L} x \right) \dx{x} \\
      =& \int\limits_{x_0}^{x_0 + L} \frac{1}{4} \left(e^{\frac{2\pi k i}{L} x} + e^{-\frac{2\pi k i}{L} x}\right) \left( e^{\frac{2\pi \ell i}{L} x} + e^{-\frac{2\pi \ell i}{L} x} \right) \dx{x} \\
      =& \int\limits_{x_0}^{x_0 + L} \frac{1}{4} \left( e^{\frac{2\pi (k+\ell) i}{L} x} + e^{\frac{2\pi (k-\ell) i}{L} x} + e^{-\frac{2\pi (k+\ell) i}{L} x} + e^{-\frac{2\pi (k-\ell) i}{L} x} \right) \dx{x} \\
      =& \frac{1}{2} \int\limits_{x_0}^{x_0 + L} \left[ \cos \left( \frac{2\pi (k+\ell)}{L} x \right) + \cos \left( \frac{2\pi (k-\ell)}{L} x \right) \right] \dx{x} \\
      =& \underbrace{\frac{1}{2} \int\limits_{x_0}^{x_0 + L} \cos \left( \frac{2\pi (k+\ell)}{L} x \right) \dx{x}}_{\parbox{12em}{\footnotesize Integrand führt für $k,\ell \geq 0$ in $[x_0,x_0+L]$ immer eine oder mehrere volle Perioden aus, also $=0$}} + \underbrace{\frac{1}{2} \int\limits_{x_0}^{x_0 + L} \cos \left( \frac{2\pi (k-\ell)}{L} x \right) \dx{x}}_{\parbox{12em}{\footnotesize Integrand führt für $k,\ell \geq 0$, außer für $k=\ell$ in $[x_0,x_0+L]$, immer eine oder mehrere volle Perioden aus}} \\
    \intertext{Zweites Integral für $k=\ell$:}
      & \frac{1}{2} \int\limits_{x_0}^{x_0 + L} \underbrace{\cos \Bigg( \underbrace{\frac{2\pi (l-\ell)}{L}}_{=0} x \Bigg)}_{=1} \dx{x} = \frac{L}{2}
    \end{align*}
\end{description}
Daraus folgt \eqref{eq:9.4b}.

\subsubsection*{\textsc{Beweis} von \eqref{eq:9.4c}}
  \begin{align*}
    & \int\limits_{x_0}^{x_0 + L} \sin \left( \frac{2\pi k}{L} x \right) \sin \left( \frac{2\pi \ell}{L} x \right) \dx{x}
  \\
    =& \int\limits_{x_0}^{x_0 + L} \frac{1}{2 i} \left( e^{\frac{2 \pi k i}{L} x} - e^{-\frac{2 \pi k i}{L} x} \right) \frac{1}{2 i} \left( e^{\frac{2 \pi \ell i}{L} x} - e^{-\frac{2 \pi \ell i}{L} x} \right) \dx{x}
  \\
    =& \int\limits_{x_0}^{x_0 + L} -\frac{1}{4} \left( e^{\frac{2 \pi (k+\ell) i}{L} x} - e^{\frac{2 \pi (k-\ell) i}{L} x} - e^{-\frac{2 \pi (k-\ell) i}{L} x} + e^{-\frac{2 \pi (k+\ell) i}{L} x} \right) \dx{x}
  \\
    =& \int\limits_{x_0}^{x_0 + L} -\frac{1}{4} \left( e^{\frac{2 \pi (k+\ell) i}{L} x} + e^{-\frac{2 \pi (k+\ell) i}{L} x} \right) 
    -\frac{1}{4} \left( - e^{\frac{2 \pi (k-\ell) i}{L} x} - e^{-\frac{2 \pi (k-\ell) i}{L} x} \right) \dx{x}
  \\
    =& \underbrace{\frac{1}{2} \int\limits_{x_0}^{x_0 + L} \cos\left( \frac{2 \pi (k-\ell)}{L} x \right) \dx{x}}_{\parbox{12em}{\footnotesize für $k=\ell$: $\frac{L}{2}$ \\ für $k \neq \ell$: volle Periode, also $=0$}}
    - \underbrace{\frac{1}{2} \int\limits_{x_0}^{x_0 + L} \cos\left( \frac{2 \pi (k+\ell)}{L} x \right) \dx{x}}_{\parbox{12em}{\footnotesize nur volle Perioden, also immer $=0$}}
  \intertext{Also:}
    =& \frac{L}{2} \delta_{k,\ell}
  \end{align*}
Daraus folgt \eqref{eq:9.4c}.



Nimmt man an, dass die Form \eqref{eq:9.1} der periodischen Funktion existiert, folgen mit \eqref{eq:9.4a}, \eqref{eq:9.4b} und \eqref{eq:9.4c} sofort die Fourierkoeffizienten \eqref{eq:9.2a} und \eqref{eq:9.2b}.

\subsection{Komplexe Darstellung}

\subsubsection{Umrechnung und neue Koeffizienten}

\begin{subequations}
\begin{align*}
  f(x) &\overset{\eqref{eq:9.1}}{=} \frac{a_0}{2} + \sum\limits_{k=1}^{\infty} \left[ a_k \cos\left( \frac{2 \pi k}{L} x \right) + b_k \sin\left( \frac{2 \pi k}{L} x \right) \right] \\
  &\overset{\eqref{eq:4.18a}}{\underset{\eqref{eq:4.18b}}{=}} \frac{a_0}{2} + \sum\limits_{k=1}^{\infty} \left[ \frac{a_k}{2}\left( e^{\frac{2 \pi k i}{L} x} + e^{-\frac{2 \pi k i}{L} x} \right) + \frac{b_k}{2i}\left( e^{\frac{2 \pi k i}{L} x} - e^{-\frac{2 \pi k i}{L} x} \right) \right] \\
  &= \underbrace{\frac{a_0}{2}}_{c_0} + \sum\limits_{k=1}^{\infty} \Bigg[ \underbrace{\frac{a_k - i b_k}{2}}_{c_k} e^{\frac{2 \pi k i}{L} x} + \underbrace{\frac{a_k + i b_k}{2i}}_{c_{-k}} e^{-\frac{2 \pi k i}{L} x} \Bigg] \\
  &= \sum\limits_{k=-\infty}^{\infty} c_k\ e^{\frac{2 \pi k i}{L} x}
\end{align*}
%
Die komplexe Fourierreihe lautet also:
%
\begin{align}
  f(x) = \sum\limits_{k=-\infty}^{\infty} c_k\ e^{\frac{2 \pi k i}{L} x} \label{eq:9.6a}
\end{align}
%
mit dem Koeffizienten
%
\begin{align}
  c_k = \frac{a_k - i b_k}{2} \overset{\eqref{eq:9.2a}}{\underset{\eqref{eq:9.2b}}{=}} \frac{1}{L} \int\limits_{x_0}^{x_0+L} f(x) e^{-\frac{2 \pi k i}{L} x} \dx{x} \label{eq:9.6b}
\end{align}
%
Anders herum gilt natürlich:
\begin{align}
  a_k = c_k + c_{-k} \; , \quad b_k = i(c_k - c_{-k}) \label{eq:9.6c}
\end{align}
\end{subequations}

\subsubsection{Symmetrien und Beziehungen}

Symmetrien der Funktion schlagen sich auch in den Fourierkoeffizienten nieder.
%
\begin{description}
  \item[relle Funktionen:] $f(x) = \overline{f}(x)$
    
    $c_k = \overline{c_{-k}} \; , \quad a_k = 2 \Re(c_k) \; , \quad b_k = -2 \Im(c_k)$
  
  \item[imaginäre Funktionen:] $f(x) = -\overline{f}(x)$
    
    $c_k = -\overline{c_k} \; , \quad a_k = 2 i \Im(c_k) \; , \quad b_k = 2 i \Re(c_k)$
  
  \item[symmetrische Funktionen:] $f(x) = f(-x)$
    
    $c_k = c_{-k} \; , \quad a_k = 2 c_k \; , \quad b_k = 0$
  
  \item[antisymmetrische Funktionen:] $f(x) = -f(-x)$
    
    $c_k = -c_{-k} \; , \quad a_k = 0 \; , \quad b_k = 2 i c_k$
\end{description}

\subsection{Beispiele}

\subsubsection{Dreieck-Funktion}

\emph{Dreieck-Funktion}: $\displaystyle f(x) = 1 - \frac{2 |x|}{L} \; , \quad -\frac{L}{2} < x < \frac{L}{2}$

\begin{figure}[H]
  \centering
  \begin{pspicture}(-3.5,-0.5)(3.5,1.5)
  \psaxes[labels=none,labelFontSize=\scriptstyle\color{gdarkgray} ,xAxis=true,yAxis=true,Dx=1,Dy=1,ticks=none]{->}(0,0)(-3.5,-0.5)(3.5,1.5)[$x$,-90][$y$,0]
  \psline[linecolor=orangered,linestyle=dashed](-3,0)(-2,1)
  \psline[linecolor=orangered,linestyle=dashed](2,1)(3,0)
  \psline[linecolor=orangered](-2,1)(-1,0)(0,1)(1,0)(2,1)
  \psline(-0.1,1)(0.1,1)
  \psline(1,-0.1)(1,0.1)
  \psline(-1,-0.1)(-1,0.1)
  \uput[180](0,1){\scriptsize $1$}
  \uput[-90](-1,0){\scriptsize $-\frac{L}{2}$}
  \uput[-90](1,0){\scriptsize $\frac{L}{2}$}
  \end{pspicture}
\end{figure}
%
Fourierkoeffizienten:
%
\begin{align*}
  a_k &\overset{\eqref{eq:9.2a}}{=} \frac{2}{L} \int\limits_{-\frac{L}{2}}^{\frac{L}{2}} \left( 1 - \frac{2 |x|}{L} \right) \cos\left( \frac{2 \pi k}{L} x \right) \dx{x}
\\
  &= \frac{2}{L} \int\limits_{-\frac{L}{2}}^{0} \left( 1 + \frac{2 x}{L} \right) \cos\left( \frac{2 \pi k}{L} x \right) \dx{x} +  \frac{2}{L} \int\limits_{0}^{\frac{L}{2}} \left( 1 - \frac{2 x}{L} \right) \cos\left( \frac{2 \pi k}{L} x \right) \dx{x}
\\
  &= \frac{2}{L} \int\limits_{-\frac{L}{2}}^{0} \cos\left( \frac{2 \pi k}{L} x \right) \dx{x} + \frac{4}{L^2} \int\limits_{-\frac{L}{2}}^{0} x\ \cos\left( \frac{2 \pi k}{L} x \right) \dx{x} \\*
  & \quad + \frac{2}{L} \int\limits_{0}^{\frac{L}{2}} \cos\left( \frac{2 \pi k}{L} x \right) \dx{x} - \frac{4}{L^2} \int\limits_{0}^{\frac{L}{2}} x\ \cos\left( \frac{2 \pi k}{L} x \right) \dx{x}
\\
  &= \frac{1}{\pi k} \overbrace{\left[ \sin\left( \frac{2 \pi k}{L} x \right) \right]_{-\frac{L}{2}}^{0}}^{=0} + \frac{2}{L \pi k} \overbrace{\left[ x \sin\left( \frac{2 \pi k}{L} x \right) \right]_{-\frac{L}{2}}^{0}}^{=0} \\*
  & \quad - \frac{2}{L \pi k} \int\limits_{-\frac{L}{2}}^{0} \sin\left( \frac{2 \pi k}{L} x \right) \dx{x} + 0 - 0 + \frac{2}{L \pi k} \int\limits_{0}^{\frac{L}{2}} \sin\left( \frac{2 \pi k}{L} x \right) \dx{x}
\\
  &= \frac{1}{(\pi k)^2} \underbrace{\left[ \cos\left( \frac{2 \pi k}{L} x \right) \right]_{-\frac{L}{2}}^{0}}_{\footnotesize \begin{cases} 1+1=2 & \text{$k$ ungerade} \\ 1-1=0 & \text{$k$ gerade} \end{cases}} - \frac{1}{(\pi k)^2} \underbrace{\left[ \cos\left( \frac{2 \pi k}{L} x \right) \right]_{0}^{\frac{L}{2}}}_{\footnotesize \begin{cases} -2 & \text{$k$ ungerade} \\ 0 & \text{$k$ gerade} \end{cases}} 
\\
  &= \begin{cases} \frac{4}{(\pi k)^2} & \text{falls $k$ ungerade} \\ 0 & \text{falls $k$ gerade} \end{cases}
\end{align*}
%
Es gilt $b_k=0$, weil $f(x)$ eine gerade (=symmetrische) Funktion ist.
%
\begin{align*}
  c_k &\overset{\eqref{eq:9.6b}}{=} \frac{a_k - i b_k}{2} = \begin{cases} \frac{2}{(\pi k)^2} & \text{falls $k$ ungerade} \\ 0 & \text{falls $k$ gerade} \end{cases}
\end{align*}
%
Somit:
%
\begin{align*}
  f(x) &\overset{\eqref{eq:9.1}}{=} \sum\limits_{k=1}^{\infty} \frac{4}{(\pi (2k-1))^2} \cos\left( \frac{2 \pi (2k-1)}{L} x \right) \\
  &\overset{\eqref{eq:9.6a}}{=} \sum\limits_{k=-\infty}^{\infty} \frac{2}{(\pi (2k-1))^2} e^{\frac{2 \pi (2k-1) i}{L} x}
\end{align*}

\subsubsection{Rechteck-Funktion}

\emph{Rechteck-Funktion}

\begin{figure}[H]
  \centering
  \begin{pspicture}(-0.5,-1.5)(3.5,1.5)
  \psaxes[labels=none,labelFontSize=\scriptstyle\color{gdarkgray} ,xAxis=true,yAxis=true,Dx=1,Dy=1,ticks=none]{->}(0,0)(-0.5,-1.5)(3.5,1.5)[$x$,-90][$y$,0]
  \psline[linecolor=orangered,linestyle=dashed](3,1)(3,-1)
  \psline[linecolor=orangered](0,0)(0,1)(1,1)(1,-1)(2,-1)(2,1)(3,1)
  \psline(-0.1,1)(0.1,1)
  \psline(-0.1,-1)(0.1,-1)
  %\psline(2,-0.1)(2,0.1)
  \psdot(2,0)
  \uput[180](0,1){\scriptsize $1$}
  \uput[180](0,-1){\scriptsize $-1$}
  \uput[-45](2,0){\scriptsize $L$}
  \end{pspicture}
\end{figure}
%
Fourierkoeffizienten:
%
\begin{align*}
  c_k &\overset{\eqref{eq:9.6b}}{=} \frac{1}{L} \int\limits_{0}^{L} f(x) e^{-\frac{2 \pi k i}{L} x} \dx{x} \\
  &= \frac{1}{L} \int\limits_{0}^{\frac{L}{2}} e^{-\frac{2 \pi k i}{L} x} \dx{x} - \frac{1}{L} \int\limits_{\frac{L}{2}}^{L} e^{-\frac{2 \pi k i}{L} x} \dx{x} \\
  &= - \frac{1}{L} \frac{L}{2 \pi k i} \underbrace{\left[ e^{-\frac{2 \pi k i}{L} x} \right]_{0}^{\frac{L}{2}}}_{e^{- \pi k i} -1} + \frac{1}{2 \pi k i} \left( 1-e^{-\pi k i} \right) \\
  &= \begin{cases} -\frac{2 i}{\pi k} & \text{für $k$ ungerade} \\ 0 & \text{für $k$ gerade} \end{cases} \\
  a_k &\overset{\eqref{eq:9.6c}}{=} 0 \\
  b_k &\overset{\eqref{eq:9.6c}}{=} \begin{cases} \frac{4}{\pi k} & \text{für $k$ ungerade} \\ 0 & \text{für $k$ gerade} \end{cases}
\end{align*}
%
Somit:
%
\begin{align*}
  f(x) &= \sum\limits_{k=1}^{\infty} \frac{4}{\pi (2k-1)} \sin\left( \frac{2 \pi (2k-1)}{L} x \right) \\
  &= \sum\limits_{k=-\infty}^{\infty} -\frac{2 i}{\pi (2k-1)}\ e^{\frac{2 \pi (2k-1) i}{L} x}
\end{align*}

\index{Fourier\ldots!Fourierreihen|)}

\section{Fouriertransformation} \index{Fourier\ldots!Fouriertransformation|(}

\subsection{Motivation aus der Fourierreihe}

In \autoref{sec:9.1.1} haben wir gesehen, dass man eine periodische Funktion allein durch ihre Fourierkoeffizienten $a_k$ und $b_k$ darstellen kann. Sie enthalten sämtliche Information über $f(x)$ und mit \eqref{eq:9.1} kann $f(x)$ eindeutig dargestellt werden.

Das kann man als \emph{Basisentwicklung} verstehen, bei der $\sin(\omega_k x)$ und $\cos(\omega_k x)$ die Basisfunktionen darstellen und die Koeffizienten $a_k$, $b_k$ den Beitrag der einzelnen Basisfunktionen zu $f(x)$ festlegen.

Man kann dies mit der Basisdarstellung eines Vektors vergleichen:
\begin{align*}
  \bm{a} = \sum\limits_{i} a_i \vek{e}{i}
\end{align*}
%
\begin{tabular}{rcl}
Frage & : & Gibt es so etwas auch für nichtperiodische Funktionen? \\
Antwort & : & Ja, dafür finden Integraltransformationen Anwendung. \\
\end{tabular}

In diesem Kapitel werden wir die \emph{Fouriertransformation} kennen lernen. Eine weitere nennenwerte aber hier nicht behandelte Integraltransformation wäre die Laplace-Transformation.

\begin{subequations}
Für die Fouriertransformation betrachten wir zunächst:
%
\begin{align}
  f(x) &\overset{\eqref{eq:9.6a}}{=} \sum\limits_{n=-\infty}^{\infty} c_n\ \exp\left( \frac{2 \pi n i}{L} x \right) \nonumber \\
  &= \sum\limits_{n=-\infty}^{\infty} \underbrace{\frac{2 \pi}{L}}_{\Delta k} \frac{1}{2 \pi} \underbrace{L\ c_n}_{\hat{f}(k_n)} \exp\Big( i \overbrace{\underbrace{\frac{2 \pi}{L}}_{\Delta k} n}^{k_n} x \Big) \nonumber \\
  &= \frac{1}{2\pi} \sum\limits_{n=-\infty}^{\infty} \Delta k \ e^{i k_n x} \hat{f}(k_n) \label{eq:9.7a}
\end{align}
%
$\hat{f}(k_n)$ kann als Funktion von $k_n$ aufgefasst werden, weil zu jedem $n$ eindeutig ein $k_n$ gehört.

Man kann nun eine nichtperiodische Funktion als eine Funktion mit unendlicher Periode auffassen:
%
\begin{align*}
  L \rightarrow \infty \; , \quad \Delta k \rightarrow 0
\end{align*}
%
Dann:
%
\begin{align}
  f(x) &= \frac{1}{2\pi} \int\limits_{-\infty}^{\infty} \hat{f}(k) \ e^{i k x} \dx{k} \label{eq:9.7b} \\
  \text{mit }\ \hat{f}(k) &\overset{\eqref{eq:9.7a}}{=} L\ c_n \overset{\eqref{eq:9.6b}}{=} L \frac{1}{L} \int\limits_{x_0}^{x_0 + L} f(x) \ e^{-i k x} \dx{x} \nonumber \\
  &\overset{(L \rightarrow \infty)}{=} \int\limits_{-\infty}^{\infty} f(x) \ e^{i k x} \dx{x} \label{eq:9.7c}
\end{align}
%

\end{subequations}
%

\subsection{Definition}

Man bezeichnet die $k$-abhängige Funktion
%
\begin{align}
  \hat{f}(k) \overset{\eqref{eq:9.7c}}{=} \int\limits_{-\infty}^{\infty} f(x) e^{-ikx} \;
  \mathrm{d}x
  \label{eq:9.8}
\end{align}
%
als die Fouriertransformierte von $f(x)$. Die inverse Fouriertransformation
%
\begin{align}
  f(x) \overset{\eqref{eq:9.7b}}{=} \frac{1}{2\pi} \int\limits_{-\infty}^{\infty}
  \hat{f}(k) e^{ikx} \; \mathrm{d}k
  \label{eq:9.9}
\end{align}
%
beschreibt die Rücktransformation von $\hat{f}(k)$ nach $f(x)$ oder, anders ausgedrückt, die Darstellung von $f(x)$ in der kontinuierlichen Basis $e^{ikx}$, $k\in \mathbb{R}$ mit den Koeffizienten $\hat{f}(k)$. Die Gleichungen \eqref{eq:9.8} und \eqref{eq:9.9} beschreiben den Darstellungswechsel von $x$ nach $k$ und umgekehrt.

Zur Bezeichnung der Fouriertransformation und ihrer Inversen schreibt man oft auch:
%
\begin{subequations}
  \begin{align}
    \hat{f}(k) &= \mathcal{F}(f)(k) \\
    f(x) &= \mathcal{F}^{-1}(\hat{f})(x)
  \end{align}
\end{subequations}

Zuletzt noch eine Anmerkung zum Vorfaktor $\nicefrac{1}{2 \pi}$: Die Aufteilung des Vorfaktors 
%
\begin{align*}
  \hat{f}(k) &= \int \dots \mathrm{d}x \; , 
  &f(x) &= \frac{1}{2\pi} \int \dots \mathrm{d}k \\
  \intertext{ist willkürlich. In der Literatur findet man vielfach andere
    Aufteilungen, nämlich}
  \hat{f}(k) &= \frac{1}{\sqrt{2\pi}} \int \dots \mathrm{d}x \; , 
  &f(x) &= \frac{1}{\sqrt{2\pi}} \int \dots \mathrm{d}k \\
  \hat{f}(k) &= \frac{1}{2\pi} \int \dots \mathrm{d}x \; , 
  &f(x) &= \int \dots \mathrm{d}k \\
  \intertext{oder}
  \hat{f}(k) &= \int f(x) e^{-2\pi ikx} \, \mathrm{d}x \; , 
  &f(x) &= \int \hat{f}(k) e^{2\pi ikx} \, \mathrm{d}k \; .
\end{align*}
%

\subsection{Wichtige Beispiele und Relationen}

\subsubsection{\texorpdfstring{$\delta$}{Delta}-Funktion}

\begin{subequations}
  \begin{align}
    \mathcal{F}(\delta(x-a))(k) =  \int\limits_{-\infty}^{\infty} \delta(x-a) e^{-ikx} \;
    \mathrm{d}x = e^{-ika}
    \label{eq:9.11a}
  \end{align}
  %
  Für die Umkehrung muss dann also gelten:
  %
  \begin{align*}
    \mathcal{F}^{-1}(e^{-ika})(x) &= \frac{1}{2\pi} \int\limits_{-\infty}^{\infty} e^{-ika}
    e^{ikx} \; \mathrm{d}k \\
    &= \frac{1}{2\pi} \int\limits_{-\infty}^{\infty} e^{ik(x-a)} \; \mathrm{d}k
    \overset{!}{=} \delta (x-a)
  \end{align*}
  %
  Das heißt, wir haben eine neue, wichtige Darstellung der $\delta$-Funktion gefunden:
  %
  \begin{equation}
    \begin{aligned}
      \delta(x-a) &= \frac{1}{2\pi} \int_{-\infty}^{\infty} e^{ik(x-a)}
      \; \mathrm{d}k \\
      &= \frac{1}{2\pi} \int_{-\infty}^{\infty} e^{-ik(x-a)} \; \mathrm{d}k
    \end{aligned}
    \label{eq:9.11b}
  \end{equation}
  %
\end{subequations}

\subsubsection{Wichtige Fouriertransformierte}

Wichtige Fouriertransformierte, die häufig in der Physik eine Rolle spielen, sind:
\begin{itemize}
\item Fouriertransformation der $\delta$-Funktion:
  %
  \begin{align}
    \mathcal{F}(\delta(x))(k) \overset{\eqref{eq:9.11a}}{=} 1 
  \end{align}

\item Fouriertransformation der $1$:
  %
  \begin{align}
    \mathcal{F}(1)(k) = \int\limits_{-\infty}^{\infty} e^{-ikx} \; \mathrm{d}x
    \overset{\eqref{eq:9.11b}}{=} 2\pi \delta(k)
  \end{align}

\item Fouriertransformation der Gauß-Funktion:
  %
  \begin{align}
    \mathcal{F}(e^{-ax^2})(k) &= \int\limits_{-\infty}^{\infty} e^{-ax^2} e^{-ikx} \; 
    \mathrm{d}x \notag \\
    &= \int\limits_{-\infty}^{\infty} e^{-a \left[x + \frac{ik}{2a}\right]^2 - \frac{k^2}{4a}} \; \mathrm{d}x
    \notag \\
    &= e^{-\frac{k^2}{4a}} \underbrace{\int\limits_{-\infty}^{\infty} e^{-a \left[x + \frac{ik}{2a}\right]^2} 
      \; \mathrm{d}x}_{=\sqrt{\frac{\pi}{a}}} \notag \\
    &= \sqrt{\frac{\pi}{a}} e^{-\frac{k^2}{4a}} \; , \quad a > 0
  \end{align}

\item Fouriertransformation des exponentiellen Abfalls:
  %
  \begin{align}
    \mathcal{F}(e^{-a|x|})(k) &= \int\limits_{-\infty}^{\infty} e^{-a|x|} e^{-ikx} \; 
    \mathrm{d}x \notag \\
    &= \int\limits_{-\infty}^{0} e^{(a-ik) x} \; \mathrm{d}x
    + \int\limits_{0}^{\infty} e^{-(a+ik) x} \; \mathrm{d}x \notag \\
    &= \frac{1}{a-ik} \underbrace{\left [ e^{(a-ik) x} \right 
      ]_{-\infty}^{0}}_{1-0 = 1} -  \frac{1}{a+ik} \underbrace{\left [ e^{-(a+ik) x}
      \right ]_{0}^{\infty}}_{0-1 = -1} \notag \\
    &= \frac{1}{a-ik} + \frac{1}{a+ik} = \frac{a+ik+a-ik}{a^2+k^2} \notag \\ 
    & = \frac{2a}{a^2+k^2}
  \end{align}
  %
  Das Ergebnis ist die Lorentz-Kurve.

\item Fouriertransformation trigonometrischer Funktionen:
  %
  \begin{subequations}
    \begin{align}
      \mathcal{F}(\cos(at))(\omega) &= \int\limits_{-\infty}^{\infty} \cos(at)
      e^{-i\omega t} \; \mathrm{d}t \notag \\
      &=  \int\limits_{-\infty}^{\infty} \frac{1}{2} \left ( e^{iat} + e^{-iat} \right )
      e^{-i\omega t} \; \mathrm{d}t \notag \\
      &= \frac{1}{2} \int\limits_{-\infty}^{\infty} e^{-i(\omega-a)t} \; \mathrm{d}t 
      + \frac{1}{2} \int\limits_{-\infty}^{\infty} e^{-i(\omega+a)t} \; \mathrm{d}t
      \notag \\
      &\overset{\eqref{eq:9.11b}} = \pi \left [ \delta(\omega-a) 
        + \delta(\omega+a) \right ]
    \end{align}
    %
    Analog erhält man:
    %
    \begin{align}
      \mathcal{F}(\sin(at))(\omega) = i \pi \left [ \delta(\omega+a) 
        - \delta(\omega-a) \right ]
    \end{align}
  \end{subequations}

  Man kann sich ganz allgemein merken, dass periodische Funktionen ein diskretes Fourierspektrum besitzen.

  \begin{figure}[H]
    \centering
    \begin{pspicture}(-0.5,-0.5)(13,2)
      \psaxes[labels=none,labelFontSize=\scriptstyle\color{gdarkgray} ,xAxis=true,yAxis=true,Dx=1,Dy=1,ticks=none]{->}(0,0)(-0.5,-0.5)(5,2)[$\color{gdarkgray} t$,-90][$\color{gdarkgray} f(t)$,180]
      \psplot[linecolor=darkblue,plotpoints=400]{0}{4}{0.4*(sin(20*x)-cos(13*x))}
      \uput[0](0.2,1.2){periodisches Signal in der Zeit}
      \psline{->}(6,0.8)(7,0.8)
      \rput(8,0){
        \psaxes[labels=none,labelFontSize=\scriptstyle\color{gdarkgray} ,xAxis=true,yAxis=true,Dx=1,Dy=1,ticks=none]{->}(0,0)(-0.5,-0.5)(5,2)[$\color{gdarkgray} \omega$,-90][$\color{gdarkgray} \hat{f}(\omega)$,180]
        \psline[linecolor=darkblue](1,0)(1,1)
        \psline[linecolor=darkblue](2,0)(2,1.5)
        \psline[linecolor=darkblue](3,0)(3,1.2)
        \psline[linecolor=darkblue](4,0)(4,0.8)
        \uput[-90](1,0){$\color{gdarkgray} \omega_0$}
        \uput[-90](2,0){$\color{gdarkgray} 2 \omega_0$}
        \uput[-90](3,0){$\color{gdarkgray} 3 \omega_0$}
        \uput[-90](4,0){$\color{gdarkgray} 4 \omega_0$}
        \uput[0](0.2,1.8){diskretes Fourierspektrum}
      }
    \end{pspicture}
  \end{figure}
\end{itemize}

\subsubsection{Faltung}

Wir betrachten das Produkt
%
\begin{subequations}
  \begin{align}
    f_1(x) = g(x) h(x)
    \label{eq:9.17a}
  \end{align}
  %
  zweier Funktionen $g$ und $h$ und seine Fouriertransformierte:
  %
  \begin{align}
    \mathcal{F}(f_1(x))(k) &= \hat{f}_1(k) \overset{\eqref{eq:9.17a}}
    {\underset{\eqref{eq:9.8}}{=}} \int\limits_{-\infty}^{\infty} g(x)h(x) e^{-ikx} \; 
    \mathrm{d}x \notag \\
    &\overset{\eqref{eq:3.1}}{=} \int\limits_{-\infty}^{\infty} \mathrm{d}x \, g(x)
    \underbrace{\int\limits_{-\infty}^{\infty} \delta(y-x)h(y)\,\mathrm{d}y}_{h(x)}
    \, e^{-ikx} \notag \\
    &\overset{\eqref{eq:9.11b}}{=} \int\limits_{-\infty}^{\infty} \mathrm{d}x \, g(x)
    \int\limits_{-\infty}^{\infty} \mathrm{d}y \, \underbrace{\frac{1}{2\pi}
      \int\limits_{-\infty}^{\infty} \mathrm{d}\ell\, e^{-i\ell(y-x)}}_{\delta(y-x)}
    h(y) \, e^{-ikx} \notag \\
    &= \frac{1}{2\pi} \int\limits_{-\infty}^{\infty} \mathrm{d}\ell \, 
    \int\limits_{-\infty}^{\infty} \mathrm{d}x \, g(x) e^{-i(k-\ell)x} 
    \int\limits_{-\infty}^{\infty} \mathrm{d}y \, h(y) e^{-i\ell y} \notag \\
    &= \frac{1}{2\pi} \int\limits_{-\infty}^{\infty} \mathcal{F}(g)(k-\ell) \, 
    \mathcal{F}(h)(\ell) \, \mathrm{d}\ell \notag \\
    &= \frac{1}{2\pi} \int\limits_{-\infty}^{\infty} \hat{g}(k-\ell) \, \hat{h}(\ell)
    \, \mathrm{d}\ell 
  \end{align}
\end{subequations}

Man nennt das Integral
%
\begin{align}
  \int\limits_{-\infty}^{\infty}  \hat{g}(k-\ell) \, \hat{h}(\ell) \, \mathrm{d}\ell 
\end{align}
%
Faltung von $\hat{g}$ mit $\hat{h}$. 

Das Ergebnis dieser Rechnung ist also, dass die Fouriertransformation des Produkts zweier Funktionen $g$ und $h$ die Faltung der beiden Fouriertransformierten $\hat{g}$ und $\hat{h}$ ergibt. Umgekehrt gilt für die Faltung
%
\begin{align*}
  f_2(x) = \int\limits_{-\infty}^{\infty} g(y) \, h(x-y) \, \mathrm{d}y \; ,
\end{align*}
%
dass ihre Fouriertransformierte
%
\begin{align}
  \mathcal{F}(f_2)(k) = \hat{g}(k) \, \hat{h}(k)
\end{align}
%
gerade das Produkt der beiden Fouriertransformierten $\hat{g}$ und $\hat{h}$
ist.

\subsection{Mehrdimensionale Fouriertransformation}

Man kann die Fouriertransformation auch in mehreren Dimensionen definieren. Sie lautet dann in $N$ Dimensionen:
%
\begin{subequations}
  \begin{align}
    \hat{f}(\bm{k}) = \overbrace{\int\limits_{-\infty}^{\infty} \mathrm{d}^N
      \bm{x}}^{\mathclap{\parbox{10em}{\footnotesize \centering Lies: Integriere\\
          jede Komponente\\ von $-\infty$ bis $\infty$}}}
    f(\bm{x}) \, e^{-i\bm{k}\cdot\bm{x}} \label{eq:9.20a}\\ 
    f(\bm{x}) = \frac{1}{(2\pi)^N}\int\limits_{-\infty}^{\infty} \mathrm{d}^N\bm{k} \,
    \hat{f}(\bm{k}) \, e^{i\bm{k}\cdot\bm{x}} 
  \end{align}
\end{subequations}

Als Beispiel für eine mehrdimensionale Fouriertransformation soll nun noch die Fouriertransformierte der dreidimensionale Funktion
%
\begin{align*}
  f(\bm{r}) = e^{-a |\bm{r}|} \; , \quad a > 0
\end{align*}
%
bestimmt werden. Das dafür benötigte Integral
%
\begin{align*}
  \hat{f}(\bm{k}) \overset{\eqref{eq:9.20a}}{=} \int\limits_{-\infty}^{\infty} 
  e^{-a |\bm{r}|} e^{-i\bm{k}\cdot\bm{r}} \underbrace{\mathrm{d}^3\bm{r}
  }_{\mathrm{d}x\, \mathrm{d}y \, \mathrm{d}z}
\end{align*}
%
lässt sich am einfachsten berechnen, wenn man in Kugelkoordinaten übergeht und -- in Gedanken -- für jedes $k$ das Koordinatensystem von $\bm{r}$ so dreht, dass $\bm{k} = k \bm{e}_z$ in $z$-Richtung zeigt. Dann erhält man
%
\begin{align*}
  |\bm{r}| &= r \; , \\
  \bm{k}\cdot\bm{r} &= k \bm{e}_z \cdot \bm{r} = kz = kr\cos{\theta} 
\end{align*}
%
und somit:
%
\begin{align*}
  \hat{f}(\bm{k}) &= \int\limits_0^{\infty} r^2 \mathrm{d}r \int\limits_0^{\pi} \sin\theta \,
  \mathrm{d}\theta \int\limits_0^{2\pi} \mathrm{d}\varphi \, e^{-ar} e^{-ikr\cos\theta} \\
  &\overset{\alpha = \cos \theta}{\underset{\mathrm{d}\alpha = -\sin\theta
      \mathrm{d}\theta}{=}} -2\pi \int_0^{\infty} r^2 \mathrm{d}r \int_1^{-1}
  \mathrm{d}\alpha\, e^{-ar} e^{-ikr \alpha} \\
  &= -2\pi \int_0^{\infty}  \mathrm{d}r \, r^2 \frac{1}{-ikr} e^{-ar}
  \left [ e^{-ikr \alpha} \right ]_{1}^{-1} \\
  &= -\frac{2\pi}{k} \int_0^{\infty} \mathrm{d}r \, e^{-ar} i r \left [ 
    e^{ikr} - e^{-ikr} \right ] \\
  &= -\frac{2\pi}{k} \Bigg[ \int_0^{\infty} \mathrm{d}r \, e^{-ar} 
    \underbrace{i r e^{ikr}}_{\frac{\partial}{\partial k} e^{ikr}}
    +  \int_0^{\infty} \mathrm{d}r \, e^{-ar} 
    \underbrace{(-i r) e^{-ikr}}_{\frac{\partial}{\partial k} e^{-ikr}} \Bigg] \\
  &= -\frac{2\pi}{k} \frac{\partial}{\partial k} \left[ 
    \int_0^{\infty} e^{-(a-ik)r} \, \mathrm{d}r +  \int_0^{\infty} e^{-(a+ik)r} \, 
    \mathrm{d}r \right] \\
  &= -\frac{2\pi}{k} \frac{\partial}{\partial k} \Bigg[ -\frac{1}{a-ik}
    \underbrace{\left [ e^{-(a-ik)r} \right ]_0^\infty}_{ = -1} 
    - \frac{1}{a+ik} \underbrace{\left [ e^{-(a+ik)r} \right ]_0^\infty}_{ = -1} 
  \Bigg] \\
  &= -\frac{2\pi}{k} \frac{\partial}{\partial k} \frac{a+ik+a-ik}{a^2+k^2}
  = -\frac{4\pi}{k} \frac{\partial}{\partial k} \frac{a}{a^2+k^2} \\
  &= -\frac{4\pi}{k} \frac{-2 ak }{(a^2+k^2)^2} = \frac{8 \pi a }{(a^2+k^2)^2}
\end{align*}

\index{Fourier\ldots!Fouriertransformation|)}

\index{Fourier\ldots|)}
