% Holger Cartarius, Henri Menke, 2012 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

\chapter{Komplexe Zahlen}
\index{Komplexe Zahlen|(}

Die \emph{komplexen Zahlen} sind eine Erweiterung der reellen Zahlen. Die Konstruktion erfolgt durch $\mathbb{C} = \mathbb{R} \times \mathbb{R}$.

\section{Notwendigkeit und Darstellung}

\subsection{Einführung}

Hat die Gleichung
\begin{align}
  z^2 = -1 \label{eq:4.1}
\end{align}
eine Lösung?

Nicht für $z \in \mathbb{R}$, aber es kann sinnvoll sein, solche Gleichungen trotzdem zu lösen, deshalb erweitern wir die bisher verwendeten reellen Zahlen:
\begin{align}
  i^2 = -1 \label{eq:4.2}
\end{align}
Damit hat die Gleichung \eqref{eq:4.1} zwei Lösungen:
\begin{align}
  z_1 = i \; , \; z_2 = -i \label{eq:4.3}
\end{align}
$i$ wird auch  als \emph{imaginäre Einheit} bezeichnet.

Eine komplexe Zahl wird dann geschrieben als:
\begin{align}
  z= x+iy \label{eq:4.4}
\end{align}
wobei $z \in \mathbb{C}$ eine komplexe Zahl bezeichnet.

\begin{itemize}
  \item $x \in \mathbb{R}$ --- \emph{Realteil} von $z$ (auch $\Re(z)$ oder $\mathfrak{R}(z)$)
  \item $y \in \mathbb{R}$ --- \emph{Imaginärteil} von $z$ (auch $\Im(z)$ oder $\mathfrak{I}(z)$)
\end{itemize}

\textbf{Beispiele} für komplexe Lösungen:
\begin{align*}
  z^2 = -4 \; , \; z &= \pm 2 i \\
  z^2 - 4z + 29 &= 0 \\
  z_{1,2} &= \frac{4 \pm \sqrt{16 - 4 \cdot 29}}{2} = \frac{4 \pm \sqrt{-100}}{2} = 2 \pm \frac{\sqrt{100}\ i}{2} \\
  &= 2 \pm 5 i
\end{align*}

\subsection{Polare Darstellung}
Reelle Zahlen können auf einer Achse dargestellt werden.

\begin{figure}[H]
  \centering
  \begin{pspicture}(-1.5,-0.5)(5,0.5)
    \psaxes[labelFontSize=\scriptstyle\color{gdarkgray} ,xAxis=true,yAxis=false,Dx=1,Dy=1,ticksize=-2pt 2pt,subticks=1]{->}(0,0)(-1.5,0)(5,0)[$\color{gdarkgray} \mathbb{R}$,-90][ ,0]
  \end{pspicture}
\end{figure}

Für komplexe Zahlen benötigt man entsprechend Gleichung \eqref{eq:4.4} \textbf{zwei} reelle Achsen. Sie werden in der \emph{komplexen Zahlenebene} oder \emph{Gauß'schen Ebene} dargestellt. \index{Komplexe Zahlen!Komplexe Zahlenebene}\index{Komplexe Zahlen!Gauß'sche Ebene}

\begin{figure}[H]
  \centering
  \begin{pspicture}(-0.5,-3.5)(6,4)
    \psaxes[labels=none,labelFontSize=\scriptstyle\color{gdarkgray} ,xAxis=true,yAxis=true,Dx=1,Dy=1,ticksize=0]{->}(0,0)(-0.5,-3.5)(6,4)[$\color{gdarkgray} x=\Re(z)$,-90][$\color{gdarkgray} y=\Im(z)$,0]
    \psline[linecolor=darkblue](0,0)(3,3)
    \psline[linecolor=orangered](0,0)(3,-3)
    \psline[linestyle=dotted](3,3)(3,0)
    \psline[linestyle=dotted](3,3)(0,3)
    %\pscurve[linecolor=red](1,1)(1.25,0.6)(1.414,0)
    \psarc[linecolor=darkblue]{->}{1.2}{0}{45}
    \psdots[dotscale=1,linecolor=darkblue,fillcolor=darkblue](3,3)
    \psdots[dotscale=1,linecolor=orangered,fillcolor=orangered](3,-3)
    \uput[-90](3,0){$\color{gdarkgray} x_0$}
    \uput[180](-.1,3){$\color{gdarkgray} y_0$}
    \uput[-45](2,2){$\color{darkblue} r$}
    \uput[45](0.4,0){$\color{darkblue} \varphi$}
    \uput[0](3,3){$\color{darkblue} z$}
    \uput[0](3,-3){$\color{orangered} \overline{z}$}
%    \uput[180](0,1){$\color{darkblue} i$}
%    \psline[linecolor=darkblue](-0.1,1)(0.1,1)
  \end{pspicture}
\end{figure}

\begin{subequations}
Man sieht, dass jede komplexe Zahl eindeutig durch den \emph{Betrag}
\begin{align}
  r=|z|=\sqrt{x^2+y^2}=\sqrt{\Re(z)^2+\Im(z)^2} \label{eq:4.5a}
\end{align}
und durch das \emph{Argument} oder die \emph{Phase}
\begin{align}
  \varphi = \arctan\left(\frac{y}{x}\right)=\arctan\left(\frac{\Im(z)}{\Re(z)}\right) \label{eq:4.5b}
\end{align}
beschrieben werden kann.
\end{subequations}
Diese Art der Darstellung wird \emph{Polar-Darstellung} genannt.

\begin{subequations}
Umrechnung:
\begin{align}
  \Re(z) &= x = r \cos(\varphi) \label{eq:4.6a} \\
  \Im(z) &= y = r \sin(\varphi) \label{eq:4.6b}
\end{align}
oder:
\begin{align}
  z = \underbrace{r \cos(\varphi)}_{x} + i \underbrace{r \sin(\varphi)}_{y} = r (\cos(\varphi)+ i \sin(\varphi)) \label{eq:4.6c}
\end{align}
\end{subequations}

\textit{Bemerkung:} Sollte das Argument im Gradmaß gefragt sein, dann kann man zur Umrechnung die Beziehung \[ 180^{\circ} \hateq \pi \] verwenden. Aus dieser lassen sich die Umrechnungsformeln herleiten
%
\begin{align*}
  \varphi_{\text{rad}} &= \frac{\varphi_{\text{deg}}}{180^{\circ}} \pi \; , \quad \varphi_{\text{deg}} \in [ 0^{\circ},360^{\circ} ) \\
  \varphi_{\text{deg}} &= \frac{\varphi_{\text{rad}}}{\pi} 180^{\circ} \; , \quad \varphi_{\text{rad}} \in [ 0,2\pi )
\end{align*}
%
\textbf{Beispiel:} $-60^{\circ}$ im Bogenmaß.
%
\begin{align*}
  \varphi_{\text{rad}} &= \frac{-60^{\circ}}{180^{\circ}} \pi = -\frac{1}{3} \pi
\intertext{oder man verwendet $-60^{\circ} \hateq 300^{\circ}$:}
  \varphi_{\text{rad}} &= \frac{300^{\circ}}{180^{\circ}} \pi = \frac{5}{3} \pi
\end{align*}
%
Es handelt sich in beiden Fällen um den gleichen Winkel, allerdings von zwei verschiedenen Richtungen aus abgemessen.

\begin{figure}[H]
  \centering
  \begin{pspicture}(-1.5,-1.5)(1.5,1.5)
    \psaxes[labels=none,labelFontSize=\scriptstyle\color{gdarkgray},xAxis=true,yAxis=true,ticks=none]{->}(0,0)(-1.5,-1.5)(1.5,1.5)
    \psarc[linecolor=darkblue]{->}(0,0){0.8}{0}{295}
    \psarc[linecolor=orangered]{<-}(0,0){0.8}{-55}{0}
    \psdot(0.5,-0.866)
    \uput{0.3}[-135](0,0){\color{darkblue} $\varphi_1$}
    \uput{0.3}[-45](0,0){\color{orangered} $\varphi_2$}
  \end{pspicture}
  \vspace*{-2em}
\end{figure}

\subsection{Euler-Form}
\index{Komplexe Zahlen!Euler Form} \label{sec:Euler Form}
Die Schreibweise \eqref{eq:4.6c} führt auf eine weitere Darstellung:
\begin{align*}
  z &\overset{\eqref{eq:4.6c}}{=} r \cos(\varphi) + i r \sin(\varphi)
\\
  &= r (\cos(\varphi)+ i \sin(\varphi))
\\
  &\overset{\eqref{eq:2.6}}{\underset{\eqref{eq:2.7}}{=}} r \Bigg( \sum\limits_{n=0}^{\infty} \underbrace{(\underbrace{-1}_{i^2})^n}_{i^{2n}} \frac{\varphi^{2n}}{(2n)!} + \underbrace{i \sum\limits_{n=0}^{\infty} \underbrace{(-1)^n}_{i^{2n}}}_{i^{2n+1}} \frac{\varphi^{2n+1}}{(2n+1)!} \Bigg)
\\
  &= r \left( \sum\limits_{n=0}^{\infty} \frac{(i\varphi)^{2n}}{(2n)!} + \sum\limits_{n=0}^{\infty} \frac{(i\varphi)^{2n+1}}{(2n+1)!} \right)
\\
  &= r \sum\limits_{n=0}^{\infty} \frac{(i\varphi)^{n}}{n!} = r e^{i\varphi}
\end{align*}
Wir halten also fest:
\begin{align}
  z &= x + iy \nonumber\\
  &= r (\cos(\varphi) + i \sin(\varphi)) \nonumber\\
  &= r e^{i\varphi} \label{eq:4.7}
\end{align}
Der letzte Term heißt \emph{Euler-Darstellung}.

\section{Rechnen mit komplexen Zahlen}\index{Komplexe Zahlen!Rechenregeln}

\begin{itemize}
\item Gleichheit zweier komplexer Zahlen\index{Komplexe Zahlen!Rechenregeln!Gleichheit}
\begin{align*}
  z_1 = z_2 \Longleftrightarrow x_1 = x_2 \text{ und } y_1 =y_2
\end{align*}

\item Addition und Subtraktion\index{Komplexe Zahlen!Rechenregeln!Addition}\index{Komplexe Zahlen!Rechenregeln!Subtraktion}
\begin{align}
  z_1 + z_2 &= x_1 +i y_1 +x_2 +iy_2 \nonumber\\
  &= (x_1+x_2) +i (y_1 +y_2) \label{eq:4.8}
\end{align}
\begin{align*}
  z_1 - z_2 &= (x_1-x_2) +i (y_1-y_2)
\end{align*}

\item Multiplikation\index{Komplexe Zahlen!Rechenregeln!Multiplikation}
\begin{align}
  z_1 \cdot z_2 &= (x_1 +i y_1) (x_2 +iy_2) \nonumber\\
  &= x_1 x_2 +ix_1 y_2 + ix_2 y_1 - y_1 y_2 \nonumber \\
  &= (x_1 x_2 - y_1 y_2) + i(x_1 y_2 + x_2 y_1)
\end{align}
oder mit:
\begin{align}
  z_1 &= r_1 (\cos(\varphi_1) + i \sin(\varphi_1)) = r_1 e^{i \varphi_1} \nonumber \\
  z_2 &= r_2 (\cos(\varphi_2) + i \sin(\varphi_2)) = r_2 e^{i \varphi_2} \nonumber \\
  z_1 \cdot z_2 &= r_1 r_2\ e^{i (\varphi_1 + \varphi_2)} \nonumber \\
  &= r_1 r_2 (\cos(\varphi_1+\varphi_2) + i \sin(\varphi_1+\varphi_2))
\end{align}
\Kasten{Merke}{Bei der Multiplikation werden die Beträge multipliziert und die Argumente addiert.}

\item Komplexe Konjugation\index{Komplexe Zahlen!Komplexe Konjugation}
\begin{align}
  \overline{z} &= \overline{x+iy}=x-iy \label{eq:4.11}
\end{align}
$\overline{z}$ heißt zu $z$ \emph{komplex konjugiert}. Die beiden Zahlen unterscheiden sich nur im Vorzeichen des Imaginärteils.

Dafür gelten folgende Regeln:
\begin{subequations}
\begin{align}
  z+\overline{z} &= x+iy+x-iy = x+x = 2\ \Re(z) \label{eq:4.12a} \\
  z-\overline{z} &= x+iy-x+iy = iy+iy = 2i\ \Im(z) \label{eq:4.12b} \\
  z \cdot \overline{z} &= (x+iy)(x-iy) = x^2-i^2y^2 = x^2+y^2 = r^2 = |z|^2 \label{eq:4.12c}
\end{align}
$|z|^2$ heißt auch Betragsquadrat.\index{Komplexe Zahlen!Betragsquadrat}
\begin{align}
  \overline{z_1+z_2} &= \overline{z_1}+\overline{z_2} \label{eq:4.12d} \\
  \overline{z_1\cdot z_2} &= \overline{z_1}\cdot \overline{z_2} \label{eq:4.12e} \\
  \overline{z_1+i z_2} &\overset{\eqref{eq:4.12d}}{\underset{\eqref{eq:4.12e}}{=}} \overline{z_1} - i \overline{z_2} \label{eq:4.12f}
\end{align}
\end{subequations}

\item Division zweier komplexer Zahlen:\index{Komplexe Zahlen!Rechenregeln!Division}
\begin{subequations}
\begin{align}
  \frac{z_1}{z_2} &= \frac{x_1+iy_1}{x_2+iy_2} = \frac{(x_1+iy_1)(x_2-iy_2)}{x_2^2+y_2^2} = \frac{x_1x_2+y_1y_2+i(x_2y_1-x_1y_2)}{x_2^2+y_2^2} \nonumber \\
  &= \frac{z_1 \overline{z_2}}{|z_2|^2} \label{eq:4.13a} \\
  \frac{z_1}{z_2} &= \frac{r_1\ e^{i\varphi_1}}{r_2\ e^{i\varphi_2}} = \frac{r_1}{r_2} e^{i(\varphi_1 - \varphi_2)} \label{eq:4.13b}
\end{align}
Man spricht vom "`Erweitern mit dem komplex Konjugierten"', oder vom "`Nenner reell umschreiben"'.
\end{subequations}
\end{itemize}

\section{Funktionen mit komplexen Zahlen}
\index{Komplexe Zahlen!Funktionen}

Funktionen mit komplexen Zahlen können über eine Potenzeihe definiert werden --- vgl. die Exponentialfunktion in \autoref{sec:Euler Form}. Ableitungen können wir analog der Ableitung reeller Funktionen definieren (siehe \autoref{chap:Kapitel1})
\begin{align}
  \frac{\dx{}}{\dx{z}}\ z^n &= n\ z^{n-1} \label{eq:4.14}
\end{align}
\begin{align}
  \frac{\dx{}}{\dx{z}}\ e^z &\overset{}{=} \frac{\dx{}}{\dx{z}}\ \sum\limits_{n=0}^{\infty} \frac{1}{n!}\ z^n = \sum\limits_{n=1}^{\infty} \frac{1}{(n-1)!}\ z^{n-1} \nonumber \\
  &= \sum\limits_{n=0}^{\infty} \frac{1}{n!}\ z^n = e^z \label{eq:4.15}
\end{align}

Auf diese Art erhält man für die elementaren Funktionen die gleichen Ableitungen, wie im Reellen.

\subsection{Exponentialfunktion}
\index{Komplexe Zahlen!Funktionen!Exponentialfunktion}

\begin{subequations}
Die Exponentialfunktion ist bereits bekannt aus \autoref{sec:Euler Form}.

Ein paar Eigenschaften und Beziehungen:
\begin{align}
  e^z = e^{x+iy} = \underbrace{e^{x}}_{=r} e^{iy} = r\ e^{iy}
\end{align}
$x$: Realteil bestimmt den Betrag $|e^z| = e^x$\\
$y$: Imaginärteil bestimmt das Argument $\arg(e^z) = y$
\end{subequations}

Betrachte nun den Spezialfall:
\begin{align*}
  &e^{i \varphi} \text{ mit } \varphi \in \mathbb{R} \nonumber\\
  &|e^{i \varphi}| = \sqrt{e^{i \varphi}\ e^{-i \varphi}} = \sqrt{e^{i (\varphi-\varphi)}} = 1 \nonumber\\
  &e^{i \varphi} \text{ liegt auf dem Einheitskreis}
\end{align*}

\begin{figure}[H]
  \centering
  \begin{pspicture}(-4,-0.5)(4,4)
    \psaxes[labels=none,labelFontSize=\scriptstyle\color{gdarkgray} ,xAxis=true,yAxis=true,Dx=1,Dy=1,ticksize=0]{->}(0,0)(-4,-0.5)(4,4)[$\color{gdarkgray} \Re(z)$,-90][$\color{gdarkgray} \Im(z)$,0]
    %\pscircle[linecolor=darkblue,linestyle=dashed](0,0){2.4}
    \psarc[linecolor=darkblue,linestyle=dashed](0,0){2.4}{0}{180}
    \psline[linecolor=orangered](0,0)(2.121320343559643,2.121320343559643)
    \psdots[fillcolor=black,dotsize=0.15](1.5,2.632747685671118)
    \uput[30](1.5,2.632747685671118){$\color{gdarkgray} z=e^{i \varphi}$}
    \uput[180](1.5,1.8){$\color{orangered} r=1$}
  \end{pspicture}
  \vspace*{-2em}
\end{figure}

\begin{align}
&e^{i \pi} = -1 \label{eq:4.17}
\end{align}

\subsection{Trigonometrische Funktionen}
\index{Komplexe Zahlen!Funktionen!Trigonometrische Funktionen}

Mit \eqref{eq:4.7} lassen sich ganz einfach die komplexen trigonometrischen Funktionen ein\-füh\-ren:
\begin{subequations}
\begin{align}
  \cos(z) &= \frac{1}{2}\left( e^{iz} + e^{-iz} \right) \label{eq:4.18a} \\
  \sin(z) &= \frac{1}{2i}\left( e^{iz} - e^{-iz} \right) \label{eq:4.18b} \\
  \tan(z) &= \frac{1}{i}\ \frac{e^{iz} - e^{-iz}}{e^{iz} + e^{-iz}} \label{eq:4.18c}
\end{align}
\end{subequations}

\subsection{Hyperbelfunktionen}
\index{Komplexe Zahlen!Funktionen!Hyperbelfunktionen}
Mit \eqref{eq:4.18a}, \eqref{eq:4.18b} und \eqref{eq:4.18c} sieht man sofort:
\begin{subequations}
\begin{align}
  \cos(iz) &\overset{\eqref{eq:4.18a}}{=} \frac{1}{2}\left( e^{-z} + e^{z} \right) = \frac{1}{2}\left( e^{z} + e^{-z} \right) \nonumber\\
  &= \cosh(z) \label{eq:4.19a} \\
  \sin(iz) &\overset{\eqref{eq:4.18b}}{=} \frac{1}{2i}\left( e^{-z} - e^{z} \right) = \frac{i}{2}\left( e^{z} - e^{-z} \right) \nonumber\\
  &= i\, \sinh(z) \label{eq:4.19b}
\intertext{weiterhin:}
  \tanh(z) &= \frac{\sinh(z)}{\cosh(z)} \label{eq:4.19c} \\
  \coth(z) &= \frac{\cosh(z)}{\sinh(z)} \label{eq:4.19d}
\end{align}
\end{subequations}

\textbf{Beispiele:}
\begin{align*}
  e^{z} + \overline{e^{z}} &= e^{x+iy} + \overline{e^{x+iy}} \\
  &= e^{x+iy} + e^{\overline{x+iy}} \\
  &= e^{x+iy} + e^{x-iy} \\
  &= e^{x} \left(e^{iy} + e^{-iy} \right) \\
  &= 2 e^x \cos(y)
\end{align*}

\begin{align*}
  e^{z} + \overline{e^{-z}} &= e^{x+iy} + e^{-x+iy} \\
  &= 2 e^{iy} \cosh(x)
\end{align*}

\begin{align*}
  \cos(\alpha) \sin(\alpha) &= \frac{1}{4i} \left( e^{i\alpha}+e^{-i\alpha} \right) \left( e^{i\alpha}-e^{-i\alpha} \right) \\
  &= \frac{1}{4i} \left( e^{i2\alpha}-e^{-i2\alpha} \right)\\
  &= \frac{1}{2} \sin(2\alpha)
\end{align*}

Mit Hilfe der komplexen Darstellungen \eqref{eq:4.18a}, \eqref{eq:4.18b} und \eqref{eq:4.18c} lassen sich leicht die Additionstheoreme der trigonometrischen Funktionen beweisen.

\section{Mehrdeutige Lösungen}
\index{Komplexe Zahlen!Mehrdeutige Lösungen}

\subsection{Wurzeln}
\index{Komplexe Zahlen!Mehrdeutige Lösungen!von Wurzeln}

Wir wissen bereits
\begin{align}
  z^2 = -1 \tag*{\eqref{eq:4.1}}
\intertext{hat zwei Lösungen}
  z = \pm i \tag*{\eqref{eq:4.3}}
\end{align}

Wie sieht das für $z^3 = 1$ aus?

Behauptung: Die Lösungen sind:

\begin{tabular}{lll}
$z_1 = 1$, & $z_2 = e^{i \frac{2\pi}{3}}$, & $z_3 = e^{i\frac{4\pi}{3}}$ \\
\\
$z_1^3 = 1$, & $z_2^3 = e^{i \frac{2\pi}{3} \cdot 3} = e^{i 2\pi}$=1, & $z_3^3 = e^{i\frac{4\pi}{3}\cdot 3} = e^{i 4\pi}$=1
\end{tabular}

Woher kommt das?

Wir können jede komplexe Zahl schreiben als
\begin{align}
  w \overset{\eqref{eq:4.7}}{=} r\ e^{i \varphi} \nonumber
\intertext{Die gleiche Zahl wird auch beschrieben durch}
  w \overset{\eqref{eq:4.7}}{=} r\ e^{i (\varphi + 2k\pi)} \; , \quad k \in \mathbb{Z} \label{eq:4.20}
\end{align}
\vspace*{-3em} % % % UGLY!
\begin{subequations}
\begin{align}
\intertext{Die Gleichung $z^n = w = r\ e^{i \varphi} = r\ e^{i (\varphi + 2k\pi)}$ hat die Lösungen}
  z_n = w^{\frac{1}{n}} = \sqrt[n]{r}\ e^{i (\frac{\varphi}{n} + \frac{2k\pi}{n})} \label{eq:4.21a}
\intertext{Das ergibt $n$ verschiedene Lösungen, z.B.}
  k = 0,1,2,\ldots,n-1 \label{eq:4.21b}
\end{align}
\end{subequations}
\textbf{Weiteres Beispiel:}
\begin{align*}
z^4 &= 1\\
\text{Lösungen: } \quad z_1 = 1,\ z_2 = -1 &,\ z_3 = i,\ z_4 = -i
\end{align*}

\subsection{Logarithmus}
\index{Komplexe Zahlen!Mehrdeutige Lösungen!des Logarithmus}

Auch die Lösung von
\begin{align}
  e^z = w \label{eq:4.22}
\end{align}
hat wegen der Vieldeutigkeit von \eqref{eq:4.20} mehr als eine Lösung, nämlich:
\begin{align}
  z &= \ln(w) \overset{\eqref{eq:4.20}}{=} \ln\left( r\ e^{i (\varphi + 2k\pi)} \right) = i (\varphi + 2k\pi) + \ln(r) \nonumber\\
  &= i\varphi + i2k\pi + \ln(r) \label{eq:4.23}
\end{align}
Der Logarithmus hat unendlich viele Lösungen.

Den Teil $\ln(w) = \ln(r) +i\varphi$ mit $0 \leq \varphi < 2\pi$ nennt man \emph{Hauptwert}.
\begin{subequations}
\begin{align}
  \ln(e) &= 1 + i2k\pi && \text{Hauptwert: } 1 \label{eq:4.24a} \\
  \ln(1) &= 0 + i2k\pi  && \text{Hauptwert: } 0 \label{eq:4.24b} \\
  \ln(i) &= \ln\left( e^{i \frac{\pi}{2}} \right) = i \frac{\pi}{2} + i2k\pi  && \text{Hauptwert: } i \frac{\pi}{2} \label{eq:4.24c} \\
  \ln(-1) &= \ln\left( e^{i \pi} \right) =  i \pi + i2k\pi  && \text{Hauptwert: } i \pi \label{eq:4.24d}
\end{align}
\end{subequations}

\section{Riemannsche Blätter}
\index{Komplexe Zahlen!Riemannsche Blätter}

Wir betrachten $z^2 = w $ für $w = r e^{i \varphi} $. Nach \eqref{eq:4.21a} und \eqref{eq:4.21b} gilt:
\begin{align}
  z_1 &= \sqrt{r}\ e^{i\frac{\varphi}{2}} \nonumber\\
  z_2 &= \sqrt{r}\ e^{i\left(\frac{\varphi}{2}+\pi\right)} \label{eq:4.25}
\end{align}

\begin{figure}[H]
  \centering
  \begin{minipage}{0.4\textwidth}
  \centering
    \begin{pspicture}(-2.5,-2.5)(2.5,2.5)
      \psaxes[labels=none,labelFontSize=\scriptstyle\color{gdarkgray} ,xAxis=true,yAxis=true,Dx=1,Dy=1,ticksize=0]{->}(0,0)(-2.5,-2.5)(2.5,2.5)[$\color{gdarkgray} \Re(z)$,-75][$\color{gdarkgray} \Im(z)$,0]
      \pspolygon[fillstyle=hlines,hatchsep=10pt,hatchcolor=redorange,linestyle=none](2,0)(-2,0)(-2,2)(2,2)
      \pspolygon[fillstyle=vlines,hatchsep=10pt,hatchcolor=darkgreen,linestyle=none](2,0)(-2,0)(-2,-2)(2,-2)
      \psarc[linecolor=darkblue,linewidth=2px]{|->}(0,0){1}{0}{180}
      \psarc[linecolor=darkblue,linewidth=2px]{|->}(0,0){1}{180}{0}
      \uput[180](-1,1){$\color{darkblue} z_1$}
      \uput[0](1,-1){$\color{darkblue} z_2$}
    \end{pspicture}
  \end{minipage}
  \hspace*{2em}
  \begin{minipage}{0.4\textwidth}
  \centering
    \begin{pspicture}(-2.5,-2.5)(2.5,2.5)
      \psaxes[labels=none,labelFontSize=\scriptstyle\color{gdarkgray} ,xAxis=true,yAxis=true,Dx=1,Dy=1,ticksize=0]{->}(0,0)(-2.5,-2.1)(2.5,2.5)[$\color{gdarkgray} \Re(w)$,-75][$\color{gdarkgray} \Im(w)$,0]
      %\pscircle[linecolor=yellow,linewidth=2px](0,0){1}
      \pspolygon[fillstyle=hlines,hatchsep=10pt,hatchcolor=redorange,linestyle=none](-2,2)(-2,-2)(2,-2)(2,2)
      \pspolygon[fillstyle=vlines,hatchsep=10pt,hatchcolor=darkgreen,linestyle=none](-2,2)(-2,-2)(2,-2)(2,2)
      \psarc[linecolor=darkblue,linewidth=2px]{|->}(0,0){1}{0}{360}
      \uput[-90](0,-2){\color{darkblue} Kreis $r=$const. $0\leq\varphi\leq 2\pi$}
    \end{pspicture}
  \end{minipage}
\end{figure}

Die "`obere"' $z$-Ebene ($\Im(z)\geq 0$) reicht aus, um über \eqref{eq:4.25} jeden Punkt auf der $w$-Ebene zu erreichen. Das gleiche gilt für die "`untere"' $z$-Ebene.

Damit die Zuordnung trotzdem eindeutig ist, führt man \emph{Riemannsche Blätter} ein.

\begin{figure}[H]
  \centering
  \scalebox{0.9}{
  \begin{pspicture}(-2.5,-3)(14,2.5)
    \psaxes[labels=none,labelFontSize=\scriptstyle\color{gdarkgray} ,xAxis=true,yAxis=true,Dx=1,Dy=1,ticksize=0]{->}(0,0)(-2.5,-2.5)(2.5,2.5)[$\color{gdarkgray} \Re(z)$,-75][$\color{gdarkgray} \Im(z)$,0]
    \pspolygon[fillstyle=hlines,hatchsep=10pt,hatchcolor=redorange,linestyle=none](2,0)(-2,0)(-2,2)(2,2)
    \pspolygon[fillstyle=vlines,hatchsep=10pt,hatchcolor=darkgreen,linestyle=none](2,0)(-2,0)(-2,-2)(2,-2)
    \rput(5.5,0){
    \psaxes[labels=none,labelFontSize=\scriptstyle\color{gdarkgray} ,xAxis=true,yAxis=true,Dx=1,Dy=1,ticksize=0]{->}(0,0)(-2.5,-2.1)(2.5,2.5)[$\color{gdarkgray} \Re(w)$,-75][$\color{gdarkgray} \Im(w)$,0]
    \pspolygon[fillstyle=hlines,hatchsep=10pt,hatchcolor=redorange,linestyle=none](-2,2)(-2,-2)(2,-2)(2,2)
    \uput[-90](0,-2){\color{redorange} 1. Blatt}
    }
    \rput(11,0){
    \psaxes[labels=none,labelFontSize=\scriptstyle\color{gdarkgray} ,xAxis=true,yAxis=true,Dx=1,Dy=1,ticksize=0]{->}(0,0)(-2.5,-2.1)(2.5,2.5)[$\color{gdarkgray} \Re(w)$,-75][$\color{gdarkgray} \Im(w)$,0]
    \pspolygon[fillstyle=vlines,hatchsep=10pt,hatchcolor=darkgreen,linestyle=none](-2,2)(-2,-2)(2,-2)(2,2)
    \uput[-90](0,-2){\color{darkgreen} 2. Blatt}
    }
    \psbezier{->}(1.5,2.1)(2.5,2.5)(3,2.5)(4,2.1)
    \psbezier{->}(1.5,-2.1)(4,-3)(6,-3)(9.5,-2.1)
  \end{pspicture}
  }
\end{figure}
%
%\begin{figure}[H]
%  \centering
%  \begin{minipage}{0.3\textwidth}
%    \centering
%    \begin{pspicture}(-2.5,-2.5)(2.5,2.5)
%      \psaxes[labels=none,labelFontSize=\scriptstyle\color{gdarkgray} ,xAxis=true,yAxis=true,Dx=1,Dy=1,ticksize=0]{->}(0,0)(-2.5,-2.5)(2.5,2.5)[$\Re(z)$,-90][$\Im(z)$,0]
%      \pspolygon[fillstyle=hlines,hatchsep=10pt,hatchcolor=redorange,linestyle=none](2,0)(-2,0)(-2,2)(2,2)
%      \pspolygon[fillstyle=vlines,hatchsep=10pt,hatchcolor=darkgreen,linestyle=none](2,0)(-2,0)(-2,-2)(2,-2)
%    \end{pspicture}
%  \end{minipage}
%  \hspace*{0.5em}
%  \begin{minipage}{0.3\textwidth}
%    \centering
%    \begin{pspicture}(-2.5,-2.5)(2.5,2.5)
%      \psaxes[labels=none,labelFontSize=\scriptstyle\color{gdarkgray} ,xAxis=true,yAxis=true,Dx=1,Dy=1,ticksize=0]{->}(0,0)(-2.5,-2.1)(2.5,2.5)[$\Re(w)$,-90][$\Im(w)$,0]
%      \pspolygon[fillstyle=hlines,hatchsep=10pt,hatchcolor=redorange,linestyle=none](-2,2)(-2,-2)(2,-2)(2,2)
%      \uput[-90](0,-2){\color{redorange} 1. Blatt}
%    \end{pspicture}
%  \end{minipage}
%  \hspace*{0.5em}
%  \begin{minipage}{0.3\textwidth}
%    \centering
%    \begin{pspicture}(-2.5,-2.5)(2.5,2.5)
%      \psaxes[labels=none,labelFontSize=\scriptstyle\color{gdarkgray} ,xAxis=true,yAxis=true,Dx=1,Dy=1,ticksize=0]{->}(0,0)(-2.5,-2.1)(2.5,2.5)[$\Re(w)$,-90][$\Im(w)$,0]
%      \pspolygon[fillstyle=vlines,hatchsep=10pt,hatchcolor=darkgreen,linestyle=none](-2,2)(-2,-2)(2,-2)(2,2)
%      \uput[-90](0,-2){\color{darkgreen} 2. Blatt}
%    \end{pspicture}
%  \end{minipage}
%\end{figure}
%
Anmerkungen:
\begin{itemize}
  \item Die Aufteilung obere/untere Hälfte ist willkürlich. Man kann auch andere Aufteilungen vornehmen.
  \item Die getrennte Zeichung oben ist nicht vollständig. Die Blätter liegen eigentlich übereinander und sind in einem Schnitt verbunden: \emph{Riemannsche Fläche}.
  \item Für $w=0$ gibt es nur eine Lösung $z_1=z_2=0$. An solchen Punkten stoßen die Blätter immer aufeinander. $w=0$ heißt \emph{Verzweigungspunkt} und liegt auf beiden Blättern.
  \item Die $n$-te Wurzel benötigt $n$ Blätter
  \item Der Logarithmus benötigt unendlich viele Blätter.
\end{itemize}
\index{Komplexe Zahlen|)}
