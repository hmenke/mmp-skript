% Holger Cartarius, Henri Menke, 2012 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

\chapter{Gewöhnliche Differentialgleichungen}
\index{Differentialgleichungen|(}

Eine \emph{Differentialgleichung} ist eine Gleichung einer gesuchten Funktion $y(x_i)$, die von einem oder mehreren $x_i$ abhängt und ihre eigenen Ableitungen enthält. Sie drückt eine Abhängigkeit zwischen den Variablen $x_i$, der Funktion $y$ und Ableitungen dieser Funktion aus.

\section{Motivation mit Beispielen aus der Physik}

Newtonsche Bewegungsgleichung:
\begin{align*}
  \frac{\dx{p}}{\dx{t}} \overset{\overset{\text{konst. Masse}}{\downarrow}}{=} m \frac{\dx{}^{2}}{\dx{t}^2} x = m \ddot{x} = F
\end{align*}
Bsp: homogenes Gravitationsfeld
%
\begin{center}
  \begin{pspicture}(-1,-1)(2.5,2.5)
    \psaxes[labels=none,labelFontSize=\scriptstyle ,xAxis=true,yAxis=true,Dx=1,Dy=1,ticksize=0]{->}(0,0)(-1,-1)(2.5,2.5)
    \psline[linecolor=redorange,arrows=->](0.5,2)(0.5,0.3)
    \uput[0](0.5,1){$\color{redorange} F=-m\ g$}
  \end{pspicture}
  \vspace*{-2em}
\end{center}
%
\stepcounter{equation} % Fehler im Skript
\begin{subequations}
\begin{align}
  m \frac{\dx{}^{2}}{\dx{t}^2} x(t) &= -m\ g \label{eq:5.2a}
\intertext{Lösung:}
  x(t) &= -\frac{1}{2}g t^2 + v_0 t +x_0 \label{eq:5.2b}
\end{align}
\end{subequations}

Über den radioaktiven Zerfall ist bekannt, dass die Anzahl der pro Zeitintervall zerfallenden Teilchen proportional zur Teilchenzahl $N$ ist:
\begin{subequations}
\begin{align}
  \frac{\dx{}}{\dx{t}} N(t) = \dot{N}(t) = -\Gamma\ N(t) \label{eq:5.3a}
\intertext{Lösung:}
  N(t) = N_0 e^{-\Gamma\ t} \label{eq:5.3b}
\end{align}
\end{subequations}

Wie findet man die Lösungen? Das wird im Folgenden besprochen.

\section{Ein paar Begriffe}
\index{Differentialgleichungen!Grundbegriffe}
\begin{tabular*}{\textwidth}{lcp{9cm}}
\emph{Ordnung} & : & höchste vorkommende Ableitung \\
\emph{linear} & : & Die Gleichung ist linear in der unbekannten Funktion und allen ihren Ableitungen. \\
\emph{gewöhnlich} & : & Die gesuchte Funktion hängt nur von einer Variablen ab. \\
\emph{partiell} & : & Bsp: $\displaystyle \frac{\partial}{\partial x} f(x,y) + \frac{\partial^2}{\partial y^2} f(x,y) = 0$, Gegenteil von gewöhnlich. \\
\end{tabular*}

Hier werden nur gewöhnliche DGL betrachtet.

\Kasten{Bemerkung}{DGL := Differentialgleichung(en)}

\section{Differentialgleichungen 1. Ordnung}
\subsection{Lineare Differentialgleichungen 1. Ordnung}
\index{Differentialgleichungen!1. Ordnung!linear}

Sind von der Form
\begin{align}
y'(x) &= a(x)\ y(x) + b(x) \label{eq:5.4}
\end{align}
Es gibt Standardverfahren diese DGL zu lösen. Dazu gehören:

\subsubsection{\texorpdfstring{$a(x)=0$}{a(x)=0}, einfaches Integral}
\begin{subequations}
\begin{align}
  y'(x) &\overset{\eqref{eq:5.4}}{\underset{a(x) = 0}{=}} b(x) \label{eq:5.5a}
\intertext{Lösung:}
  y(x) &= \int b(x) \dx{x} \nonumber\\
  &= B(x) + c \label{eq:5.5b} \\
  B(x) &= \text{Stammfunktion} \nonumber\\
  c &= \text{Integrationskonstante, wird z.B. durch} \nonumber \\
  &\phantom{=\ } \text{ eine Anfangsbedingung festgelegt.} \nonumber
\end{align}
\end{subequations}

\textbf{Beispiel:} Fall im Schwerefeld
\begin{subequations}
\begin{align}
  \dot{v} &\overset{\eqref{eq:5.2a}}{=} -g \text{ , mit } v(0) = 5 \frac{m}{s} \label{eq:5.6a} \\
  v(t) &= v_0 - g\ t \label{eq:5.6b} \\
  v(0) &= v_0 \overset{!}{=} 5 \frac{m}{s} \label{eq:5.6c} \\
  \text{Also: } v(t) &\overset{\eqref{eq:5.6b}}{\underset{\eqref{eq:5.6c}}{=}} -g\ t + 5 \frac{m}{s} \nonumber
\end{align}
\end{subequations}

\subsubsection{\texorpdfstring{$b(x)=0$}{b(x)=0}, homogene DGL}
Enthält die DGL \textit{keinen} Term, in dem y oder seine Ableitungen nicht vorkommen, heißt sie \emph{homogen}.
\begin{subequations}
\begin{align}
  y'(x) = a(x)\ y(x) \label{eq:5.7a}
\end{align}
Dies können wir einfach lösen:
\begin{align}
  {\color{darkgreen} \int} \frac{y'(x)}{y(x)} {\color{darkgreen} \dx{x}} &= {\color{darkgreen} \int} a(x) {\color{darkgreen} \dx{x}} \nonumber\\
  \ln|y(x)| &= A(x) + \tilde{c} \quad A(x) \text{: Stammfunktion} \nonumber\\
  y(x) &= \pm e^{A(x) + \tilde{c}} \nonumber\\
  &= c\ e^{A(x)} \quad c = \pm e^{\tilde{c}} \label{eq:5.7b}
\end{align}
\end{subequations}
\textbf{Radioaktiver Zerfall} [\textit{siehe} \eqref{eq:5.3a}, \eqref{eq:5.3b}]
\begin{align*}
  \frac{\dx{}}{\dx{t}} N(t) &= \underbrace{- \Gamma}_{a(t)} N(t) \; , \quad N(0) = N_0 \\
  A(t) &= -\Gamma t + \tilde{c} \\
  N(t) &\overset{\eqref{eq:5.7b}}{\underset{\eqref{eq:5.3a}}{=}} c\ e^{-\Gamma t} \\
  N(0) &= c \overset{!}{=} N_0
\end{align*}
\begin{align*}
  \text{Also: } N(t) &= N_0\ e^{-\Gamma t}
\end{align*}

Anmerkung: Man kann vereinfacht schreiben:
\begin{align*}
  y'(x) &= a(x)\ y(x) = \frac{\dx{y}}{\dx{x}}
\end{align*}
Umgeformt:
\begin{align}
  \int \frac{\dx{y}}{y} &= \int a(x) \dx{x} \label{eq:5.8}
\end{align}
Diese Schreibweise nennt man auch \emph{Separation der Variablen}. Dies ist aber nur als verkürzte Schreibweise zu verstehen, die zwar immer funktioniert, \eqref{eq:5.7b} ist aber der eigentliche Lösungsweg.

\subsubsection{Inhomogene DGL}
Für das vollständige Problem \eqref{eq:5.4} führt der Weg über die \emph{Variation der Konstanten} zum Ziel einer allgemeingültigen Form.

\textbf{Ansatz:}
\begin{subequations}
\begin{align}
  &y_p(x) = \underbrace{c(x)}_{\mathclap{\text{Variable statt Konstante}}} \overbrace{e^{A(x)}}^{\mathclap{\text{Lösung der zugehörigen homogenen DGL}}} , A(x) = \int a(x) \dx{x} \label{eq:5.9a}
\end{align}

Nach dem Einsetzen in \eqref{eq:5.4} bleibt:
\begin{align}
  c'(x) = b(x) e^{-A(x)} \label{eq:5.9b}
\intertext{oder}
  c(x) = \int b(x) e^{-A(x)} \dx{x} \label{eq:5.9c}
\intertext{oder}
  y_p(x) \overset{\eqref{eq:5.9a}}{\underset{\eqref{eq:5.9c}}{=}} e^{A(x)} \int b(x) e^{-A(x)} \dx{x} \label{eq:5.9d}
\end{align}

$y_p(x)$ ist \textit{eine} spezielle (\emph{partikuläre}) Lösung von \eqref{eq:5.4}. Wir können auf diese jede Lösung $y_h(x)$ der zugehörigen homogenen DGL \eqref{eq:5.7a} addieren, denn
\begin{align*}
  (y_h(x) + y_p(x))' &= y_h'(x)+y_p'(x) \\
  &= \underbrace{a(x)\ y_h(x)}_{\overset{\eqref{eq:5.7a}}{=} y_h'(x)} + \underbrace{a(x)\ y_p(x) + b(x)}_{\overset{\eqref{eq:5.4}}{=}y_p'(x)} = a(x)\ (y_h(x)+y_p(x)) + b(x)
\end{align*}

Also lautet die allgemeine Lösung von \eqref{eq:5.4}:
\begin{align}
  y(x) \overset{\eqref{eq:5.7b}}{\underset{\eqref{eq:5.9d}}{=}} c e^{A(x)} + e^{A(x)} \int b(x)\ e^{-A(x)} \dx{x} \label{eq:5.9e}
\end{align}
\end{subequations}

\textbf{Beispiel:}
\begin{align*}
  y'(x) &= \underbrace{-2x}_{a(x)} y(x) + \underbrace{4x}_{b(x)} \\
  \text{daraus folgt: } A(x) &= -x^2 \\
  y_h(x) &= c e^{-x^2} \\
  y_p(x) &= e^{-x^2} \underbrace{\int 4x\ e^{x^2} \dx{x}}_{2\ e^{x^2}} = 2 \\
  \text{Lösung: } y(x) &= 2 + c\ e^{-x^2}
\end{align*}

\subsection{Nichtlineare DGL 1. Ordnung}
\index{Differentialgleichungen!1. Ordnung!nichtlinear}

Nichtlineare DGL 1. Ordnung sind von der  Form $y'(x) = F(x,y)$ mit beliebig kompliziertem $F$. Im Allgemeinen existiert keine geschlossene Lösung. Ein allgemeingültiges Lösungsverfahren gibt es also nur in Spezialfällen, welche wir im Folgenden behandeln werden.

\subsubsection{Separable Gleichungen}
\stepcounter{equation} % Fehler im Skript
\begin{align}
  \text{Form: } y'(x) &= f(x)\ g(y) \label{eq:5.11}
\end{align}
\begin{align}
  \text{Umformen: } \frac{\dx{y}}{\dx{x}} &= f(x)\ g(y) \nonumber\\
  \int \frac{\dx{y}}{g(y)} &= \int f(x) \dx{x} \label{eq:5.12}
\end{align}

Seien die Stammfunktionen:
\begin{subequations}
\begin{align}
  \tilde{G}(y) &= \int \frac{\dx{y}}{g(y)} \label{eq:5.13a}
\intertext{und}
  F(x) &= \int f(x) \dx{x} \label{eq:5.13b}
\intertext{bekannt. Dann lautet die Lösung zu \eqref{eq:5.11}:}
  y(x) &= \tilde{G}^{-1}(F(x)+c) \label{eq:5.13c}
\end{align}
Für diese Darstellung muss $\tilde{G}$ umkehrbar sein.
\end{subequations}

\textbf{Beispiel:}
\begin{align*}
  \overbrace{y'(x)}^{=\frac{\dx{y}}{\dx{x}}} &= x + xy^2 = \overbrace{x}^{f(x)} \overbrace{(1+y^2)}^{g(y)} \\
  \int \frac{\dx{y}}{1 + y^2} &= \int x \dx{x} \\
  \arctan(y) &= \frac{1}{2} x^2 + c \\
  y(x) &= \tan\left(\frac{1}{2} x^2 + c\right)
\end{align*}

\subsubsection{Geschickte Substitution}

Gelegentlich lässt sich eine DGL durch eine günstige \emph{Substitution} auf eine bekannte, lösbare Form bringen.
\index{Differentialgleichungen!1. Ordnung!Bernoulli-DGL}

\textbf{Beispiel:} Bernoulli-DGL
\begin{align}
y' &= a(x)\ y + b(x)\ y^\alpha \quad , \alpha \in \mathbb{R}, \alpha \neq 0,1 \label{eq:5.14}
\end{align}
\begin{subequations}
Substitution:
\begin{align}
  z(x) &= (y(x))^{1-\alpha} \label{eq:5.15a} \\
  z'(x) &= (1-\alpha)\ (y(x))^{-\alpha}\ y'(x) \nonumber
\intertext{oder}
  y(x)^{-\alpha}\ y'(x) &= \frac{1}{1-\alpha} z'(x) \label{eq:5.15b}
\intertext{Multiplikation von \eqref{eq:5.14} mit $y^{-\alpha}$.}
  \underbrace{y'(x)\ (y(x))^{-\alpha}}_{\overset{\eqref{eq:5.15b}}{=}\frac{z'(x)}{1-\alpha}}  &= a(x) \underbrace{(y(x))^{1-\alpha}}_{\overset{\eqref{eq:5.15a}}{=}z(x)} + b(x) \label{eq:5.15c} \\
  z'(x) &= (1-\alpha) \left(a(x)\ z(x) + b(x)\right) \label{eq:5.15d}
\end{align}
Auf diesem Weg können wir eine DGL der Bernoulli-Form \eqref{eq:5.14} immer zu einer linearen DGL umformen, die mit den Methoden des vorherigen Abschnitts lösbar ist.
\end{subequations}

\section{Lineare Differentialgleichungen höherer Ordnung}
\index{Differentialgleichungen!höhere Ordnung!linear}

Allgemeine Form:
\begin{align}
  \sum\limits_{i=1}^{N} a_i(x) y^{(i)}(x) &= b(x) \label{eq:5.16}
\end{align}

\subsection{Eigenschaften der Lösungen}

Aus der Form \eqref{eq:5.16} erkennt man, dass wenn die $y_i(x)$ mit $i=1,\ldots,n$ Lösungen der homogenen DGL zu \eqref{eq:5.16} mit $b(x)=0$ sind, so auch
\begin{align}
  y_h(x) = \sum\limits_{i=1}^{n} c_i\ y_i(x) \; , \quad c_i \in \mathbb{C} \label{eq:5.17}
\end{align}

Sind die $y_i(x)$ alle unabhängigen Lösungen von \eqref{eq:5.16}, heißt $y_h(x)$ \emph{allgemeine Lösung} der homogenen DGL.

Ist für $b(x) \neq 0$ (inhomogene DGL) $y_p(x)$ eine partikuläre Lösung der DGL, so löst auch
\begin{align}
  y_a(x) = y_h(x) + y_p(x) \overset{\eqref{eq:5.17}}{=} \sum\limits_{i} c_i\ y_i(x) + y_p(x) \label{eq:5.18}
\end{align}
die DGL \eqref{eq:5.16}.

Haben wir eine DGL von der Form \eqref{eq:5.16}, müssen wir also $y_h(x)$ und $y_p(x)$ bestimmen, um ihre Lösungen zu erhalten.

Mit nicht konstanten Koeffizienten $a(x)$ kann es beliebig kompliziert werden. Daher beschränken wir uns auf konstante Koeffizienten.

\subsection{Konstante Koeffizienten}
\index{Differentialgleichungen!höhere Ordnung!konstante Koeffizienten}

Form:
\begin{align}
  \sum\limits_{i=1}^{N} \underbrace{a_i}_{\mathrlap{\text{\hspace*{-1em} Hängen nicht von $x$ ab.}}}\ y^{(i)}(x) = b(x) \label{eq:5.19}
\end{align}

Vorgehen in Schritten:
\renewcommand{\theenumi}{\bfseries \roman{enumi}}
\renewcommand{\labelenumi}{(\theenumi)}
\begin{enumerate}
  \item Bestimme $y_h(x)$
  \item Bestimme ein $y_p(x)$
  \item Die gesuchte Lösung ist \eqref{eq:5.18}
\end{enumerate}

\subsubsection{homogene DGL}
Das heißt \eqref{eq:5.19} mit $b(x) = 0$:
%
\begin{align}
  \sum\limits_{i=1}^{N} a_i\ y^{(i)}(x) = 0 \label{eq:5.20}
\end{align}

Es gibt eine Standardmethode, die prinzipiell immer funktioniert.

Ansatz: $y(x) = e^{\lambda x}$

Einsetzen in \eqref{eq:5.20}:
\begin{align*}
  \sum\limits_{i=1}^{N} a_i\ \lambda^{i} e^{\lambda x} = 0
\end{align*}
$e^{\lambda x} \neq 0$, daher Division erlaubt:
\begin{align}
  \sum\limits_{i=1}^{N} a_i\ \lambda^{i} = a_{N} \lambda^{N} + a_{N-1} \lambda^{N-1} + \ldots + a_{1} \lambda + a_0 = 0 \label{eq:5.21}
\end{align}

Das Problem reduziert sich also auf die Lösung des Polynoms \eqref{eq:5.21}. Es heißt \emph{charakteristisches Polynom}. Auf diesem Weg erhalten wir alle $N$ unabhängigen Lösungen von \eqref{eq:5.20}. Wir unterscheiden zwei Fälle: \index{Differentialgleichungen!höhere Ordnung!charakteristisches Polynom}
\begin{enumerate}
\item Falls alle $N$ Lösungen von \eqref{eq:5.21} verschieden sind, sind bereits alle $y_i(x) = e^{\lambda_i x}$ linear unabhängig und die allgemeine Lösung hat die Form
\begin{align}
  y_h(x) = \sum\limits_{i=1}^{N} c_i\ e^{\lambda_i x} \; , \quad c_i \in \mathbb{C}. \label{eq:5.22}
\end{align}

\item Es gibt mehrfache Nullstellen des Polynoms, das heißt einige $\lambda_i$ haben den gleichen Wert und die zugehörigen Lösungen $e^{\lambda_i x}$ sind nicht unabhängig.

In diesem Fall gilt:

Ist $\lambda_i$ $k_i$-fache Nullstelle, dann lautet die Lösung:
\begin{align}
  y_h(x) = \sum\limits_{i=1}^{M} \left( \sum\limits_{j=0}^{k_i - 1} c_{i,j}\ x^{j} \right) e^{\lambda_i x} \; , \quad c_{i,j} \in \mathbb{R} \label{eq:5.23}
\end{align}
$M$ ist die Zahl der unabhängigen (verschiedenen) Nullstellen von \eqref{eq:5.21}.
\end{enumerate}

\textbf{Beispiel:}
\begin{align*}
  y^{(6)}(x) - 10 y^{(5)}(x) + 40 y^{(4)}(x) - 82 y^{(3)}(x) + 91 y''(x) - 52 y'(x) + 12 y(x) &= 0
\end{align*}
charakteritisches Polynom:
\begin{align*}
  \lambda^{6} - 10 \lambda^{5} + 40 \lambda^{4} - 82 \lambda^{3} + 91 \lambda^{2} - 52 \lambda + 12 &= 0 \\
  &= (\lambda_3 - 3) (\lambda_2 - 2)^{2} (\lambda_1 - 1)^{3}
\end{align*}
\begin{tabular}{ll}
$\lambda_1 = 1$ & ist dreifache Nullstelle \\
$\lambda_2 = 2$ & ist doppelte Nullstelle \\
$\lambda_3 = 3$ & ist einfache Nullstelle
\end{tabular}

\underline{Lösung:}
\begin{align*}
  y_h(x) = (c_1 + c_2\ x+c_3\ x^2)\ e^{x} + (c_4 + c_5\ x)\ e^{2x} + c_6\ e^{3x}
\end{align*}

\textbf{Besonderheit bei reellen Koeffizienten:}
\index{Differentialgleichungen!höhere Ordnung!Besonderheit bei reellen Koeffizienten}

\begin{subequations}
Sind \emph{alle} Koeffizienten $a_i$ der DGL \eqref{eq:5.20} reell und gibt es komplexe Lösungen $\lambda_i = \alpha_i + i\, \beta_i$ des charakteristischen Polynoms \eqref{eq:5.21}, dann ist auch die konjugiert-komplexe Zahl $\overline{\lambda_i} = \alpha_i - i\, \beta_i$ eine Lösung:
\begin{align}
  &\sum\limits_{j=0}^{N} a_j\, \overline{\lambda}_{i}^{\,j}=0 \nonumber
\intertext{Diese beiden Lösungen lassen sich dann schreiben als}
  &c_i\, e^{\lambda_i\, x} + \tilde{c_i}\, e^{\overline{\lambda_i}\, x} \nonumber\\
  =\ &c_i\, e^{\alpha_i\, x+i\, \beta_i\, x} + \tilde{c_i}\, e^{\alpha_i\, x-i\, \beta_i\, x} \nonumber\\
  =\ &e^{\alpha_i\, x} \left(c_i\, e^{i\, \beta_i\, x} + \tilde{c_i}\, e^{-i\, \beta_i\, x}\right) \nonumber\\
  =\ &e^{\alpha_i\, x} \ A \ \cos(\beta_i\, x + \varphi) \nonumber\\
  =\ &e^{\alpha_i\, x} \ A \ \sin(\beta_i\, x + \tilde{\varphi}) \label{eq:5.24a}
\end{align}

Das heißt, die Lösungen können durch $\sin(x)$ und $\cos(x)$ ausgedrückt werden. Diese sind unabhängig.

Äquivalent zu \eqref{eq:5.24a} ist:
\begin{align}
y_h(x) &= \ldots + e^{\alpha_i\, x} \left(A \ \cos(\beta_i\, x) + B \ \sin(\beta_i\, x) \right) \label{eq:DGLhoehereOrdnungKoeffA6}
\end{align}
\end{subequations}

\subsubsection{inhomogene DGL} \label{sec:inhomogene DGL}

Wenn für die DGL \eqref{eq:5.16} die Lösung der zugehörigen homogenen DGL der Form \eqref{eq:5.20} bekannt ist, reicht es \textit{eine} partikuläre Lösung von \eqref{eq:5.16} zu kennen. Es gibt kein allgemeines Verfahren diese partikuläre Lösung zu bestimmen. Vielfach sind die Ausdrücke $b(x)$ aber so einfach, dass leicht eine Lösung, oder zumindest ein Ansatz, erraten werden kann.

Immer einen Versuch wert sind:
\begin{subequations}
\begin{enumerate}
\item für $b(x) = B\, e^{\alpha x}$
\label{itm:VersuchA}
\begin{align}
  \text{Ansatz: } y_p(x) = A\, e^{\alpha x}
  \label{eq:5.25a}
\end{align}

\item für $b(x) = B_1\, \cos(\alpha x) + B_2\, \sin(\alpha x)$
\label{itm:VersuchB}
\begin{align}
  \text{Ansatz: } y_p(x) = A_1\, \cos(\alpha x) + A_2\, \sin(\alpha x)
  \label{eq:5.25b}
\end{align}

\item für $b(x) = B_0 + B_1\, x + B_2\, x^2 + \ldots + B_n\, x^n$
\label{itm:VersuchC}
\begin{align}
  \text{Ansatz: } y_p(x) = A_0 + A_1\, x + A_2\, x^2 + \ldots + A_n\, x^n
  \label{eq:5.25c}
\end{align}

\item \label{itm:VersuchD} Ist $b(x)$ eine Summe oder ein Produkt der Formen \ref{itm:VersuchA}, \ref{itm:VersuchB} oder \ref{itm:VersuchC}, kann man für den Ansatz eine Summe oder ein Produkt der gleichen Form versuchen.

\item Kommt einer der Terme aus \ref{itm:VersuchA}, \ref{itm:VersuchB}, \ref{itm:VersuchC} und \ref{itm:VersuchD} bereits in der homogenen Lösung $y_h(x)$ vor, kann man versuchen, im Ansatz den entsprechen Term mit der kleinsten Potenz von $x$ zu multiplizieren, sodass der Term sich von allen homogenen Lösungen unterscheidet.
\end{enumerate}
\end{subequations}
\renewcommand{\theenumi}{\arabic{enumi})}

\textbf{Beispiel:}
\begin{align*}
  b(x) &= e^{2x},\ y_h(x) = a\, e^{2x} + b\, e^{-3x} \\
  y_p(x) &= B\, x\, e^{2x}
\end{align*}

\subsubsection{Beispiele}
DGL eines getriebenen, gedämpften harmonischen Oszillators:
\stepcounter{equation} %Mehrere Fehler im Skript
\begin{align}
  \ddot{x}(t) + 2 \gamma\, \dot{x}(t) + \omega_0^2 \, x(t) = f(t)
  \label{eq:5.27}
\end{align}
Wo kommt das in der Physik vor?

\textbf{Beispiel:} Federpendel im Ölbad.
\begin{figure}[H]
  \centering
  \begin{pspicture}(0,0)(5,5)
    %Öl
    \psplot[linecolor=redorange]{0}{5}{0.1*sin(5*x)+1.7}
    %Decke
    \psline(0,5)(5,5)
    %Wanne
    \psline(0,2)(0,0)(5,0)(5,2)
    %Feder
    \psline(2.5,5)(2.5,4.8)
    \pscurve(2.5,4.8)(2.4,4.7)(2.5,4.6)(2.6,4.5)(2.5,4.4)(2.4,4.3)(2.5,4.2)(2.6,4.1)(2.5,4)(2.4,3.9)(2.5,3.8)(2.6,3.7)(2.5,3.6)(2.4,3.5)(2.5,3.4)(2.6,3.3)(2.5,3.2)(2.4,3.1)(2.5,3)(2.6,2.9)(2.5,2.8)(2.4,2.7)(2.5,2.6)(2.6,2.5)(2.5,2.4)(2.4,2.3)(2.5,2.2)(2.6,2.1)(2.5,2)(2.4,1.9)(2.5,1.8)(2.6,1.7)(2.5,1.6)(2.4,1.5)(2.5,1.4)(2.6,1.3)(2.5,1.2)
    %Körper
    \psline(2,1.2)(3,1.2)(3,0.5)(2,0.5)(2,1.2)
    %Rolle und Seil
    \psline[linecolor=darkgreen](2.7,1.2)(2.7,4.7)(2.85,4.85)(5,4.85)
    \psdots[dotscale=3,dotstyle=o,dotsize=3pt 0,linecolor=darkgreen,fillcolor=darkgreen](2.85,4.7)
    \uput[-90](4.4,4.85){\color{darkgreen} Motor}
  \end{pspicture}
\end{figure}

Öl: geschwindigkeitsabhängige Reibungskraft
\begin{align*}
  F_R &= -\tilde{\gamma} v
\intertext{Dann lauten die Konstanten für \eqref{eq:5.27}:}
  \gamma &= \frac{\tilde{\gamma}}{2m} \\
  f &= \frac{F(t)}{m} && \text{(Kraft des Motors)} \\
  \omega_0^2 &= \frac{D}{m} && D = \text{Federkonstante}
\end{align*}

\begin{figure}[H]
  \begin{flushleft}
    \textbf{Beispiel:} Elektrischer Schwingkreis
  \end{flushleft}
  \centering
  \begin{pspicture}(0,0)(5,6)
    %Leiterbahnen
    \psline(0.4,2.5)(0.4,0.5)(3,0.5)(3,1.5)
    \psline(3,3.5)(3,5)(2.4,5)
    \psline(1,5)(0.4,5)(0.4,3)
    %Induktion
    \pscurve(3,3.5)(2.9,3.4)(3,3.3)(3.1,3.2)(3,3.1)(2.9,3)(3,2.9)(3.1,2.8)(3,2.7)(2.9,2.6)(3,2.5)(3.1,2.4)(3,2.3)(2.9,2.2)(3,2.1)(3.1,2)(3,1.9)(2.9,1.8)(3,1.7)(3.1,1.6)(3,1.5)
    %Widerstand
    \psline(2.4,5.5)(2.4,4.5)(1,4.5)(1,5.5)(2.4,5.5)
    %Kondensator
    \psline(0.0,2.5)(0.8,2.5)
    \psline(0.0,3)(0.8,3)
    %Leiterbahnen
    \psline(4,3.5)(4,5)(4.8,5)
    \psline(4,1.5)(4,0.5)(4.8,0.5)
    \psdots[dotstyle=o,dotscale=1.5](4.8,0.5)
    \psdots[dotstyle=o,dotscale=1.5](4.8,5)
    %Induktion
    \pscurve(4,3.5)(3.9,3.4)(4,3.3)(4.1,3.2)(4,3.1)(3.9,3)(4,2.9)(4.1,2.8)(4,2.7)(3.9,2.6)(4,2.5)(4.1,2.4)(4,2.3)(3.9,2.2)(4,2.1)(4.1,2)(4,1.9)(3.9,1.8)(4,1.7)(4.1,1.6)(4,1.5)
    %Beschriftung
    \uput[90](3.5,2.5){$\color{gdarkgray} L$}
    \uput[90](3.5,0.5){$\color{gdarkgray} \uparrow$}
    \uput[-90](3.5,0.4){\color{gdarkgray} \scriptsize induktive Kopplung}
    \uput[0](1.3,5){$\color{gdarkgray} R$}
    \uput[0](0.8,2.75){$\color{gdarkgray} C$}
    \uput[0](4.5,4){\color{gdarkgray} Strom}
  \end{pspicture}
\end{figure}

\begin{align*}
  L \ddot{I} + R\, \dot{I} + \frac{1}{C}\, I &= I_{\text{err}}(t) \\
  \ddot{I} + \underbrace{\frac{R}{L}}_{2\gamma}\, \dot{I} + \underbrace{\frac{1}{L\,C}}_{\omega_0^2}\, I &= \underbrace{\frac{I_{\text{err}}(t)}{L}}_{f(t)}
\end{align*}

Lösung der homogenen DGL für $\gamma < \omega_0$ (s. Übungen):
\begin{align}
  x_h(t) = e^{-\gamma t} (a\, e^{i \omega t} + b\, e^{-i \omega t}) && \text{mit } \omega = \sqrt{\omega_0^2 - \gamma^2}
  \label{eq:5.28}
\end{align}

Beispiel für eine äußere Anregung:
\begin{subequations}
\begin{align}
  f(t) = A\, \cos(\omega_0 t) \nonumber \\
  \text{Ansatz: } x_p(t) = a\, \cos(\omega_0 t) + b\, \sin(\omega_0 t) \label{eq:5.29a}
\intertext{Ableiten und Einsetzen in \eqref{eq:5.27}:}
  -a \omega_0^2 \cos(\omega_0 t) -b \omega_0^2 \sin(\omega_0 t) - 2 \gamma a \omega_0 \sin(\omega_0 t) \quad & \nonumber \\
  + 2 \gamma b \omega_0 \cos(\omega_0 t) + a \omega_0^2 \cos(\omega_0 t) + b \omega_0^2 \sin(\omega_0 t) &\overset{!}{=} A \cos(\omega_0 t) \nonumber \\
  - 2 \gamma a \omega_0 \sin(\omega_0 t) + 2 \gamma b \omega_0 \cos(\omega_0 t) &\overset{!}{=} A \cos(\omega_0 t)
  \label{eq:5.29b}
\end{align}

\begin{align}
\intertext{\emph{Koeffizientenvergleich} \index{Differentialgleichungen!höhere Ordnung!Koeffizientenvergleich}}
\intertext{\fbox{$\cos(\omega_0 t)$}}
  2 \gamma \omega_0 b &= A \nonumber \\
  \rightarrow b &= \frac{A}{2 \gamma \omega_0}
  \label{eq:5.29c}
\end{align}
\begin{align}
\intertext{\fbox{$\sin(\omega_0 t)$}}
  2 \gamma \omega_0 a &= 0 \nonumber \\
  \rightarrow a &= 0
  \label{eq:5.29d}
\end{align}

\begin{align}
  x_p(t)
  \overset{\eqref{eq:5.29a}}
  {
  \underset{\substack{\eqref{eq:5.29c} \\ \eqref{eq:5.29d}}}
  {=}
  }
  \frac{A}{2 \gamma \omega_0}\, \sin(\omega_0 t)
  \label{eq:5.29e}
\end{align}
Die allgemeine Lösung der inhomogenen DGL lautet also:
\begin{align}
  x_a(t) \overset{\eqref{eq:5.28} }{\underset{\eqref{eq:5.29e}}{=}}
  e^{-\gamma t} (a\, e^{i \omega t} + b\, e^{-i \omega t}) + \frac{A}{2 \gamma \omega_0} \sin(\omega_0 t)
  \label{eq:5.29f}
\end{align}
\end{subequations}

\subsubsection{Integrationskonstanten}

Die Lösungen \eqref{eq:5.22} bzw. \eqref{eq:5.23} einer DGL $n$-ter Ordnung enthalten $N$ Integrationskonstanten $c_i$. Diese werden festgelegt durch $N$ Bedingungen. Das führt uns auf das \emph{Anfangswertproblem}:
\begin{align*}
  y(0) = A_1,\ y'(0)=A_2 ,\ \ldots ,\ y^{(N-1)}(0)=A_{N-1}
\end{align*}
oder das \emph{Randwertproblem}:
\begin{align*}
  y(0) = A_1,\ y'(x_1)=A_2 ,\ \ldots
\end{align*}

\subsubsection{Zusammenfassung}

Lösungsschema:
\renewcommand{\theenumi}{{\bfseries \arabic{enumi}.}}
\begin{enumerate}
\item Homogene DGL lösen:

$e^{\lambda x}$ funkioniert immer --- aber nur im \textit{homogenen} Fall und nur für \textit{lineare} DGL mit \textit{konstanten} Koeffizienten.

\item Eine partikuläre Lösung der inmhomogenen DGL bestimmen:

Siehe \autoref{sec:inhomogene DGL}

\item Die allgemeine Lösung der DGL ergibt sich durch:

$y_a(x) = y_h(x) + y_p(x)$

\item Anfangs- oder Randbedingungen berücksichtigen.
\end{enumerate}
\renewcommand{\theenumi}{\arabic{enumi})}

\section{Greensche Funktion}
\label{sec:Greensche Funktion}
\index{Differentialgleichungen!Greensche Funktion}
\subsection{Allgemeine Betrachtung}

Wir betracheten eine inhomogene, lineare DGL:
\begin{align*}
  \sum\limits_{i=0}^{N} a_i\, y^{(i)}(x) = b(x) \tag*{\eqref{eq:5.19}}
\end{align*}
von der die Lösung $y_h(x)$ der zugehörigen homogenen DGL bekannt sei.

Das folgende Verfahren dient dazu, die Lösung von \eqref{eq:5.19} zu finden, die mit einem Satz gewählter Anfangs- oder Randbedingungen verträglich ist.

Wir suchen dazu eine Funktion $G(x,z)$, sodass wir die Lösung berechnen können als
%
\begin{align}
  y(x) = \int\limits_{a}^{b} G(x,z)\, b(z) \dx{z},
  \label{eq:5.30}
\end{align}
%
d.h. wenn wir $G(x,z)$ bestimmt haben, können wir für jede beliebige Inhomogenität $b(x)$ die Lösung einfach nach \eqref{eq:5.30} berechenen. Dies gilt nur für die Lösung im Intervall $a \leq x \leq b$.

Versuch: Einsetzen \eqref{eq:5.30} in \eqref{eq:5.19}
%
\begin{align*}
  \int\limits_{a}^{b} \underbrace{\sum\limits_{i=0}^{N} a_i\, \frac{\dx{}^i}{\dx{x}^i} G(x,z)}_{\delta(z-x)}\, b(z) \dx{z} = b(x),
\end{align*}
%
falls das der $\delta$-Funktion entspricht ist die Gleichheit erfüllt.

Wir sehen also $G(x,z)$ muss Lösung sein von
\begin{align}
  \sum\limits_{i=0}^{N} a_i\, \frac{\dx{}^i}{\dx{x}^i} G(x,z) = \delta(z-x)
  \label{eq:5.31}
\end{align}

Weiterhin muss $G(x,z)$ folgende Bedingungen erfüllen:
\begin{itemize}
\item $G(x,z)$ muss so gewählt sein, dass $y(x)$ die gewünschten Rand- oder Anfangswerte erfüllt. Also, wenn $y(x_j) = 0$ für bestimmte $x_j$, dann auch $G(x_j,z) = 0$.

\item Gleichung \eqref{eq:5.31} lässt sich nur erfüllen, wenn $\left. \frac{\dx{}^N}{\dx{x}^N} G(x,z) \right|_{x=z} \rightarrow \infty$, bei den niedrigeren Ableitungen macht das keinen Sinn.

\item Das ist möglich, wenn die Ableitungen wie folgt aussehen:

\begin{figure}[H]
  \centering
  \begin{pspicture}(-0.5,-2.2)(5,3.2)
    \psaxes[labels=none,labelFontSize=\scriptstyle ,xAxis=true,yAxis=true,ticksize=0 0]{->}(0,0)(-0.5,-2.2)(5,3.2)%[$t$,-90][$v(t)$,180]
    \pscurve(0,1)(0.2,1.1)(1,1.5)(1.8,2.7)(2,3)
    \psline[linestyle=dotted](2,3.2)(2,-2.2)
    \pscurve(2,-2)(2.2,-1.9)(3,-1.5)(3.8,-0.3)(4,0)
    \uput[-45](2,0){$\color{gdarkgray} z$}
  \end{pspicture}
  \hspace*{1em}
  \begin{pspicture}(-0.5,-2.2)(5,3.2)
    \psaxes[labels=none,labelFontSize=\scriptstyle ,xAxis=true,yAxis=true,ticksize=0 0]{->}(0,0)(-0.5,-2.2)(5,3.2)%[$t$,-90][$v(t)$,180]
    \pscurve(0,1)(0.2,1.1)(1,1.5)(1.8,2.7)(2,3)
    \psline[linestyle=dotted](2,3.2)(2,-2.2)
    \pscurve(2,3)(2.2,2.7)(3,1.5)(3.8,1.1)(4,1)
    \uput[-45](2,0){$\color{gdarkgray} z$}
  \end{pspicture} \\
  $\frac{\dx{}^{N-1}}{\dx{x}^{N-1}} G(x,z)$ hat einen \emph{Sprung} bei $x=z$. $\frac{\dx{}^{N-2}}{\dx{x}^{N-2}} G(x,z)$ hat einen \emph{Knick} bei $x=z$.
\end{figure}

\end{itemize}

Alle niedrigen Ableitungen sind bei $x=z$ differenzierbar und somit auch stetig.

Die Bedingungen können verwendet werden um $G(x,z)$ zu bestimmen, denn berechne:
\begin{align*}
  \lim_{\varepsilon \rightarrow 0} \int\limits_{z-\varepsilon}^{z+\varepsilon} \sum\limits_{i=0}^{N} a_i\, \frac{\dx{}^i}{\dx{x}^i} G(x,z) \dx{x} &= \lim_{\varepsilon \rightarrow 0} \int\limits_{z-\varepsilon}^{z+\varepsilon} \delta(z-x) \dx{x} \\
  &= 1
\end{align*}

Linke Seite:
\begin{align*}
  &a_N \lim_{\varepsilon \rightarrow 0} \left[ \frac{\dx{}^{N-1}}{\dx{x}^{N-1}} G(z+\varepsilon , z) -  \frac{\dx{}^{N-1}}{\dx{x}^{N-1}} G(z-\varepsilon , z) \right] && \ \text{\color{gray} ($N$-ter Term)}\\
  + &\underbrace{ \sum\limits_{i=1}^{N-1} a_i\, \lim_{\varepsilon \rightarrow 0} \left[ \frac{\dx{}^{i-1}}{\dx{x}^{i-1}} G(z+\varepsilon , z) -  \frac{\dx{}^{i-1}}{\dx{x}^{i-1}} G(z-\varepsilon , z) \right] }_{ = 0 \text{, weil stetig}} && {\color{gray} \left(\begin{matrix} \text{$N-1$-ter bis} \\ \text{$1$-ter Term} \end{matrix}\right)} \\
  + &\underbrace{ \lim_{\varepsilon \rightarrow 0} a_0 G(z,z) \varepsilon }_{ = 0} && \ \text{\color{gray}  ($0$-ter Term)}
\end{align*}
Es bleibt übrig:
\begin{align}
  a_N \lim_{\varepsilon \rightarrow 0} \left[ \frac{\dx{}^{N-1}}{\dx{x}^{N-1}} G(z+\varepsilon , z) -  \frac{\dx{}^{N-1}}{\dx{x}^{N-1}} G(z-\varepsilon , z) \right] = 1
  \label{eq:5.32}
\end{align}

Für $x \neq z$ ist $\delta(z - x) = 0$, dort muss $G(x,z)$ also die homogene DGL erfüllen und die Abhängigkeit von $z$ kann nur in den Koeffizienten stecken.
\begin{align}
  G(x,z) \overset{\eqref{eq:5.22}}{=} \sum\limits_{i=1}^{N} c_i(z)\, e^{\lambda_i x} \; , \quad x \neq z
  \label{eq:5.33}
\end{align}
oder analog für \eqref{eq:5.23}.

\textbf{Anmerkung:} Dieses Vorgehen funktioniert genau gleich bei nicht-konstanten Koeffizienten $a_i(x)$ der DGL.


\subsection{Vorgehen an einem Beispiel}
\index{Differentialgleichungen!Greensche Funktion!Beispiel}

Aufgabe: Löse die DGL
\begin{align}
  \frac{\dx{}^{2}}{\dx{x}^2} y(x) + y(x) = \frac{1}{\sin(x)} \label{eq:5.34}
\end{align}
im Intervall $0 \leq x \leq \frac{\pi}{2}$ mit $y(0) = y \left( \frac{\pi}{2} \right) = 0$.

\textbf{Schritt 1:} Allgemeine Lösung für $x \neq z$
\begin{subequations}
\begin{align}
  G(x,z) \overset{\eqref{eq:5.28}}{\underset{\eqref{eq:DGLhoehereOrdnungKoeffA6}}{=}}& 
    \begin{dcases}
      A(z) \cos(x) + B(z) \sin(x) & x < z \\
      C(z) \cos(x) + D(z) \sin(x) & x > z \\
    \end{dcases}
  \label{eq:5.35a} \\
  \nonumber \\
    A(z), B(z), C(z), D(z) &: \text{$z$-abhängige Koeffizienten} \nonumber \\
    \cos(x), \sin(x) &: \text{Lösung der homogenen DGL} \nonumber
\end{align}

\textbf{Schritt 2:} Die Randbedingungen fordern
\begin{align}
  G(\underbrace{0}_{\mathclap{x < z \rightarrow A(z)=0}},z) = G(\underbrace{\frac{\pi}{2}}_{\mathclap{x > z \rightarrow D(z)=0}}, z) = 0
  \label{eq:5.35b}
\end{align}
und es bleibt:
\begin{align}
  G(x,z) \overset{\eqref{eq:5.35a}}{\underset{\eqref{eq:5.35b}}{=}}& 
    \begin{dcases}
      B(z) \sin(x) & x < z \\
      C(z) \cos(x) & x > z \\
    \end{dcases}
  \label{eq:5.35c}
\end{align}
\end{subequations}

\textbf{Schritt 3:} Stetigkeit und Sprungbedingung \eqref{eq:5.32} auswerten:
\begin{subequations}
\begin{align*}
  \lim_{\varepsilon \rightarrow 0} \left(G(z+\varepsilon , z) - G(z-\varepsilon , z)\right) = 0,
\end{align*}
also mit \eqref{eq:5.35c}:
\begin{align}
  C(z) \cos(z) - B(z) \sin(z) = 0,
  \label{eq:5.36a}
\end{align}
und aus \eqref{eq:5.32}:
\begin{align*}
  \lim_{\varepsilon \rightarrow 0} \left(\frac{\dx{G}(z+\varepsilon , z)}{\dx{x}} - \frac{\dx{G}(z-\varepsilon , z)}{\dx{x}}\right) = 1,
\end{align*}
also mit \eqref{eq:5.35c}:
\begin{align}
  - C(z) \sin(z) - B(z) \cos(z) = 1
  \label{eq:5.36b}
\end{align}
\end{subequations}
Die Gleichungen \eqref{eq:5.36a} und \eqref{eq:5.36b} liefern unter Verwendung der Identität $\sin^2(x) + \cos^2(x) = 1$:
\begin{subequations}
\begin{align}
  C(z) &= -\sin(z) \label{eq:5.37a} \\
  B(z) &= -\cos(z) \label{eq:5.37b}
\end{align}
\end{subequations}

\textbf{Schritt 4:} Greensche Funktion aufstellen:
\begin{align}
  G(x,z) \overset{\eqref{eq:5.35c}}{\underset{\eqref{eq:5.37a}, \eqref{eq:5.37b}}{=}}& 
    \begin{dcases}
      - \cos(z) \sin(x) & x < z \\
      - \sin(z) \cos(x) & x > z \\
    \end{dcases}
  \label{eq:5.38} \\
  \nonumber
\end{align}

\textbf{Schritt 5:} Bestimmen der Lösung von \eqref{eq:5.32}
\begin{align}
  y(x) &= \int\limits_{0}^{\frac{\pi}{2}} G(x,z) \frac{1}{\sin(z)} \dx{z} \nonumber\\
      &\overset{\eqref{eq:5.38}}{=} -\int\limits_{0}^{x} \cos(x) \frac{\sin(z)}{\sin(z)} \dx{z} - \int\limits_{x}^{\frac{\pi}{2}} \sin(x) \frac{\cos(z)}{\sin(z)} \dx{z} \nonumber \\
  &= -x \cos(x) + \sin(x) \ln(\sin(x))
  \label{eq:5.39}
\end{align}

Anmerkungen:
\begin{itemize}
\item Die gleiche Greensche Funktion kann nun bei gleichen Randbedingungen auch für jede andere Inhomogenität verwendet werden.
\item Die hier vorgestellte Methode funktioniert nur bei homogenen Randbedingungen $y^{(n)}(x_i) = 0$. Liegen diese nicht vor, transformiert man
  \begin{align*}
    f(x) = y(x) + g(x)
  \end{align*}
mit einem $g(x)$ so, dass $f(x)$ homogene Randbedingungen erfüllt. Man löst die DGL für $f(x)$.
\end{itemize}

\index{Differentialgleichungen|)}
