% Holger Cartarius, Henri Menke, 2012 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

\section{Zu den Differentialoperatoren}\label{sec:Appendix3}

\subsection*{Definition des Nabla-Operators in kartesischen Koordinaten}
  \begin{equation*}
    \nabla = \bm{e}_x \frac{\partial}{\partial x} 
    +  \bm{e}_y \frac{\partial}{\partial y} 
    +  \bm{e}_z \frac{\partial}{\partial z} 
    = \begin{pmatrix} \nicefrac{\partial}{\partial x} \\ \nicefrac{\partial}{\partial y} \\ \nicefrac{\partial}{\partial z} \end{pmatrix}
  \end{equation*}

\subsection*{Gradient}
  \begin{align*}
    \mathrm{grad} \, V(\bm{r}) = \nabla V(\bm{r}) 
    &= \bm{e}_x \frac{\partial V(\bm{r})}{\partial x} 
    + \bm{e}_y \frac{\partial V(\bm{r})}{\partial y}
    + \bm{e}_z \frac{\partial V(\bm{r})}{\partial z}
    \quad \text{\color{gray} (kartesische Koordinaten)}\\
    &= \bm{e}_\varrho \frac{\partial V(\bm{r})}{\partial \varrho} 
    + \bm{e}_\varphi \frac{1}{\varrho} \frac{\partial V(\bm{r})}{\partial 
      \varphi}
    + \bm{e}_z \frac{\partial V(\bm{r})}{\partial z} 
    \quad \text{\color{gray} (Zylinderkoordinaten)}\\
    &= \bm{e}_r \frac{\partial V(\bm{r})}{\partial r} 
    + \bm{e}_\vartheta \frac{1}{r} \frac{\partial V(\bm{r})}{\partial \vartheta}
    + \bm{e}_\varphi \frac{1}{r \sin\vartheta} \frac{\partial V(\bm{r})}
    {\partial \varphi}
    \quad \text{\color{gray} (Kugelkoordinaten)}
  \end{align*}
      
\subsection*{Divergenz}
  \begin{align*}
    \mathrm{div} \, \bm{A}(\bm{r}) = \nabla \cdot \bm{A}(\bm{r}) 
    &= \frac{\partial A_x(\bm{r})}{\partial x} + \frac{\partial A_y(\bm{r})}
    {\partial y} + \frac{\partial A_z(\bm{r})}{\partial z} \\
    &= \frac{1}{\varrho} \frac{\partial}{\partial \varrho}(\varrho 
    A_\varrho(\bm{r})) + \frac{1}{\varrho} \frac{\partial A_\varphi(\bm{r})}
    {\partial \varphi} + \frac{\partial A_z(\bm{r})}{\partial z} \\
    &= \frac{1}{r^2} \frac{\partial}{\partial r}(r^2 A_r(\bm{r}))
    + \frac{1}{r \sin\vartheta} \frac{\partial}{\partial \vartheta}
    (A_\vartheta(\bm{r}) \sin\vartheta ) + \frac{1}{r \sin\vartheta} 
    \frac{\partial A_\varphi(\bm{r})}{\partial \varphi}
  \end{align*}

  Ein Vektorfeld mit $\mathrm{div}\,\bm{A}=0$ heißt "`quellenfrei"'.

\subsection*{Rotation}
  \begin{align*}
    \mathrm{rot} \, \bm{A}(\bm{r}) = \nabla \times \bm{A}(\bm{r}) 
    &= \bm{e}_x \left [ \frac{\partial A_z(\bm{r})}{\partial y}
      - \frac{\partial A_y(\bm{r})}{\partial z} \right ]
    + \bm{e}_y \left [ \frac{\partial A_x(\bm{r})}{\partial z}
      - \frac{\partial A_z(\bm{r})}{\partial x} \right ]  \\ &\quad
    + \bm{e}_z \left [ \frac{\partial A_y(\bm{r})}{\partial x}
      - \frac{\partial A_x(\bm{r})}{\partial y} \right ] \\
    &= \bm{e}_\varrho \left [ \frac{1}{\varrho} \frac{\partial A_z(\bm{r})}
      {\partial \varphi} - \frac{\partial A_\varphi(\bm{r})}{\partial z} \right ]
    + \bm{e}_\varphi \left [ \frac{\partial A_\varrho(\bm{r})}{\partial z}
      - \frac{\partial A_z(\bm{r})}{\partial \varrho} \right ] \\ &\quad
    + \bm{e}_z \frac{1}{\varrho} \left [ \frac{\partial}{\partial \varrho}
      (\varrho A_\varphi(\bm{r})) - \frac{\partial A_\varrho(\bm{r})}
      {\partial \varphi} \right ] \\
    &= \bm{e}_r \frac{1}{r \sin\vartheta} \left [ \frac{\partial}{\partial
        \vartheta} (A_\varphi(\bm{r}) \sin \vartheta) - \frac{\partial 
        A_\vartheta(\bm{r})}{\partial \varphi} \right ] \\ &\quad
    + \bm{e}_\vartheta \frac{1}{r} \left [ \frac{1}{\sin \vartheta} 
      \frac{\partial A_r(\bm{r})}{\partial \varphi} - \frac{\partial}
      {\partial r} (r A_\varphi(\bm{r})) \right ] \\ &\quad
    + \bm{e}_\varphi \frac{1}{r} \left [ \frac{\partial}{\partial r}
      (r A_\vartheta(\bm{r})) - \frac{\partial A_r(\bm{r})}{\partial \vartheta}
    \right ]
  \end{align*}

  Ein Vektorfeld mit $\mathrm{rot}\,\bm{A}=0$ heißt "`wirbelfrei"'.

\subsection*{Laplace}
  \begin{align*}
    \Delta V(\bm{r}) = \nabla^2 V(\bm{r}) 
    &= \frac{\partial^2 V(\bm{r})}{\partial x^2} + \frac{\partial^2 V(\bm{r})}
    {\partial y^2} + \frac{\partial^2 V(\bm{r})}{\partial z^2} \\
    &= \frac{1}{\varrho} \frac{\partial}{\partial \varrho} \left ( \varrho
      \frac{\partial V(\bm{r})}{\partial \varrho} \right ) + \frac{1}
      {\varrho^2} \frac{\partial^2 V(\bm{r})}{\partial \varphi^2}
      + \frac{\partial^2 V(\bm{r})}{\partial z^2} \\
    &= \frac{1}{r^2} \frac{\partial}{\partial r} \left ( r^2 \frac{\partial
        V(\bm{r})}{\partial r} \right ) + \frac{1}{r^2 \sin\vartheta}
    \frac{\partial}{\partial \vartheta} \left ( \sin \vartheta \frac{\partial
        V(\bm{r})}{\partial \vartheta} \right ) + \frac{1}{r^2 \sin^2\vartheta}
    \frac{\partial^2 V(\bm{r})}{\partial \varphi^2} 
  \end{align*}

\subsection*{In beliebigen krummlinigen Koordinaten}
  In den Koordinaten $u_1$, $u_2$ und $u_3$ zur Darstellung des Vektors 
  $\bm{r} = \bm{r}(u_1,u_2,u_3)$ erhält man mit den Basisvektoren
  $\bm{e}_i = \frac{1}{h_i} \frac{\partial \bm{r}}{\partial u_i}$ und
  mit $h_i = \left | \frac{\partial \bm{r}}{\partial u_i} \right |$
  für das Skalarfeld $V(u_1,u_2,u_3)$ und das Vektorfeld $\bm{A}(u_1,u_2,u_3)
  = \sum_i A_{u_i}(u_1,u_2,u_3) \bm{e}_i(u_1,u_2,u_3)$ die folgenden Darstellung
  der Differentialoperatoren:
  \begin{align*}
    \mathrm{grad}\,V = \nabla V &= \sum_i \bm{e}_i \frac{1}{h_i} 
    \frac{\partial V}{\partial u_i} \\
    \mathrm{div}\,\bm{A} = \nabla \cdot \bm{A} &= \frac{1}{h_{u_1}h_{u_2}h_{u_3}}
    \left [ \frac{\partial}{\partial u_1} \left ( h_{u_2}h_{u_3} A_{u_1} \right )
      + \frac{\partial}{\partial u_2} \left ( h_{u_1}h_{u_3} A_{u_2} \right )
      + \frac{\partial}{\partial u_3} \left ( h_{u_1}h_{u_2} A_{u_3} \right )
    \right ] \\
    \mathrm{rot}\,\bm{A} = \nabla \times \bm{A}
    &= \frac{1}{h_{u_2}h_{u_3}} \bm{e}_{u_1} \left [ \frac{\partial}{\partial u_2}
      \left ( h_{u_3} A_{u_3} \right ) - \frac{\partial}{\partial u_3} 
      \left ( h_{u_2} A_{u_2} \right ) \right ] \\ & \quad
    + \frac{1}{h_{u_1}h_{u_3}} \bm{e}_{u_2} \left [ \frac{\partial}{\partial u_3}
      \left ( h_{u_1} A_{u_1} \right ) - \frac{\partial}{\partial u_1} 
      \left ( h_{u_3} A_{u_3} \right ) \right ] \\ & \quad 
    + \frac{1}{h_{u_1}h_{u_2}} \bm{e}_{u_3} \left [ \frac{\partial}{\partial u_1}
      \left ( h_{u_2} A_{u_2} \right ) - \frac{\partial}{\partial u_2} 
      \left ( h_{u_1} A_{u_1} \right ) \right ] \\
    \Delta V = & \frac{1}{h_{u_1}h_{u_2}h_{u_3}} \left [ \frac{\partial}
      {\partial u_1} \left ( \frac{h_{u_2}h_{u_3}}{h_{u_1}}
        \frac{\partial V}{\partial u_1} \right ) \right. + \frac{\partial}{\partial u_2} \left ( \frac{h_{u_1}h_{u_3}}{h_{u_2}} 
      \frac{\partial V}{\partial u_2} \right ) \\
      & + \left. \frac{\partial}{\partial u_3} \left ( \frac{h_{u_1}h_{u_2}}{h_{u_3}} 
      \frac{\partial V}{\partial u_3} \right ) \right ]
  \end{align*}


\subsection*{Nützliche Relationen}
  \vspace*{-2em}
  \begin{align*}
    \mathrm{div} \, \mathrm{grad} \, V &= \nabla \cdot \nabla V 
    = \Delta \, V \\
    \mathrm{rot} \, \mathrm{grad} \, V &= \nabla \times \nabla V 
    = 0 \\
    \mathrm{div} \, \mathrm{rot} \, \bm{A} &= \nabla \cdot (\nabla \times 
    \bm{A}) = 0 \\
    \nabla (V+W) &= \nabla V + \nabla W \\
    \nabla (V W) &= W \nabla V + V \nabla W \\
    \nabla (\bm{A}\cdot\bm{B}) &= \bm{A} \times (\nabla \times \bm{B})
    +  \bm{B} \times (\nabla \times \bm{A}) + (\bm{A} \cdot \nabla) \bm{B}
    + (\bm{B} \cdot \nabla) \bm{A} \\
    \nabla \cdot (\bm{A}+\bm{B}) &= \nabla \cdot \bm{A} + \nabla \cdot \bm{B} \\
    \nabla \times (\bm{A}+\bm{B}) &= \nabla \times \bm{A} 
    + \nabla \times \bm{B} \\
    \nabla \cdot (V \bm{A}) &= \nabla V \cdot \bm{A} + V \nabla 
    \cdot \bm{A} \\
    \nabla \times (V \bm{A}) &= \nabla V \times \bm{A} + V \nabla 
    \times \bm{A} \\
    \nabla \cdot (\bm{A} \times \bm{B}) &= \bm{B} \cdot (\nabla \times \bm{A})
    - \bm{A} \cdot (\nabla \times \bm{B}) \\
    \nabla \times (\bm{A} \times \bm{B}) &= \bm{A} (\nabla \cdot \bm{B})
    - \bm{B} (\nabla \cdot \bm{A}) + (\bm{B} \cdot \nabla) \bm{A}
    - (\bm{A} \cdot \nabla) \bm{B} \\
    \nabla \times (\nabla \times \bm{A}) &= \nabla (\nabla \cdot \bm{A}) 
    - \Delta \bm{A}
  \end{align*}

\clearpage