% Holger Cartarius, Henri Menke, 2012 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

\chapter{Potenzreihenentwicklung}
\index{Potenzreihenentwicklung|(}

Als \emph{Potenzreihe} bezeichnet man eine unendliche konvergente Reihe der Form 
\[
  \sum\limits_{n=1}^{\infty} a_n (x-x_0)^n.
\]

\section{Bedeutung in der Physik}
Potenzreihen sind ein wichtiges Näherungsverfahren in der Physik. Sie kommen immer zur Anwendung, wenn eine Funktion $f(x)$ nur in einer kleinen Umgebung um einen Punkt $x_0$ ausgewertet werden muss.

\textbf{Beispiel:} Eine Masse schwingt um das Minimum eines komplizierten Potentials. Wie lautet die Schwingungsfrequenz? Zur Beantwortung dieser Frage reicht es aus, die Funktion $v(x)$ in der Nähe des Minimums zu kennen. Annäherung durch z.B. eine Parabel:

\begin{figure}[H]
  \centering
  \begin{pspicture}(-0.5,-0.5)(6,4)
    \psaxes[labels=none,labelFontSize=\scriptstyle\color{gdarkgray} ,xAxis=true,yAxis=true,Dx=1,Dy=1,ticks=none]{->}(0,0)(-0.5,-0.5)(6,4)[$\color{gdarkgray} x$,-90][$\color{gdarkgray} v(x)$,180]
    %
    \pscurve[linewidth=1px](0,1)(1,2)(2,0.5)(3,2)(4,1.6)(5,3)(6,2.7)
    \psplot[linecolor=darkblue,linewidth=1.2px]{1.05}{2.95}{3.5*(x-2)^2+0.55}
    \psline[linestyle=dotted](2,0.5)(2,0)
    \uput[-90](2,0){$\color{darkblue} x_0$}
    \psdots[dotstyle=o,dotscale=1.3](2,0.59)
  \end{pspicture}
\end{figure}

Was wird durch Potenzreihen einfacher?

Sei die Funktion $f(x)$ beliebig kompliziert.
\begin{align}
\intertext{Potenzreihe:}
  f_P(x) &= a_0+a_1(x-x_0)+a_2(x-x_0)^2+a_3(x-x_0)^3+\ldots +a_j(x-x_0)^j + \ldots \nonumber\\
  &= \sum\limits_{n=0}^{\infty} a_n(x-x_0)^n \label{eq:2.1}
\end{align}

Oft reicht es für eine lokale Umgebung von $x_0$ ($x-x_0$ ist klein), niedrige Potenzen zu betrachten:
\begin{align*}
  f_{P,2}(x)&=a_0+a_1(x-x_0)+a_2(x-x_0)^2 && \text{\color{gray} (quadratische Näherung)}
\end{align*}
%
$f_{P,2}(x)$ ist meist wesentlich einfacher und macht das Integrieren, Lösen von Differentialgleichungen, etc. einfacher oder überhaupt erst möglich.

\section{Taylorreihe}
\index{Potenzreihenentwicklung!Taylorreihe}
Wie sieht eine Potenzreihe $f_{P}(x)$ einer Funktion$ f(x) $ um $ x=x_{0} $ aus?
\begin{subequations}
  \begin{align}
    f_{P}(x) &\overset{\eqref{eq:2.1}}{=} \sum\limits_{n=0}^{\infty} a_n(x-x_0)^n \nonumber
  \intertext{Setze $x=x_0$. Aus der Forderung $ f(x_0)=f_P(x_0) $ und aus $f_{P}(x_0) = a_0+0$ folgt:}
     a_0 &= f(x_0)
  \intertext{Es muss auch gelten $ f'(x_0) = f_{P}'(x_0) $:}
    f_{P}'(x) &= \sum\limits_{n=1}^{\infty} a_n\, n\, (x-x_0)^{n-1} \nonumber\\
    f_{P}'(x_0) &= a_1 \overset{!}{=} f'(x_0) \label{eq:2.2b}
  \intertext{Weiterhin:}
    f_{P}''(x) &= \sum\limits_{n=2}^{\infty} a_n\, n (n-1)\, (x-x_0)^{n-2} \nonumber\\
    f_{P}''(x_0) &= 2\, a_2 \overset{!}{=} f''(x_0) \nonumber\\
  \text{oder }a_2 &= \frac{1}{2}f''(x_0) \label{eq:2.2c}
  \end{align}
\end{subequations}
\begin{align}
  \intertext{Fortsetzen des Verfahrens führt auf die \emph{Formel von Taylor}:}
  \boxed{ f_P(x) = \sum\limits_{n=0}^{\infty} \frac{1}{n!} f^{(n)}(x_0) (x-x_0)^{n} }
  \label{eq:2.3}
\end{align}
\index{Potenzreihenentwicklung!Taylorreihe!Formel von Taylor}

Anmerkungen:
\begin{itemize}
  \item Hier wurde vorrausgesetzt, dass die Potenzreihe existiert und $f(x)$ beliebig oft differenzierbar ist.
  \item Eine Potenzreihe muss nicht immer konvergieren. Meist ist die Konvergenz nur innerhalb eines \emph{Konvergenzradius} gegeben, d.h. für $ |x-x_0| < r $.
  \item Eine Taylorreihe mit $ x_0 = 0 $ heißt \emph{McLaurin-Reihe}.
\end{itemize}

\section{Beispiele}
\fbox{$f(x) = \frac{1}{1-x}$}
\begin{align}
  f(x) &= \frac{1}{1-x},\quad x_0=0 \nonumber\\
  f'(x) &= \frac{1}{(1-x)^{2}},\quad f''(x) = \frac{2}{(1-x)^{3}},\quad f'''(x) = \frac{6}{(1-x)^{4}} \nonumber\\
  \text{oder } f^{(n)}(x) &= \frac{n!}{(1-x)^{n+1}},\quad f^{(n)}(x_0) = n! \nonumber\\
\intertext{Taylorreihe:}
  \frac{1}{1-x} &= \sum\limits_{n=0}^{\infty} \frac{1}{n!} n! (x-\overbrace{x_0}^{=0})^{n} = \sum\limits_{n=0}^{\infty} x^n
\intertext{Konvergiert für $ |x| < 1 $ $ \longrightarrow $ geometrische Reihe!} \nonumber
\end{align}

\fbox{$f(x) = e^x$}
\begin{align}
  f(x) &= e^x,\quad x_0=0 \nonumber\\
  f^{(n)}(x) &= e^x,\quad f^{(n)}(x_0) = 1 \nonumber\\
\intertext{Taylorreihe:}
  e^x &= \sum\limits_{n=0}^{\infty} \frac{x^n}{n!} \label{eq:2.5}
\intertext{Konvergiert für $x \in \mathbb{R}$} \nonumber
\end{align}

\textbf{Weitere Beispiele:}
\index{Potenzreihenentwicklung!Taylorreihe!Einige Taylorreihen}
\begin{align}
  \sin(x) &= \sum\limits_{n=0}^{\infty} (-1)^{n} \frac{x^{2n+1}}{(2n+1)!}, \quad x \in \mathbb{R} \label{eq:2.6}
\end{align}
\begin{align}
  \cos(x) &= \sum\limits_{n=0}^{\infty} (-1)^{n} \frac{x^{2n}}{(2n)!}, \quad x \in \mathbb{R} \label{eq:2.7}
\end{align}
\begin{align}
  \ln(1+x) &= \sum\limits_{n=1}^{\infty} (-1)^{n+1} \frac{x^{n}}{n}, \quad -1 < x < 1 \label{eq:2.8}
\end{align}
\index{Potenzreihenentwicklung|)}

%\begin{align*}
%  \int\limits_{a}^{b} f(x) \frac{1}{\sqrt{x}} \dx{x} &= \left. 2 \sqrt{x} \right|_{a}^{b}
%\end{align*}
%$x \rightarrow 0$, $\frac{1}{\sqrt{x}} \rightarrow \infty$, aber das Integral konvergiert an dieser Grenze \\
%$x \rightarrow \infty$, $\frac{1}{\sqrt{x}} \rightarrow 0$, aber das Integral divergiert
