% Holger Cartarius, Henri Menke, 2012 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

\section{Zur Berechnung von Determinanten}\label{sec:Appendix2}

In der Vorlesung wurde die Jacobi-Determinante im Zusammenhang mit mehrdimensionalen Integralen eingeführt. In diesem Abschnitt soll darauf eingegangen werden, wie man eine Determinante berechnet.

\subsection*{Zur Determinante}
  Eine Determinante ist eine Abbildung, die einer quadratischen Matrix eindeutig eine Zahl zuordnet. Diese kann reell oder komplex sein. Das hängt von den Matrixelementen ab. Man schreibt für die $N\times N$-Matrix
  \begin{equation*}
    \bm{A} = \begin{pmatrix}
      a_{1,1} & a_{1,2} & \cdots & a_{1,N} \\
      a_{2,1} & \ddots &       & \vdots \\
      \vdots &       & \ddots & a_{N-1,N} \\
      a_{N,1} & \cdots & a_{N,N-1} & a_{N,N}
    \end{pmatrix} \; , \quad
    \det \bm{A} = \begin{vmatrix}
      a_{1,1} & a_{1,2} & \cdots & a_{1,N} \\
      a_{2,1} & \ddots &       & \vdots \\
      \vdots &       & \ddots & a_{N-1,N} \\
      a_{N,1} & \cdots & a_{N,N-1} & a_{N,N}
    \end{vmatrix}
  \end{equation*}
  und meint mit letzterem die Determinante von $\bm{A}$.

\subsection*{Zur Berechnung einer Determinante}
  
  Allgemein lässt sich eine Determinante mit dem Entwicklungssatz berechnen. Dieser besagt, dass für eine Determinante gilt:
  \begin{align}
    \det \bm{A} = \sum_{\nu=1}^{N} (-1)^{\mu+\nu} a_{\mu\nu} \det \bm{A}_{\mu\nu}
    \qquad (1 \le \mu \le N \text{, fest})
    \label{eq:Zeilenentwicklung} \\
    \det \bm{A} = \sum_{\mu=1}^{N} (-1)^{\mu+\nu} a_{\mu\nu} \det \bm{A}_{\mu\nu} 
    \qquad (1 \le \nu \le N \text{, fest}) 
    \label{eq:Spaltenentwicklung}
  \end{align}
  Dabei ist $\bm{A}_{\mu\nu}$ in Gleichung \eqref{eq:Zeilenentwicklung} die Untermatrix, bei der sowohl die Zeile $\mu$ als auch die Spalte $\nu$, also die Zeile und die Spalte, in denen das Matrixelement $a_{\mu\nu}$ steht, entfernt wurden. Dies ist nun eine $(N-1) \times (N-1)$-Matrix, also eine Dimension kleiner als die Matrix $\bm{A}$. Den Entwicklungssatz kann man rekursiv anwenden, bis man bei einer $1\times 1$-Matrix angekommen ist, deren Determinante einfach das einzige verbleibende Matrixelement ist. Da man in Gleichung \eqref{eq:Zeilenentwicklung} über alle Matrixelemente der Zeile $\mu$ summiert, spricht man in diesem Fall von der "`Entwicklung nach der $\mu$-ten Zeile"'. Ganz analog kann man sich überlegen, wie die "`Entwicklung nach der $\nu$-ten Spalte"' in Gleichung \eqref{eq:Spaltenentwicklung} funktioniert. Über den Entwicklungssatz ist der Wert einer Determinante definiert, d.h. der Satz ist mehr als eine Berechnungsmethode. Er legt eindeutig fest, was der Wert einer Determinante ist. Für kleine Matrizen gibt es einfache Formeln, so dass man sich den -- im ersten Moment vielleicht kompliziert wirkenden -- Entwicklungssatz für diese nicht merken muss. 

\subsection*{\texorpdfstring{Zur Berechnung einer $2\times 2$-Determinante}{Zur Berechnung einer 2x2-Determinante}}
  Für eine $2\times 2$-Determinante erhält man:
  \begin{equation*}
    \det\bm{A} = \begin{vmatrix}
      a & b \\
      c & d \end{vmatrix}
    = ad - cb
  \end{equation*}
  Man kann sich einfach merken: Produkt der Elemente der Hauptdiagonalen minus Produkt der Elemente der Nebendiagonalen.

\subsection*{\texorpdfstring{Zur Berechnung einer $3\times 3$-Determinante}{Zur Berechnung einer 3x3-Determinante}}
  Für Determinanten von $3\times 3$-Matrizen gibt es ebenfalls noch eine einfache Methode, die Regel von Sarrus:
   \begin{equation*}
    \det \bm{A} = \begin{vmatrix}
      a & b & c \\
      d & e & f \\
      g & h & i \end{vmatrix}
    = aei + bfg + cdh - gec - hfa - idb
  \end{equation*}
  Um sich die Regel von Sarrus leicht merken zu können, schreibt man sich -- nach etwas Übung meist nur noch in Gedanken -- die ersten beiden Spalten noch einmal neben die eigentliche Determinante:
  \begin{center}
    \begin{picture}(95,55)(0,0)
      \put(10,45){\makebox(0,0)[b]{$a$}}
      \put(30,45){\makebox(0,0)[b]{$b$}}
      \put(50,45){\makebox(0,0)[b]{$c$}}
      \put(10,25){\makebox(0,0)[b]{$d$}}
      \put(30,25){\makebox(0,0)[b]{$e$}}
      \put(50,25){\makebox(0,0)[b]{$f$}}
      \put(10,5){\makebox(0,0)[b]{$g$}}
      \put(30,5){\makebox(0,0)[b]{$h$}}
      \put(50,5){\makebox(0,0)[b]{$i$}}
      \put(70,45){\makebox(0,0)[b]{$a$}}
      \put(90,45){\makebox(0,0)[b]{$b$}}
      \put(70,25){\makebox(0,0)[b]{$d$}}
      \put(90,25){\makebox(0,0)[b]{$e$}}
      \put(70,5){\makebox(0,0)[b]{$g$}}
      \put(90,5){\makebox(0,0)[b]{$h$}}
      \put(0,0){\line(0,1){55}}
      \put(60,0){\line(0,1){55}}
      \thicklines
      \put(15,43){\line(1,-1){10}}
      \put(35,23){\line(1,-1){10}}
      \put(35,43){\line(1,-1){10}}
      \put(55,23){\line(1,-1){10}}
      \put(55,43){\line(1,-1){10}}
      \put(75,23){\line(1,-1){10}}
      \put(10,5){\dottedline{2}(5,10)(15,20)}
      \put(30,25){\dottedline{2}(5,10)(15,20)}
      \put(30,5){\dottedline{2}(5,10)(15,20)}
      \put(50,25){\dottedline{2}(5,10)(15,20)}
      \put(50,5){\dottedline{2}(5,10)(15,20)}
      \put(70,25){\dottedline{2}(5,10)(15,20)}
    \end{picture}
  \end{center}
  Mit diesem Bild vor Augen addiert man die Produkte der Matrixelemente, die mit durchgezogenen Linien verbunden sind, und subtrahiert davon die Produkte der Elemente, die mit gepunkteten Linien verbunden sind.

\clearpage