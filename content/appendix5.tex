% Holger Cartarius, Henri Menke, 2012 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

\section{Matrizen}

Ein rechteckiges Zahlenschema der Form
\begin{equation}
  \bm{A} = \begin{pmatrix} a_{1,1} & a_{1,2} & \cdots & a_{1,n} \\
    a_{2,1} & a_{2,2} & \cdots & a_{2,n} \\
    \vdots & \vdots & \ddots & \vdots \\ 
    a_{m,1} & a_{m,2} & \cdots & a_{m,n}
  \end{pmatrix}
  \label{eq:MatrixA}
\end{equation}
nennt man eine $m \times n$-Matrix. Das Matrixelement $a_{i,j}$ [andere
gebräuchliche Schreibweise: $(\bm{A})_{i,j}$] steht in der $i$-ten Zeile und
in der $j$-ten Spalte. 

\subsection*{Besondere Matrixformen}
Ist die Zahl der Spalten und Zeilen gleich, liegt also eine $n\times n$-Matrix
vor, spricht man von einer \emph{quadratischen} Matrix.

Die Matrix $\bm{0}$, bei der alle Matrixelemente verschwinden, $a_{i,j} = 0$,
heißt \emph{Nullmatrix}.

Eine quadratische Matrix $\bm{D}$, bei der nur die Elemente auf der
Hauptdiagonalen $d_{i,i}$ von Null verschieden sind,
\begin{equation*}
  \bm{D} = \begin{pmatrix} d_{1,1} & 0 & \cdots & 0 & 0 \\
    0 & d_{2,2} & & 0 & 0 \\
    \vdots &  & \ddots & & \vdots \\ 
    0 & 0 & & d_{n-1,n-1} & 0 \\
    0 & 0 & \cdots & 0 & d_{n,n}
  \end{pmatrix} \; ,
\end{equation*}
heißt \emph{Diagonalmatrix}.
  
Die Diagonalmatrix $\bm{E}$ (manchmal auch $\bm{1}$ genannt), bei der alle
Diagonalelemente den Wert $d_{i,i} = 1$ haben, heißt \emph{Einheitsmatrix}.

\subsection*{Transponierte und adjungierte Matrizen}
Vertauscht man in der Matrix $\bm{A}$ ihre Zeilen und Spalten, so erhält
man die zu $\bm{A}$ \emph{transponierte} Matrix, die den Namen
$\bm{A}^{\mathrm{T}}$ trägt. Im Beispiel aus Gleichung \eqref{eq:MatrixA}
erhält man:
\begin{equation*}
  \bm{A}^{\mathrm{T}} = \begin{pmatrix} a_{1,1}^{\mathrm{T}} & a_{1,2}^{\mathrm{T}} 
    & \cdots & a_{1,m}^{\mathrm{T}} \\
    a_{2,1}^{\mathrm{T}} & a_{2,2} ^{\mathrm{T}}& \cdots & a_{2,m}^{\mathrm{T}} \\
    \vdots & \vdots & \ddots & \vdots \\ 
    a_{n,1}^{\mathrm{T}} & a_{n,1}^{\mathrm{T}} & \cdots & a_{n,m}^{\mathrm{T}}
  \end{pmatrix}
  \overset{\eqref{eq:MatrixA}}{=}
  \begin{pmatrix} a_{1,1} & a_{2,1} & \cdots & a_{m,1} \\
    a_{1,2} & a_{2,2} & \cdots & a_{m,2} \\
    \vdots & \vdots & \ddots & \vdots \\ 
    a_{1,n} & a_{2,n} & \cdots & a_{m,n}
  \end{pmatrix}
\end{equation*}
Man beachte, dass so aus einer $m\times n$- eine $n \times m$-Matrix
wird. Für die einzelnen Elemente gilt entsprechend $a_{i,j}^{\mathrm{T}}
= (\bm{A}^{\mathrm{T}})_{i,j} = a_{j,i} = (\bm{A})_{j,i}$. Man kann sich das
Transponieren als Spiegelung der Matrixelemente an der Hauptdiagonalen
vorstellen.

Im Fall komplexer Matrixelemente $a_{i,j}$ definiert man darüber hinaus die
zu $\bm{A}$ \emph{adjungierte} Matrix $\bm{A}^\dagger$, die zusätzlich zum
Transponieren noch komplex konjugiert wird, $\bm{A}^\dagger = 
\bar{\bm{A}}^{\mathrm{T}}$. Für ihre Elemente gilt $a_{i,j}^\dagger 
= (\bm{A}^\dagger)_{i,j} = \overline{a_{j,i}} = \overline{(\bm{A})_{j,i}}$.

\subsection*{Spur und Determinante}
Für quadratische Matrizen $\bm{A}$ sind mit der Determinante
$\mathrm{det}(\bm{A})$ und der Spur $\mathrm{Tr}(A)$ zwei wichtige
Skalare definiert, die sich eindeutig aus der Matrix ergeben. Die
Determinante ist aus ihrem eigenen Infoblatt bekannt. Die Spur einer
$n\times n$-Matrix $\bm{A}$ ist die Summe ihrer Hauptdiagonalelemente:
\begin{equation*}
  \mathrm{Tr}(\bm{A}) = \sum_{i=1}^n a_{i,i}
\end{equation*}

\subsection*{Symmetrische, antisymmetrische, hermitesche und
  antihermitesche Matrizen:\hspace{2mm}}

Eine quadratische Matrix nennt man \emph{symmetrisch}, wenn die Elemente
$a_{i,j}$ und $a_{j,i}$, also die beiden Elemente, die bei einer Spiegelung
an der Hauptdiagonalen vertauscht werden, gleich sind,  $a_{i,j} = a_{j,i}$.
Für symmetrische Matrizen gilt $\bm{A}^{\mathrm{T}} = \bm{A}$, das heißt
die Matrix ist mit ihrer transponierten identisch.

Eine quadratische Matrix mit der Eigenschaft $\bm{A}^{\mathrm{T}} = -\bm{A}$
nennt man \emph{antisymmetrisch} oder \emph{schiefsymmetrisch}. Für ihre
Elemente gilt $a_{i,j} = -a_{j,i}$. Daraus folgt sofort, dass alle 
Diagonalelemente verschwinden müssen $a_{i,j}=0$.

Eine komplexe quadratische Matrix, die mit ihrer adjungierten identisch
ist, $\bm{A}^\dagger = \bm{A}$, nennt man \emph{hermitesch} oder
\emph{selbstadjungiert}. Für ihre Matrixelemente gilt $a_{i,j} = 
\overline{a_{j,i}}$. Entsprechend nennt man Matrizen mit der Eigenschaft
$\bm{A}^\dagger = -\bm{A}$ oder $a_{i,j} = -\overline{a_{j,i}}$
\emph{antihermitesch}. 

\subsection*{Vektoren als Matrizen}
Ein $n$-dimensionaler  \emph{Spaltenvektor}
\begin{equation*}
  \bm{a} = \begin{pmatrix} a_1 \\ a_2 \\ \vdots \\ a_n \end{pmatrix}
\end{equation*}
kann als $n\times 1$-Matrix aufgefasst werden. Die zugehörige
transponierte $1\times n$-Matrix
\begin{equation*}
  \bm{a}^{\mathrm{T}} = \begin{pmatrix} a_1 & a_2 & \cdots & a_n \end{pmatrix}
\end{equation*}
bezeichnet man als $n$-dimensionalen \emph{Zeilenvektor}.

\subsection*{Gleichheit zweier Matrizen}
Zwei Matrizen sind gleich, wenn alle ihre Elemente gleich sind.

\subsection*{Addition und Subtraktion}
Zwei $m\times n$-Matrizen $\bm{A}$ und $\bm{B}$ werden addiert oder
subtrahiert, indem man die Elemente, die sich an der gleichen Stelle
befinden, aufaddiert oder von einander subtrahiert. Ein Beispiel ist:
\begin{equation*}
  \bm{A} \pm \bm{B} = \begin{pmatrix} a_{1,1} & a_{1,2} 
    \\ a_{2,1} & a_{2,2}  \end{pmatrix} 
  \pm \begin{pmatrix} b_{1,1} & b_{1,2} 
    \\ b_{2,1} & b_{2,2}  \end{pmatrix}
  = \begin{pmatrix} a_{1,1} \pm b_{1,1} & a_{1,2} \pm b_{1,2} 
    \\ a_{2,1} \pm b_{2,1} & a_{2,2} \pm b_{2,2}  \end{pmatrix}
\end{equation*}

\subsection*{Multiplikation einer Matrix mit einer Zahl}
Wird eine Matrix mit einer Zahl multipliziert, so multipliziert man
jedes Element mit dieser Zahl. Beispiel:
\begin{equation*}
  c \bm{A} = c \begin{pmatrix} a_{1,1} & a_{1,2} 
    \\ a_{2,1} & a_{2,2}  \end{pmatrix} 
  = \begin{pmatrix} c a_{1,1} & c a_{1,2} 
    \\ c a_{2,1} & c a_{2,2}  \end{pmatrix} 
\end{equation*}

\subsection*{Multiplikation zweier Matrizen}
Die $m\times n$-Matrix $\bm{A}$ und die $n\times p$-Matrix $\bm{B}$
lassen sich in der Form multiplizieren, dass als Produkt eine neue
$m\times p$-Matrix $\bm{C}$ entsteht, deren Elemente
\begin{equation*}
  c_{i,j} = \sum_{k} a_{i,k}b_{k,j}
\end{equation*}
lauten.

Die Multiplikation merkt man sich am einfachsten, indem man die Matrizen
-- nach etwas Übung ganz automatisch in Gedanken -- in Zeilen- und
Spaltenvektoren aufteilt. Für die Matrix $\bm{A}$ erhält man $m$
Zeilenvektoren der Dimension $n$:
\begin{equation*}
  \bm{A} = \begin{pmatrix} a_{1,1} & a_{1,2} & \cdots & a_{1,n} \\
    a_{2,1} & a_{2,2} & \cdots & a_{2,n} \\
    \vdots & \vdots & \ddots & \vdots \\ 
    a_{m,1} & a_{m,2} & \cdots & a_{m,n}
  \end{pmatrix}
  = \begin{pmatrix} \bm{a}_{Z1}^{\mathrm{T}} \\ \bm{a}_{Z2}^{\mathrm{T}} \\ \vdots
    \\ \bm{a}_{Zm}^{\mathrm{T}}
  \end{pmatrix}
\end{equation*}
Entsprechend kann man die Matrix $\bm{B}$ in $p$ Spaltenvektoren der gleichen
Dimension $n$ aufteilen:
\begin{equation*}
  \bm{B} = \begin{pmatrix} b_{1,1} & b_{1,2} & \cdots & b_{1,p} \\
    b_{2,1} & b_{2,2} & \cdots & b_{2,p} \\
    \vdots & \vdots & \ddots & \vdots \\ 
    b_{n,1} & b_{n,2} & \cdots & b_{n,p}
  \end{pmatrix}
  = \begin{pmatrix} \bm{b}_{S1} & \bm{b}_{S2} & \cdots & \bm{b}_{Sp}
  \end{pmatrix}
\end{equation*}
Dann lassen sich die Elemente der Matrix $\bm{C}$ als Skalarprodukte
dieser Vektoren verstehen:
\begin{align*}
  \bm{C} &= \begin{pmatrix} \bm{a}_{Z1} \cdot \bm{b}_{S1} & \bm{a}_{Z1} \cdot 
    \bm{b}_{S2} & \cdots & \bm{a}_{Z1} \cdot \bm{b}_{Sp} \\ 
    \bm{a}_{Z2} \cdot \bm{b}_{S1} & \bm{a}_{Z2} \cdot \bm{b}_{S2} & \cdots 
    & \bm{a}_{Z2} \cdot \bm{b}_{Sp} \\
    \vdots & \vdots & \ddots & \vdots \\ 
    \bm{a}_{Zm} \cdot \bm{b}_{S1} & \bm{a}_{Zm} \cdot 
    \bm{b}_{S2} & \cdots & \bm{a}_{Zm} \cdot \bm{b}_{Sp} \\
  \end{pmatrix}
\end{align*}
Natürlich schreibt man das nie in diesen Schritten auf, sondern stellt
sich nur die entsprechenden Zeilen und Spalten vor und multipliziert diese
im Kopf. Als Regel merkt man sich, dass Matrizen multipliziert werden, indem
man Zeilen mit Spalten multipliziert.

Man beachte, dass das Matrixprodukt nicht kommutativ ist. Für das Produkt
zweier Matrizen $\bm{A}$ und $\bm{B}$ heißt das im Allgemeinen
$\bm{A}\bm{B}\neq\bm{B}\bm{A}$. Selbstverständlich sind die beiden
Reihenfolgen nur dann möglich, wenn $\bm{A}$ eine $m\times n$-Matrix und
$\bm{B}$ eine $n\times m$-Matrix sind.

\subsection*{Multiplikation einer Matrix mit einem Vektor}
Eine Multiplikation eines  $n$-dimensionalen Vektors mit einer
$m\times n$-Matrix funktioniert exakt so wie die Multiplikation zweier
Matrizen, wenn man einen $n$-dimensionalen Spaltenvektor $\bm{b}$ als
$n\times 1$-Matrix auffasst:
\begin{equation*}
  \bm{A} \bm{b} = \begin{pmatrix} \bm{a}_{Z1}^{\mathrm{T}} \\ 
    \bm{a}_{Z2}^{\mathrm{T}} \\ \vdots \\ \bm{a}_{Zm}^{\mathrm{T}}
  \end{pmatrix} \bm{b} = \begin{pmatrix} \bm{a}_{Z1} \cdot \bm{b} \\ 
    \bm{a}_{Z2} \cdot \bm{b} \\ \vdots \\ \bm{a}_{Zm} \cdot \bm{b}
  \end{pmatrix}
\end{equation*}
Das Ergebnis ist ein $m$-dimensionaler Spaltenvektor. Analog gilt für einen
$m$-dimensionalen Zeilenvektor $\bm{b}^{\mathrm{T}}$, den man als 
$1\times m$-Matrix verstehen muss:
\begin{equation*}
  \bm{b}^{\mathrm{T}} \bm{A} = \bm{b}^{\mathrm{T}} \begin{pmatrix} \bm{a}_{S1} 
    & \bm{a}_{S2} & \cdots \bm{a}_{Sn} \end{pmatrix} 
  = \begin{pmatrix} \bm{b} \cdot \bm{a}_{S1} & \bm{b} \cdot \bm{a}_{S2} &
    \cdots & \bm{b} \cdot \bm{a}_{Sn}
  \end{pmatrix}
\end{equation*}


\subsection*{Skalarprodukt als Matrixprodukt}
Das Skalarprodukt $\bm{a}\cdot \bm{b}$ kann im Sinne des Matrixprodukts
als Multiplikation des Zeilenvektors $\bm{a}^{\mathrm{T}}$ mit dem
Spaltenvektor $\bm{b}$ verstanden werden:
\begin{equation}
  \bm{a}\cdot \bm{b} = \bm{a}^{\mathrm{T}} \bm{b}
  \label{eq:Skalarprodukt}
\end{equation}

\subsection*{Dyadisches Produkt}
Mit dem dyadischen Produkt
\begin{equation*}
  \bm{a}\otimes\bm{b} = \bm{a}\bm{b}^T =
  \begin{pmatrix}
    a_1 b_1 & a_1 b_2 & \cdots & a_1 b_m \\
    a_2 b_1 & a_2 b_2 & \cdots & a_2 b_m \\
    \vdots & \vdots & \ddots & \vdots \\
    a_n b_1 & a_n b_2 & \cdots & a_n b_m
  \end{pmatrix}
\end{equation*}
wird aus den Vektoren $\bm{a}$ und $\bm{b}$ eine Matrix gebildet. Dazu wird
der $n$-dimensionale Spaltenvektor $\bm{a}$ mit dem $m$-dimensionalen
Zeilenvektor $\bm{b}^T$ im Sinne des Matrixprodukts multipliziert. Es entsteht
eine $n\times m$-Matrix

\subsection*{Inverse Matrix}
Gibt es zu einer $n\times n$-Matrix $\bm{A}$ eine weitere $n\times n$-Matrix
$\bm{B}$, für die gilt, dass das Produkt von $\bm{B}$ mit $\bm{A}$ die
Einheitsmatrix $\bm{E}$ ergibt, so nennt man $\bm{B}$ die \emph{Inverse} zu
$\bm{A}$ und schreibt $\bm{B} = \bm{A}^{-1}$. Es gilt also:
\begin{equation*}
  \bm{A}^{-1} \bm{A} = \bm{A} \bm{A}^{-1} = \bm{E}
\end{equation*}

Nicht jede Matrix besitzt eine Inverse. Gibt es jedoch eine Inverse zu
$\bm{A}$, nennt man $\bm{A}$ invertierbar. Wie man die Inverse berechnet,
würde den Umfang dieses Blattes sprengen. Das kommt aber ganz sicher in 
der Mathematik. In ein paar Spezialfällen, die auf diesem Blatt vorkommen,
erhält man die Inverse auf einem sehr einfachen Weg.

\subsection*{Inverse und Transponierte bei Produkten von Matrizen}
Die Inverse eines Produktes $\bm{A}\bm{B}$ von Matrizen ist
\begin{equation*}
  (\bm{A}\bm{B})^{-1} = \bm{B}^{-1}\bm{A}^{-1} \; , 
\end{equation*}
wie sich leicht zeigen lässt, denn:
\begin{equation*}
  (\bm{A}\bm{B})^{-1}\bm{A}\bm{B} = \bm{B}^{-1}\underbrace{\bm{A}^{-1}
    \bm{A}}_{\bm{E}}\bm{B} = \bm{B}^{-1}\bm{E}\bm{B}= \underbrace{\bm{B}^{-1}
    \bm{B}}_{\bm{E}} = \bm{E}
\end{equation*}

Für die Transponierte des Produktes $\bm{A}\bm{B}$ findet man mit
etwas Überlegung:
\begin{equation*}
  (\bm{A}\bm{B})^{\mathrm{T}} = \bm{B}^{\mathrm{T}}\bm{A}^{\mathrm{T}} 
  \label{eq:TranspProdukt}
\end{equation*}

\subsection*{Orthogonale Matrizen}
Eine $n\times n$-Matrix
\begin{equation*}
  \bm{R} = \begin{pmatrix} r_{1,1} & r_{1,2} & \cdots & r_{1,n} \\
    r_{2,1} & r_{2,2} & \cdots & r_{2,n} \\
    \vdots & \vdots & \ddots & \vdots \\ 
    r_{n,1} & r_{n,2} & \cdots & r_{n,n}
  \end{pmatrix}
  = \begin{pmatrix} \bm{r}_{S1} & \bm{r}_{S2} & \cdots \bm{r}_{Sn}
  \end{pmatrix}
\end{equation*}
nennt man orthogonal, wenn alle ihre Spaltenvektoren $\bm{r}_{Si}$
paarweise orthogonal aufeinander stehen und auf $1$ normiert sind, wenn
also $\bm{r}_{Si} \cdot \bm{r}_{Sj} = \delta_{i,j}$ für alle $i,j \in
\{1,2,\dots,n\}$ gilt. Das gilt dann automatisch auch für ihre
Zeilenvektoren. Ausgedrückt über das Matrixprodukt kann man diese
Bedingung sehr einfach schreiben:
\begin{equation*}
  \bm{R} \bm{R}^{\mathrm{T}} = \bm{R}^{\mathrm{T}} \bm{R} = \bm{E}
\end{equation*}
Das heißt also, das Produkt einer orthogonalen Matrix mit ihrer
Transponierten ergibt die Einheitsmatrix. Oder anders ausgedrückt: Die
Transponierte einer orthogonalen Matrix ist ihre Inverse.

Es lässt sich leicht beweisen, dass die Menge aller orthogonalen
$n\times n$-Matrizen eine Gruppe bilden. Man nennt sie die orthogonale
Gruppe $\mathrm{O}(n)$. Ebenfalls kann man einfach berechnen, dass
eine orthogonale Matrix nur die Determinante $+1$ oder $-1$ haben kann. Die
orthogonalen $n\times n$-Matrizen mit der Determinante $+1$ bilden eine
eigene Gruppe, eine Untergruppe von $\mathrm{O}(n)$, die spezielle 
orthogonale Gruppe heißt und mit $\mathrm{SO}(n)$ bezeichnet wird.
  
\subsection*{Drehungen im Raum mit Matrizen aus $\mathrm{SO}(3)$}
Die Matrizen aus $\mathrm{SO}(3)$ sind in der Mechanik besonders
wichtig, weil sie Drehungen beschreiben. Wird ein Vektor $\bm{x}$ auf
eine orthogonale Matrix $\bm{R}$ multipliziert, $\bm{x}' = \bm{R}\bm{x}$,
ändert er nur seine Richtung, nicht seine Länge. Dass $\bm{x}'$ und
$\bm{x}$ den gleichen Betrag haben, lässt sich sehr einfach berechnen.
Dazu gehen wir vom Betragsquadrat aus und schreiben das Skalarprodukt wie
oben als Matrixprodukt:
\begin{equation*}
  \bm{x}' \cdot \bm{x}' =
  (\bm{R}\bm{x})\cdot \bm{R}\bm{x} \overset{\eqref{eq:Skalarprodukt}}{=}
  (\bm{R}\bm{x})^{\mathrm{T}} \bm{R}\bm{x} 
  \overset{\eqref{eq:TranspProdukt}}{=} \bm{x}^{\mathrm{T}} \bm{R}^{\mathrm{T}}
  \bm{R}\bm{x} = \bm{x}^{\mathrm{T}} \bm{x} 
  \overset{\eqref{eq:Skalarprodukt}}{=} \bm{x}\cdot \bm{x}
\end{equation*}

Ein einfaches Beispiel für eine Drehung um den Winkel $\varphi$ um die 
$z$-Achse ist die Matrix
\begin{equation*}
  \bm{R}_z(\varphi) = \begin{pmatrix} \cos(\varphi) & -\sin(\varphi) & 0 \\
    \sin(\varphi) &  \cos(\varphi) & 0 \\ 0 & 0 & 1 \end{pmatrix}
\end{equation*}

Wählt man den Vektor $\bm{a}^{\mathrm{T}} = \begin{pmatrix} 1 & 0 & 0 
\end{pmatrix}$ und $\varphi = \pi/2$, sollte die Matrix den Vektor $\bm{a}$
gerade auf die $y$-Achse drehen. Kommt das so heraus?
\begin{equation*}
  \bm{R}_z(\pi/2) \bm{a} = \begin{pmatrix} 0 & -1 & 0 \\ 1 & 0 & 0 \\
    0 & 0 & 1 \end{pmatrix} \begin{pmatrix} 1 \\ 0 \\ 0 \end{pmatrix}
  = \begin{pmatrix} 0 \\ 1 \\ 0 \end{pmatrix}
\end{equation*}

\subsection*{Schiefsymmetrische $3\times 3$-Matrizen}
Aus der Bedingung $a_{i,j} = -a_{j,i}$ für schiefsymmetrische Matrizen
lässt sich ablesen, dass die Diagonalelemente $a_{i,i}$ verschwinden
müssen, $a_{i,i}= 0$. In drei Dimensionen hat eine schiefsymmetrische
Matrix 
\begin{equation*}
  \bm{\Omega} = \begin{pmatrix} 
    0 & -\omega_3 & \omega_2 \\
    \omega_3 & 0 & -\omega_1 \\
    -\omega_2 & \omega_1 & 0 \\
  \end{pmatrix}
\end{equation*}
also nur drei Komponenten. Die Multiplikation einer solchen
schiefsymmetrischen Matrix $\bm{\Omega}$ mit einem Vektor kann mit Hilfe des 
Vektorprodukts umgeschrieben werden in
\begin{equation*}
  \bm{\Omega} \bm{a} = \bm{\omega} \times \bm{a} \; ,
\end{equation*}
wobei
\begin{equation*}
  \bm{\omega} = 
  \begin{pmatrix} \omega_1 \\ \omega_2 \\ \omega_3 \end{pmatrix} \; .
\end{equation*}

\subsection*{Unitäre Matrizen}
In Ergänzung zu den orthogonalen Matrizen sei noch angemerkt, dass ihre
Entsprechung bei komplexen Matrizen die unitären Matrizen sind. Für 
unitäre Matrizen gilt:
\begin{equation*}
  \bm{U} \bm{U}^{\dagger} = \bm{U}^{\dagger} \bm{U} = \bm{E}
\end{equation*}

\subsection*{Symplektische Matrizen} Wesentlich wichtiger werden in der
kanonischen Mechanik aber -- je nach gewähltem Schwerpunkt -- symplektische
Matrizen werden.

Die reellen symplektischen $(2n\times 2n)$-Matrizen bilden ebenfalls eine
Gruppe. Zu ihr gehören alle Matrizen $\bm{M}$, die
\begin{equation*}
  \bm{M}^{\mathrm{T}}\bm{J} \bm{M} = \bm{J}
\end{equation*}
mit der Matrix
\begin{equation*}
  \bm{J} = \begin{pmatrix} \bm{0}_n & \bm{E}_n \\
    -\bm{E}_n & \bm{0}_n \end{pmatrix}
\end{equation*}
erfüllen. Dabei sind $\bm{0}_n$ und $\bm{E}_n$ die $n\times n$-Null- und
-Einheitsmatrizen. Symplektische Matrizen haben die Determinante $+1$
und die Inverse einer symplektischen Matrix lautet:
\begin{equation*}
  \bm{M}^{-1} = \bm{J}^{-1} \bm{M}^T \bm{J} \qquad
  \text{mit} \qquad \bm{J}^{-1} = \bm{J}^{\mathrm{T}} = -\bm{J}
\end{equation*}

