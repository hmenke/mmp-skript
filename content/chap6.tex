% Holger Cartarius, Henri Menke, 2012 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

\chapter{Vektoren}
\index{Vektoren|(}

Unter einem \emph{Vektor} versteht man in der analytischen Geometrie ein Objekt, das eine Parallelverschiebung im Raum beschreibt.

\section{Notwendigkeit und Darstellung}

Wir kennen physikalische Größen, die nur einen Wert haben, zum Beispiel:
\begin{itemize}
  \item Masse, Energie, Druck, \ldots $\longrightarrow$ Skalare
\end{itemize}
Daneben gibt es solche, die zusätzlich von ihrer Richtung abhängen, zum Beispiel:
\begin{itemize}
  \item Kraft, Geschwindigkeit, \ldots
\end{itemize}
Zur Darstellung verwenden wir einen Pfeil im Raum, dessen Länge den Betrag der physikalischen Größe beschreibt. Die Länge eines Vektors wird \emph{Betrag} genannt.

\begin{figure}[H]
  \centering
  \begin{pspicture}(0,0)(4,4)
    \psline{->}(0,0)(1,2)
    \uput[0](0.75,1){$\color{gdarkgray} \bm{v}_1$}
    \psline{->}(2,0)(4,4)
    \uput[0](3,2){$\color{gdarkgray} \bm{v}_2$}
  \end{pspicture}
\end{figure}

In diesem Bild kann man zwei Rechenoperationen einführen:
\begin{itemize}
\item Addition
\begin{figure}[H]
  \centering
  \begin{pspicture}(0,0)(4,4)
    \psline{->}(0,0)(0,2)
    \psline{->}(0,0)(3,2)
    \psline[linestyle=dotted](0,2)(3,4)
    \psline[linestyle=dotted](3,2)(3,4)
    \psline[linecolor=darkblue]{->}(0,0)(3,4)
    \uput[0](0,1){$\color{gdarkgray} \bm{a}$}
    \uput[0](2,1){$\color{gdarkgray} \bm{b}$}
    \uput[0](2.5,3){$\color{darkblue} \bm{a} + \bm{b}$}
  \end{pspicture}
\end{figure}

\begin{align*}
  \bm{a} + \bm{b} &= \bm{b} + \bm{a} \\
  (\bm{a} + \bm{b}) + \bm{c} &= \bm{a} + (\bm{b} + \bm{c})
\end{align*}

\item Skalare Multiplikation: $\lambda \bm{a}$
\begin{figure}[H]
  \centering
  \begin{pspicture}(0,0)(4,4)
    \psline{->}(0,0)(1,2)
    \psline{->}(1,0)(2,2)
    \psline[linestyle=dotted]{->}(2,2)(3,4)
    \uput[-90](1,1.5){$\color{gdarkgray} \bm{a}$}
    \uput[0](2.7,2.5){$\color{gdarkgray} 2 \bm{a}$}
  \end{pspicture}
\end{figure}

\end{itemize}

Für Vektoren gibt es mehrere Schreibweisen.
\index{Vektoren!Schreibweisen}

\begin{tabular}{rl}
  Pfeil: & $\vec{a}$, $\overset{\rightharpoonup}{a}$ \\ 
  Unterstrich: & $\underline{a}$ \\ 
  Fett: & $\bm{a}$ \\ 
\end{tabular} 

\section{Basis und Koordinatensystem}
\subsection{Basisvektoren}

Im dreidimensionalen können wir jeden Vektor als \emph{Linearkombination} von drei \emph{Basisvektoren} darstellen:
\index{Vektoren!Linearkombination} \index{Vektoren!Basisvektor}

%\Kasten{Bemerkung}{LK := Linearkombination}

\begin{align}
  \bm{a} = a_1\ \vek{e}{1} + a_2\ \vek{e}{2} + a_3\ \vek{e}{3}
  \label{eq:6.1}
\end{align}

Damit das funktioniert, muss gelten:
\begin{itemize}
\item Die Anzahl der Basisvektoren muss der Dimension des Raumes entsprechen.
\item Die Vektoren müssen linear unabhängig sein, d.h.
\begin{align*}
  a_1\ \vek{e}{1} + a_2\ \vek{e}{2} + a_3\ \vek{e}{3} = \bm{0}
\end{align*}
darf nur erfüllt sein für alle $a_i = 0$.

Anschaulich: Es darf nie möglich sein, einen der Basisvektoren durch ein Linearkombination der anderen darzustellen. Er muss in eine Richtung zeigen, die durch die anderen nicht darstellbar ist.
\end{itemize}

In einer gegebenen Basis wird oft die Komponentenschreibweise verwendet:
\begin{align}
  \bm{a} = a_1\ \vek{e}{1} + a_2\ \vek{e}{2} + a_3\ \vek{e}{3} = 
  \left(
    \begin{matrix}
    a_1 \\ 
    a_2 \\ 
    a_3
    \end{matrix}   
  \right)
  \label{eq:6.2}
\end{align}

\Kasten{\color{purple} Achtung}{Basis! Die Komponenten allein lassen noch KEINEN Rückschluss auf die Basisvektoren zu!

Vgl. Kugelkoordinaten:
\begin{align*}
  \left(  
    \begin{matrix}
    r \\ 
    \varphi \\ 
    \theta
    \end{matrix}   
  \right)
\end{align*}
Was sollen hier die Basisvektoren sein?
}

Außerdem sollte man immer daran denken zu \textbf{welcher} Basis die Komponenten ge\-hö\-ren.

Bei \emph{kartesischen} Koordinaten sind die Addition und die skalare Multiplikation in der Komponentenschreibweise wie folgt definiert:
\begin{align}
  \bm{a} + \bm{b} = 
  \left(
    \begin{matrix}
    a_1 \\ 
    a_2 \\ 
    a_3
    \end{matrix}   
  \right)
  +
  \left(
    \begin{matrix}
    b_1 \\ 
    b_2 \\ 
    b_3
    \end{matrix}   
  \right)
  =
  \left(
    \begin{matrix}
    a_1 + b_1 \\ 
    a_2 + b_2 \\ 
    a_3 + b_3
    \end{matrix}   
  \right)
  \label{eq:6.3}
\end{align}

\begin{align}
  \lambda \bm{a} = 
  \lambda  
  \left(
    \begin{matrix}
    a_1 \\ 
    a_2 \\ 
    a_3
    \end{matrix}   
  \right)
  =
  \left(
    \begin{matrix}
    \lambda a_1 \\ 
    \lambda a_2 \\ 
    \lambda a_3
    \end{matrix}   
  \right)
  \label{eq:6.4}
\end{align}

\subsection{Kartesische Koordinaten}
\index{Vektoren!Kartesische Koordinaten}

\begin{figure}[H]
  \centering
  \begin{pspicture}(-2,-2)(4,4)
    %Axis    
    \psline{->}(0,0)(0,4)
    \psline{->}(0,0)(4,0)
    \psline{->}(0,0)(-2,-2)
    %Basevectors
    \psline[linecolor=redorange]{->}(0,0)(0,1)
    \psline[linecolor=redorange]{->}(0,0)(1,0)
    \psline[linecolor=redorange]{->}(0,0)(-0.5,-0.5)
    %dotted
    \psline[linestyle=dotted](3,0)(1.5,-1.5)
    \psline[linestyle=dotted](-1.5,-1.5)(1.5,-1.5)
    \psline[linestyle=dotted](0,0)(1.5,-1.5)
    \psline[linestyle=dotted](1.5,-1.5)(1.5,2)
    \psline[linestyle=dotted](0,3.5)(1.5,2)
    %Vec
    \psline[linecolor=darkblue]{->}(0,0)(1.5,2)
    %Labels
      %Axis   
      \uput[135](-2,-2){$\color{gdarkgray} x$} 
      \uput[90](4,0){$\color{gdarkgray} y$}
      \uput[180](0,4){$\color{gdarkgray} z$}
      %Components
      \uput[135](-1.5,-1.5){$\color{darkblue} a_x$}
      \uput[90](3,0){$\color{darkblue} a_y$}
      \uput[180](0,3.5){$\color{darkblue} a_z$}
      %Basevectors
      \uput[-45](-0.5,-0.5){$\color{redorange} \vek{e}{x}$}
      \uput[-90](0.7,0){$\color{redorange} \vek{e}{y}$}
      \uput[180](0,0.7){$\color{redorange} \vek{e}{z}$}
      %Vec
      \uput[0](1.5,2){$\color{darkblue} \bm{a}$}
  \end{pspicture}
\end{figure}

\emph{Natürliche Basis}: Basisvektoren zeigen entlang der Koordinatenachsen $x$, $y$ und $z$.

\begin{align}
  \bm{a} = a_x\ \vek{e}{x} + a_y\ \vek{e}{y} + a_z\ \vek{e}{z} = 
  \left(
    \begin{matrix}
    a_x \\ 
    a_y \\ 
    a_z
    \end{matrix}   
  \right)
  \label{eq:6.5}
\end{align}

\section{Vektoroperationen}
\index{Vektoren!Vektoroperationen|(}
\subsection{Skalarprodukt}
\index{Vektoren!Vektoroperationen!Skalarprodukt}

In kartesischen Koordinaten definiert man das \emph{Skalarprodukt} als:
\begin{align}
  \bm{a} \cdot \bm{b} &= 
  \left(
    \begin{matrix}
    a_1 \\ 
    a_2 \\ 
    a_3
    \end{matrix}   
  \right)
  \cdot
  \left(
    \begin{matrix}
    b_1 \\ 
    b_2 \\ 
    b_3
    \end{matrix}   
  \right)
  = a_1\ b_1 + a_2\ b_2 + a_3\ b_3
  \label{eq:6.6}
\end{align}

Andere Schreibweise für das Skalarprodukt:
\begin{align*}
  \langle \bm{a}, \bm{b} \rangle = \langle \bm{a} | \bm{b} \rangle = \bm{a} \cdot \bm{b}
\end{align*}

Man sieht damit schnell, dass die Länge eines Vektors, die wir auch Betrag nennen, berechnet werden kann mit:
\begin{align}
  a = |\bm{a}| = \sqrt{\bm{a} \cdot \bm{a}} = \sqrt{a_x^2 + a_y^2 + a_z^2}
  \label{eq:6.7}
\end{align}

Für das Skalarprodukt gilt:
\begin{align}
  \bm{a} \cdot \bm{b} = |\bm{a}| |\bm{b}| \cos(\vartheta)
  \label{eq:6.8}
\end{align}
\begin{figure}[H]
  \centering
  \begin{pspicture}(0,0)(3,2)
    %Vec
    \psline{->}(0,0)(2,2)
    \psline{->}(0,0)(3,0)
    \psarc[linecolor=redorange]{->}{1.2}{0}{45}
    %Labels
    \uput[135](1.5,1.5){$\color{gdarkgray} \bm{a}$}
    \uput[90](3,0){$\color{gdarkgray} \bm{b}$}
    \uput[45](0.6,0){$\color{redorange} \vartheta$}
  \end{pspicture}
\end{figure}

Wegen dieser Eigenschaft nennt man Vektoren mit $\bm{a} \cdot \bm{b} = 0$ \emph{orthogonal}.

Die Basis $\vek{e}{x}$, $\vek{e}{y}$, $\vek{e}{z}$ ist \emph{orthonormiert} \index{Vektoren!Vektoroperationen!orthonormal}, denn die Vektoren erfüllen:
\begin{subequations}
\begin{align}
  \vek{e}{x} \cdot \vek{e}{y} = \vek{e}{x} \cdot \vek{e}{z} = \vek{e}{y} \cdot \vek{e}{z} = 0 && \text{orthogonale Basis}
  \label{eq:6.9a}
\end{align}
\begin{align}
  |\vek{e}{x}| = |\vek{e}{y}| = |\vek{e}{z}| = 1  && \text{normierte Basis}
  \label{eq:6.9b}
\end{align}
\end{subequations}

\textit{Bemerkung:} Die Axiome des Skalarprodukts lauten mit drei Vektoren $\bm{x}$, $\bm{y}$ und $\bm{z}$ und dem Skalar $\alpha$:
%
\begin{align*}
  \langle \bm{x} | \bm{x} \rangle &\geq 0 \; , \quad \text{insbesondere } \langle \bm{x} | \bm{x} \rangle \in \mathbb{R} \\
  \langle \bm{x} | \bm{x} \rangle &= 0 \Leftrightarrow \bm{x} = 0 \\
  \langle \alpha \bm{x} | \bm{y} \rangle &= \overbrace{\overline{\alpha}}^{\mathclap{\text{komplex konjugiert}}} \langle \bm{x} | \bm{y} \rangle \\
  \langle \bm{x} | \alpha \bm{y} \rangle &= \alpha \langle \bm{x} | \bm{y} \rangle \\
  \langle \bm{x} + \bm{y} | \bm{z} \rangle &= \langle \bm{x} | \bm{z} \rangle + \langle \bm{y} | \bm{z} \rangle \\
  \langle \bm{x} | \bm{y} + \bm{z} \rangle &= \langle \bm{x} | \bm{y} \rangle + \langle \bm{x} | \bm{z} \rangle \\
  \overline{\langle \bm{x} | \bm{y} \rangle} &= \langle \bm{y} | \bm{x} \rangle 
\end{align*}
%
In kartesischen Koordinanten lautet das Skalarprodukt für zwei Vektoren $\bm{v}$ und $\bm{w}$ über dem $\mathbb{R}^n$
%
\begin{align*}
  \langle \bm{v} | \bm{w} \rangle &= \sum\limits_{i=1}^{n} v_i \, w_i \; , \quad \bm{v}, \bm{w} \in \mathbb{R}^n \; ,
\end{align*}
%
während über einem komplexen Vektorraum $\mathbb{C}^n$ gilt
%
\begin{align*}
  \langle \bm{v} | \bm{w} \rangle &= \sum\limits_{i=1}^{n} \overline{v_i} \, w_i \; , \quad \bm{v}, \bm{w} \in \mathbb{C}^n \; .
\end{align*}

\subsection{Vektorprodukt} \label{sec:Vektorprodukt}
\index{Vektoren!Vektoroperationen!Kreuzprodukt|see{Vektorprodukt}}
\index{Vektoren!Vektoroperationen!Vektorprodukt}
Mit dem Vektorprodukt
\begin{align}
  \bm{c} = \bm{a} \times \bm{b}
  \label{eq:6.10}
\end{align}
führt man einen Vektor $\bm{c}$ ein, der orthogonal zu $\bm{a}$ und $\bm{b}$ ist und den Betrag
\begin{align}
  |\bm{c}| = |\bm{a}| |\bm{b}| \sin(\vartheta)
  \label{eq:6.11}
\end{align}
hat.

\begin{figure}[H]
  \centering
%  \begin{pspicture}(0,-1)(2,2)
%    %Vec
%    \psline{->}(0,0)(1,-1)
%    \psline{->}(0,0)(1,1)
%    \psline{->}(0,0)(0,2)
%    \psline[linestyle=dotted](1,-1)(2,0)
%    \psline[linestyle=dotted](1,1)(2,0)
%    \psline[linestyle=dotted](0,0)(2,0)
%    \psarc[linecolor=redorange]{-}{0.8}{-45}{45}
%    %Labels
%    \uput[-45](1,-1){$\color{gdarkgray} \bm{a}$}
%    \uput[45](1,1){$\color{gdarkgray} \bm{b}$}
%    \uput[180](0,2){$\color{gdarkgray} \bm{c}$}
%    \uput[0](0.2,0){$\color{redorange} \vartheta$}
%  \end{pspicture}
  \psset{Alpha=80,Beta=30}
  \begin{pspicture}(-1.2,-1.2)(1.3,1.3)
    \pstThreeDLine{->}(0,0,0)(2,0,0)
    \pstThreeDLine{->}(0,0,0)(0,2,0)
    \pstThreeDLine{->}(0,0,0)(0,0,2)
    \pstThreeDLine[linestyle=dotted]{-}(0,0,0)(2,2,0)
    \pstThreeDLine[linestyle=dotted]{-}(2,0,0)(2,2,0)
    \pstThreeDLine[linestyle=dotted]{-}(0,2,0)(2,2,0)
    \pstThreeDNode(1.5,0,0){A}
    \pstThreeDNode(0,1.5,0){B}
    \ncarc[linecolor=redorange,angleA=90]{B}{A}
    \pstThreeDPut(2,-0.3,0){$\color{gdarkgray} \bm{a}$}
    \pstThreeDPut(-0.2,2.2,0){$\color{gdarkgray} \bm{b}$}
    \pstThreeDPut(0,0.3,2){$\color{gdarkgray} \bm{c}$}
    \pstThreeDPut(0.5,0.5,0){$\color{redorange} \vartheta$}
  \end{pspicture}
  \hspace*{-2em}
\end{figure}

Der Betrag $|\bm{c}|$ entspricht dem Flächeninhalt des Parallelogramms, das von $\bm{a}$ und $\bm{b}$ aufgespannt wird.

Die Vektoren $\bm{a}$, $\bm{b}$, $\bm{c}$ bilden in dieser Reihenfolge ein \emph{rechtshändiges Dreibein}. (Rechte-Hand-Regel) \index{Vektoren!Rechte-Hand-Regel}


Beziehungen:
\begin{subequations}
\begin{align}
  (\bm{a} + \bm{b}) \times \bm{c} = \bm{a} \times \bm{c} + \bm{b} \times \bm{c}
  \label{eq:6.12a}
\end{align}
\begin{align}
  \bm{b} \times \bm{a} = -\bm{a} \times \bm{b}
  \label{eq:6.12b}
\end{align}
\end{subequations}

\Kasten{\color{purple} Achtung}{Das Vektorprodukt ist NICHT kommutativ!}

In kartesischen Koordinaten gilt die Komponentendarstellung:
\begin{align}
  \bm{a} \times \bm{b} =
    \left(
    \begin{matrix}
    a_1 \\ 
    a_2 \\ 
    a_3
    \end{matrix}   
  \right)
  \times
  \left(
    \begin{matrix}
    b_1 \\ 
    b_2 \\ 
    b_3
    \end{matrix}   
  \right)
  =
    \left(
    \begin{matrix}
    a_2\ b_3 - a_3\ b_2 \\ 
    a_3\ b_1 - a_1\ b_3 \\ 
    a_1\ b_2 - a_2\ b_1
    \end{matrix}   
  \right)
  =
  \bm{c}
  \label{eq:6.13}
\end{align}

\index{Vektoren!Vektoroperationen|)}

\textbf{Ist das Vektorprodukt in der Physik relevant?}

\begin{itemize}
\item Drehimpuls:
\begin{align*}
  \bm{L} = \underbrace{\bm{r}}_{\mathclap{\text{Ort}}} \times \underbrace{\bm{p}}_{\mathclap{\text{Impuls}}}
\end{align*}

\item Lorentzkraft:
\begin{align*}
  \bm{F} = \overbrace{q}^{\text{Ladung}} (\overbrace{\bm{E}}^{\text{el. Feld}} + \underbrace{\bm{v}}_{\mathclap{\text{Geschwindigkeit der Ladung}}} \times \overbrace{\bm{B}}^{\mathclap{\text{Magnetfeld}}})
\end{align*}
\end{itemize}
\index{Vektoren|)}
