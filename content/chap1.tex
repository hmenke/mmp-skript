% Holger Cartarius, Henri Menke, 2012 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

\chapter{Differential- und Integralrechnung für Funktionen einer Veränderlichen}\label{chap:Kapitel1}

Differential- und Integralrechnung werden unter dem Begriff \emph{Infinitesimalrechnung} zusammengefasst und bilden einen wichtigen Bestandteil der Analysis.

\section{Differentialrechnung}

\subsection{Notwendigkeit und Definition der Ableitung}\label{sec:Notwendigkeit und Definition der Ableitung}
\index{Differentialrechnung|(}
\index{Differentialrechnung!Ableitung}

\subsubsection{Beispiel}
Wir betrachten eine Trajektorie eines Teilchens über einen gewissen Zeitraum. Aus den gesammelten Daten erstellen wir ein Ort-Zeit-Diagramm $x(t)$ des Teilchens. Wie schnell war das Teilchen?

\begin{figure}[H]
  \centering
  \psset{xunit=1.0cm,yunit=1.0cm,algebraic=true}
  \begin{pspicture}(0,0)(5.5,2.5)
    \psaxes[labels=none,labelFontSize=\scriptstyle\color{gdarkgray} ,xAxis=true,yAxis=true,Dx=1,Dy=1,ticksize=-2pt 2pt,subticks=1]{->}(0,0)(0,0)(5.5,2.5)
    \uput[-90](0,0){$\color{gdarkgray} t_0$}
    \uput[-90](1,0){$\color{gdarkgray} t_1$}
    \uput[-90](2,0){$\color{gdarkgray} t_2$}
    \uput[-90](3,0){$\color{gdarkgray} t_3$}
    \uput[-90](4,0){$\color{gdarkgray} t_4$}
    \uput[-90](5,0){$\color{gdarkgray} t_5$}
    %
    \uput[180](0,0){$\color{gdarkgray} x(t_0)$}
    \uput[180](0,1){$\color{gdarkgray} x(t_1)$}
    \uput[180](0,2){$\color{gdarkgray} x(t_5)$}
    %
    \pscurve(0,0)(1,1)(2,0.7)(3,1.5)(4,1.3)(5,2)
    \psline[linewidth=1pt,linecolor=darkblue](5,0)(5,2)
    \psline[linewidth=1pt,linecolor=darkblue](0,0)(5,0)
    \psline[linewidth=1pt,linestyle=dotted](0,2)(5,2)
    \psline[linewidth=1pt,linestyle=dotted](5,0)(5,2)
    \uput[0](5,1){$\color{darkblue} x(t_5)-x(t_0)$}
    %
    \psline[linewidth=1pt,linecolor=orangered](4,1)(4,1.3)
    \psline[linewidth=1pt,linecolor=orangered](1,1)(4,1)
    \psline[linewidth=1pt,linestyle=dotted](4,0)(4,1.3)
    \psline[linewidth=1pt,linestyle=dotted](0,1.3)(4,1.3)
    \uput[90](2.5,1.4){$\color{orangered} x(t_4)-x(t_1)$}
    %
    \psline[linewidth=1pt,linestyle=dotted](1,0)(1,1)(0,1)
    %
    \psline[linewidth=1pt,linestyle=dashed,linecolor=darkgreen](2.5,-0.1)(2.5,1.1)
    \uput[-90](2.5,0){$\color{darkgreen} t^{*}$}
  \end{pspicture}
  \psset{xunit=0.8cm,yunit=0.8cm,algebraic=true}
\end{figure}

\begin{subequations}
Wir ermitteln die Durchschnittsgeschwindigkeit:
%
\begin{align}
  \overline{v}=\frac{x(t_5)-x(t_0)}{t_5-t_0}
  \label{eq:1.1a}
\end{align}
%
Um zu erfahren, wie schnell das Teilchen zur Zeit $t^{*}$ war verkleinern wir die Steigungsdreiecke:
%
\begin{align}
  \overline{v_1}=\frac{x(t_4)-x(t_1)}{t_4-t_1}, \quad \overline{v_2}=\frac{x(t_3)-x(t_2)}{t_3-t_2}
  \label{eq:1.1b}
\end{align}
\end{subequations}
%
Die Gleichungen \eqref{eq:1.1a} und \eqref{eq:1.1b} haben die Form
%
\begin{subequations}
\begin{align}
  \overline{v_t}=\frac{x(t+\Delta t)-x(t)}{\Delta t}
  \label{eq:1.2a}
\end{align}
%
Wollen wir die Momentangeschwindigkeit wissen, müssen wir den (zeitlichen) Abstand zwischen beiden Punkten gegen $0$ gehen lassen.
%
\begin{align}
  v_{t^{*}}=\lim_{\Delta t \rightarrow 0}\frac{x(t^{*}+\Delta t)-x(t^{*})}{\Delta t}
  \label{eq:1.2b}
\end{align}
\end{subequations}

\subsubsection{Definition}
Die \emph{Ableitung} beschreibt, wie sich eine Funktion $f(x)$ in Abhängigkeit von ihrem Argument $x$ ändert. Sie ist über den \emph{Differentialquotient} \index{Differentialrechnung!Differentialquotient}
\begin{align}
  \frac{\dx{f}}{\dx{x}} = \frac{\dx{}}{\dx{x}} f(x) = f'(x)=\lim_{\Delta x \rightarrow 0}\frac{f(x+ \Delta x)-f(x)}{\Delta x}
  \label{eq:1.3}
\end{align}
definiert. Die Funktion $f(x)$ heißt \emph{differenzierbar}, wenn dieser Grenzwert existiert.
%
\begin{figure}[H]
  \centering
  \begin{pspicture}(-0.5,-0.5)(4,4.2)
    \psaxes[labels=none,labelFontSize=\scriptstyle\color{gdarkgray} ,xAxis=true,yAxis=true,ticksize=0 0]{->}(0,0)(-0.5,-0.5)(4,4)[$\color{gdarkgray} x$,-90][$\color{gdarkgray} y$,180]
    \psline[linewidth=1pt](2.5,1)(2.5,3.15)
    \psline[linewidth=1pt](1.414,1)(2.5,1)
    \psline[linewidth=1pt,linestyle=dotted](2,2)(2,0)
    \psplot[linecolor=darkblue]{0}{2.8}{0.5*x^2}
    \psplot[linecolor=orangered]{0.8}{3}{2*x-2}
    \uput[0](2.5,2){$\color{gdarkgray} \Delta f$}
    \uput[-90](2,1){$\color{gdarkgray} \Delta x$}
    \uput[-60](2,0){$\color{gdarkgray} x_0$}
  \end{pspicture}
\end{figure}
%
Als \emph{Tangente} bezeichnen wir eine Gerade, die die Funktion im Punkt $x_0$ berührt und dort die Steigung $f'(x_0)$ hat.

\subsubsection{Höhere Ableitung}
\index{Differentialrechnung!Höhere Ableitung}

Höhere Ableitungen folgen dem gleichen Schema.

Frage: Wie ändert sich $f'(x)$ mit $x$?
\begin{subequations}
  \begin{align}
    \shortintertext{Antwort:}
    \frac{\dx{}^{2}}{\dx{x}^2} f(x)= &\frac{\dx{}}{\dx{x}} \frac{\dx{}}{\dx{x}} f(x)=\frac{\dx{}}{\dx{x}} f'(x) \nonumber\\
    \overset{\eqref{eq:1.3}}{=} &\lim_{\Delta x \rightarrow 0}\frac{f'(x+ \Delta x)-f'(x)}{\Delta x}
    \label{eq:1.4a}
  \end{align}
Sofern die Grenzwerte existieren, lassen sich Ableitungen beliebiger Ordnung $n$ bilden:
  \begin{align}
    \frac{\dx{}^{n}}{\dx{x}^n} f(x) = f^{(n)}(x)\overset{\eqref{eq:1.3}}{\underset{\eqref{eq:1.4a}}{=}}
    \lim_{\Delta x \rightarrow 0}\frac{f^{(n-1)}(x+ \Delta x)-f^{(n-1)}(x)}{\Delta x}
    \label{eq:1.4b}
  \end{align}
\end{subequations}

\subsection{Wichtige Ableitungen}
\index{Differentialrechnung!Wichtige Ableitungen}
\begin{subequations}
\begin{align}
  \frac{\dx{}}{\dx{x}} x^n &= n x^{n-1} \label{eq:1.5a} \\
  \frac{\dx{}}{\dx{x}} e^{ax} &= a e^{ax} \label{eq:1.5b} \\
  \frac{\dx{}}{\dx{x}} \ln(ax) &= \frac{1}{x} \label{eq:1.5c} \\
  \frac{\dx{}}{\dx{x}} \sin(ax) &= a\ \cos(ax) \label{eq:1.5d} \\
  \frac{\dx{}}{\dx{x}} \cos(ax) &= -a\ \sin(ax) \label{eq:1.5e}
\end{align}
\end{subequations}

\textbf{Beispiel} für eine Funktion, die nicht überall differenzierbar ist: $f(x)=|x|$\\
Betrachte $x=0$:
\begin{align*}
  \lim_{\Delta x \rightarrow 0}\frac{f(\Delta x)-f(0)}{\Delta x} &= \lim_{\Delta x \rightarrow 0}\frac{\Delta x-0}{\Delta x}=1 \\
  \lim_{\Delta x \rightarrow 0}\frac{f(-\Delta x)-f(0)}{-\Delta x} &= \lim_{\Delta x \rightarrow 0}\frac{|-\Delta x|-0}{-\Delta x}=-1
\end{align*}
%
Das heißt, der linksseitige und rechtsseitige Grenzwert sind verschieden und somit existiert die Ableitung nicht im Punkt $x=0$. Im Schaubild drückt sich das durch einen Knick aus.
%
\begin{figure}[H]
  \centering
  \begin{pspicture}(-2.2,-0.5)(2.2,2)
    \psaxes[labels=none,labelFontSize=\scriptstyle\color{gdarkgray},xAxis=true,yAxis=true,Dx=1,Dy=1,ticks=none]{->}(0,0)(-2.2,-0.5)(2.2,2)[$\color{gdarkgray} x$,-90][$\color{gdarkgray} y$,180]
    \psplot[linewidth=1pt,linecolor=darkblue]{-2}{2}{abs(x)}
    \psline(-1.5,1.5)(-1.5,0)
    \uput[90](-1,0){\color{gdarkgray} \small $-\Delta x$}
    \psline(1.5,1.5)(1.5,0)
    \uput[90](1,0){\color{gdarkgray} \small $\Delta x$}
  \end{pspicture}
\end{figure}

\subsection{Differentiationsregeln}
\subsubsection{Produkte von Funktionen}
\index{Differentialrechnung!Ableitungsregeln!Produktregel}
Betrachte $f(x)=u(x)\ v(x)$:
\begin{align}
  &\frac{\dx{f}}{\dx{x}} = \frac{\dx{}}{\dx{x}} f(x) \overset{\eqref{eq:1.3}}{=} \lim_{\Delta x \rightarrow 0}\frac{f(x + \Delta x)-f(x)}{\Delta x} \nonumber\\
  &= \lim_{\Delta x \rightarrow 0}\frac{u(x+\Delta x)\ v(x+\Delta x) - u(x)\ v(x)}{\Delta x} \nonumber\\
  &= \lim_{\Delta x \rightarrow 0}\frac{u(x+\Delta x)\ v(x+\Delta x) {\color{purple} -u(x+\Delta x)\ v(x)+u(x+\Delta x)\ v(x)} - u(x)\ v(x)}{\Delta x} \nonumber\\
  &= \lim_{\Delta x \rightarrow 0} \Bigg( u(x+\Delta x) \underbrace{\frac{v(x+\Delta x)-v(x)}{\Delta x}}_{=v'(x)} + \underbrace{\frac{u(x+\Delta x)-u(x)}{\Delta x}}_{=u'(x)} v(x) \Bigg) \nonumber\\
  &= u(x)\ v'(x) + u'(x)\ v(x)
  \label{eq:1.6}
\end{align}

\subsubsection{Verkettete Funktionen}
\index{Differentialrechnung!Ableitungsregeln!Kettenregel}
Betrachte $f(g(x))$, führe ein:  \begin{subequations}\begin{equation} \Delta g = g(x+\Delta x)-g(x) \label{eq:1.7a} \end{equation}\end{subequations}
\begin{align}
  \frac{\dx{f}}{\dx{x}} &\overset{\eqref{eq:1.3}}{=} \lim_{\Delta x \rightarrow 0}\frac{f(g(x + \Delta x))-f(g(x))}{\Delta x} \nonumber \\
  &\overset{\eqref{eq:1.7a}}{=} \lim_{\Delta x \rightarrow 0} \left(\frac{f(g+ \Delta g)-f(g)}{\Delta x} \frac{\Delta g}{\Delta g} \right) \nonumber \\
  &= \lim_{\substack{\Delta x \rightarrow 0 \\ \Delta g \rightarrow 0}} \left(\frac{f(g+ \Delta g)-f(g)}{\Delta g} \frac{g(x+ \Delta x)-g(x)}{\Delta x} \right) \nonumber \\
  &= \lim_{\Delta g \rightarrow 0} \frac{f(g+ \Delta g)-f(g)}{\Delta g} \lim_{\Delta x \rightarrow 0} \frac{g(x+ \Delta x)-g(x)}{\Delta x} \nonumber \\
  &= \frac{\dx{f(g(x))}}{\dx{g}} \frac{\dx{g(x)}}{\dx{x}} 
  \label{eq:1.8}
\end{align}
\textbf{Beispiele:}
\begin{align*}
  f(x) &= x\ \sin(x) \\
  f'(x) &= \sin(x) +x\ \cos(x) \\
  g(x) &= e^{-x^2} \\
  g'(x) &= -2\, x e^{-x^2}
\end{align*}

\subsubsection{Quotienten von Funktionen}
\index{Differentialrechnung!Ableitungsregeln!Quotientenregel}Quotientenregel:
\begin{align}
  \frac{\dx{}}{\dx{x}} \frac{f(x)}{g(x)} & \overset{\eqref{eq:1.6}}{=} \frac{f'(x)}{g(x)}+f(x)\left(\frac{1}{g(x)}\right)' \nonumber \\
  &\overset{\eqref{eq:1.8}}{=} \frac{f'(x)}{g(x)}-f(x)\frac{1}{g(x)^2}g'(x) \nonumber\\
  &= \frac{f'(x)\ g(x) - f(x)\ g'(x)}{g(x)^2}
  \label{eq:1.9}
\end{align}

\subsubsection{Umkehrfunktion}
\index{Differentialrechnung!Umkehrfunktion}
Sei $y=f(x)$. Dann lautet die Umkehrfunktion: $x=f^{-1}(y)$.
\begin{align}
  \frac{\dx{}}{\dx{y}} f^{-1}(y) = \lim_{\Delta y \rightarrow 0} \frac{\Delta x}{\Delta y}=\frac{1}{\underset{\Delta x \rightarrow 0}{\lim} \frac{\Delta y}{\Delta x}} = \frac{1}{f'(f^{-1}(y))}
  \label{eq:1.10}
\end{align}

\Kasten{Zusammenfassung}{
\[
  \begin{array}{rrcl}
    \text{Produktregel:} & \frac{\dx{}}{\dx{x}} u(x) v(x) & = & u'(x) v(x) + u(x) v'(x) \\ 
  \noalign{\smallskip}
    \text{Kettenregel:} & \frac{\dx{}}{\dx{x}} u(v(x)) & = & u'(v(x)) v'(x) \\ 
  \noalign{\smallskip}
    \text{Quotientenregel:} & \frac{\dx{}}{\dx{x}} \frac{u(x)}{v(x)} & = & \frac{u'(x) v(x) - u(x) v'(x)}{v(x)^2}
  \end{array}
\]
}

\index{Differentialrechnung|)}

\section{Integralrechnung}

Die \emph{Integration} ist die Umkehrung der Differentiation.

\subsection{Motivation und eine mögliche Definition}
\index{Integration|(}
In Anlehnung an \autoref{sec:Notwendigkeit und Definition der Ableitung}: Wieder betrachten wir die Trajektorie eines Teilchens über einen gewissen Zeitraum. Wir kennen dieses Mal die Geschwindigkeit $v(t)$. Welchen Weg hat das Teilchen aber zurückgelegt?

Bei der Druchschnittsgeschwindigkeit ist das einfach:
\begin{align*}
  \Delta s \overset{\eqref{eq:1.2a}}{=} \overline{v} \Delta t
\end{align*}

Was müssen wir betrachten, wenn sich die Geschwindigkeit in Stücken ändert?

Wir erhalten nur eine stückweise konstante Geschwindigkeit:
%
\begin{align*}
  \Delta s = v_1 \Delta t_1 + v_2 \Delta t_2 + v_3 \Delta t_3 = \sum\limits_{i} v_i \Delta t_i
\end{align*}
%
\begin{figure}[H]
  \centering
  \begin{pspicture}(-0.5,-0.5)(8,4.2)
    \psaxes[labels=none,labelFontSize=\scriptstyle\color{gdarkgray} ,xAxis=true,yAxis=true,ticksize=0 0]{->}(0,0)(-0.5,-0.5)(8,4)[$\color{gdarkgray} t$,-90][$\color{gdarkgray} v(t)$,180]
    \psline(0,2)(2,2)
    \psline[linestyle=dotted](2,2)(2,0)
    \psline(2,1)(4,1)
    \psline[linestyle=dotted](4,3)(4,0)
    \psline(4,3)(7,3)
    \psline[linestyle=dotted](7,3)(7,0)
    \psline(-0.1,1)(0.1,1)
    \psline(-0.1,2)(0.1,2)
    \psline(-0.1,3)(0.1,3)
    \uput[180](0,2){$\color{gdarkgray} v_1$}
    \uput[180](0,1){$\color{gdarkgray} v_2$}
    \uput[180](0,3){$\color{gdarkgray} v_3$}
    \uput[-90](1,0){$\color{gdarkgray} \Delta t_1$}
    \uput[-90](3,0){$\color{gdarkgray} \Delta t_2$}
    \uput[-90](5.5,0){$\color{gdarkgray} \Delta t_3$}
  \end{pspicture}
\end{figure}
%
Ändert sich $v$ ständig, muss $\Delta t$ gegen Null gehen. $\Delta t \rightarrow \dx{t}$ \index{Integration!Infinitesimales Element}

Das führt uns auf das \emph{infinitesimale Wegelement} $\dx{s}= v(t) \dx{t}$.

Dies wiederum führt auf die Integration:
\begin{align}
  \Delta s &= \int\limits_{t_0}^{t_1} v(t) \dx{t} \label{eq:1.11}
\end{align}

Das Integral
\begin{align}
  I &= \int\limits_{a}^{b} f(x) \dx{x} = \int\limits_{a}^{b} \dx{x}\ f(x) \label{eq:1.12}
\end{align}
ist über einen Grenzwertprozess definiert.

Betrachte dazu:

\begin{figure}[H]
  \centering
  \begin{pspicture}(-0.5,-0.5)(6,4)
    \psaxes[labels=none,labelFontSize=\scriptstyle\color{gdarkgray} ,xAxis=true,yAxis=true,Dx=1,Dy=1,ticksize=0 0,subticks=1]{->}(0,0)(-0.5,-0.5)(6,4)[$\color{gdarkgray} x$,-90][$\color{gdarkgray} f(x)$,180]
    %
    \pscurve(1,2)(2,2.7)(3,2.9)(4,3.1)(5,3.8)
    \psline[linestyle=dotted](1,2)(1,0)
    \psline[linestyle=dotted](2,2.7)(2,0)
    \psline[linestyle=dotted](3,2.9)(3,0)
    \psline[linestyle=dotted](4,3.1)(4,0)
    \psline[linestyle=dotted](5,3.8)(5,0)
    \uput[-90](1,0){$\color{gdarkgray} \xi_1$}
    \uput[-90](2,0){$\color{gdarkgray} \xi_2$}
    \uput[-90](3,0){$\color{gdarkgray} \xi_3$}
    \uput[-90](4,0){$\color{gdarkgray} \xi_4$}
    \uput[-90](5,0){$\color{gdarkgray} \xi_5$}
  \end{pspicture}
\end{figure}

Das Intervall $a \leq x \leq b$ wird in $n$ kleine Intervalle $a<\xi_1<\xi_2<\dots<\xi_n<b$ mit Funktionswerten $f(x_i)$ aufgeteilt, wobei $\xi_{i-1}<x_i<\xi_i$.

Mit diesen Intervallen kann eine Summe geformt werden:
\index{Integration!Riemann-Summe}
\index{Integration!Integral!bestimmtes}
\begin{align}
  S = \sum\limits_{i=1}^{n} f(x_i) (\xi_i - \xi_{i-1}) \label{eq:1.13}
\end{align}
\index{Integration!Riemann'sches Integral}

Lässt man die Teilintervalle gegen Null gehen (und $n \rightarrow \infty$), erhält man das \emph{Riemann'sche Integral}:
\begin{align}
  \int\limits_{a}^{b} f(x) \dx{x}= \lim_{n \rightarrow \infty} \sum\limits_{i=1}^{n} f(x_i) \Delta x \; , \quad \text{mit } \Delta x = \frac{b-a}{n} \label{eq:1.14}
\end{align}

Eine Funktion $f(x)$ heißt \emph{integrierbar}, wenn dieser Grenzwert existiert und eindeutig ist (unabhängig von der Wahl der $x_i$ und $\xi_i$).

Bekannte Interpretation aus der Schule: Fläche unter der Kurve:
\begin{figure}[H]
  \centering
  \begin{pspicture}(-0.5,-0.5)(6,4.2)
    \psaxes[labels=none,labelFontSize=\scriptstyle\color{gdarkgray} ,xAxis=true,yAxis=true,Dx=1,Dy=1,ticksize=0 0,subticks=1]{->}(0,0)(-0.5,-0.5)(6,4)[$\color{gdarkgray} x$,-90][$\color{gdarkgray} f(x)$,180]
    \pscurve(0,2)(1,2.7)(2,2.9)(3,3.1)(4,3.8)
    \pspolygon[linecolor=darkblue](0,0)(0,2.4)(1,2.4)(1,0)
    \psline[linecolor=darkblue,linestyle=dotted](0.5,0)(0.5,2.4)
    \pspolygon[linecolor=darkblue](1,0)(1,2.85)(2,2.85)(2,0)
    \psline[linecolor=darkblue,linestyle=dotted](1.5,0)(1.5,2.85)
    \pspolygon[linecolor=darkblue](2,0)(2,3)(3,3)(3,0)
    \psline[linecolor=darkblue,linestyle=dotted](2.5,0)(2.5,3)
    \pspolygon[linecolor=darkblue](3,0)(3,3.4)(4,3.4)(4,0)
    \psline[linecolor=darkblue,linestyle=dotted](3.5,0)(3.5,3.4)
    \uput[-90](0.5,0){$\color{darkblue} \Delta x$}
    \uput[-90](1.5,0){$\color{darkblue} \Delta x$}
    \uput[-90](2.5,-0.2){\color{gdarkgray} \ldots}
  \end{pspicture}
\end{figure}

\index{Integration!Integral!bestimmtes}

Eigenschaften des bestimmten Integrals:
\begin{subequations}
\begin{align}
  \int\limits_{a}^{b} 0 \dx{x} &= 0 \\
  \int\limits_{a}^{a} f(x) \dx{x} &= 0 \\
  \int\limits_{b}^{a} f(x) \dx{x} &= -\int\limits_{a}^{b} f(x) \dx{x} \\
  \int\limits_{a}^{c} f(x) \dx{x} &= \int\limits_{a}^{b} f(x) \dx{x} + \int\limits_{b}^{c} f(x) \dx{x} \label{eq:1.15d} \\
  \int\limits_{a}^{b} (a f(x) + b g(x)) \dx{x} &= a \int\limits_{a}^{b} f(x) \dx{x} + b \int\limits_{a}^{b} g(x) \dx{x}
\end{align}
\end{subequations}

\subsection{Die Stammfunktion}
\subsubsection{Definition}
Betrachte die Funktion $\displaystyle F(x) = \int\limits_{a}^{x} f(u) \dx{u}$ und deren Ableitung.

Betrachte dazu zuerst:
%
\begin{subequations}
\begin{align}
  F(x+\Delta x) = \int\limits_{a}^{x+\Delta x} f(u) \dx{u} &\overset{\eqref{eq:1.15d}}{=} \int\limits_{a}^{x} f(u) \dx{u} + \int\limits_{x}^{x+\Delta x} f(u) \dx{u} \nonumber\\
  &= F(x) + \int\limits_{x}^{x+\Delta x} f(u) \dx{u} \label{eq:1.16a}
\end{align}
%
Daraus folgt dann:
%
\begin{align}
  F(x+\Delta x) -F(x) \overset{\eqref{eq:1.16a}}{=} \int\limits_{x}^{x+\Delta x} f(u) \dx{u} \label{eq:1.16b}
\end{align}
\end{subequations}
%
und die Ableitung lautet:
%
\begin{align}
  \frac{\dx{}}{\dx{x}} F(x) &= \lim_{\Delta x \rightarrow 0} \frac{F(x+\Delta x) -F(x)}{\Delta x} \nonumber\\
  &\overset{\eqref{eq:1.16b}}{=} \lim_{\Delta x \rightarrow 0}  \frac{\int\limits_{x}^{x+\Delta x} f(u) \dx{u}}{\Delta x} \nonumber\\
  &\overset{\eqref{eq:1.14}}{=} \lim_{\Delta x \rightarrow 0}  \frac{f(x) \Delta x}{\Delta x} = f(x) \label{eq:1.17}
\end{align}

Mit diesem Ergebnis definiert man die \emph{Stammfunktion} \index{Integration!Stammfunktion} oder das \emph{unbestimmte Integral}\index{Integration!Integral!unbestimmtes}:

\begin{align}
  F(x) = \int f(x) \dx{x} \label{eq:1.18}
\end{align}

$F(x)$ ist immer dann Stammfunktion von $f(x)$, wenn gilt $\frac{\dx{F}(x)}{\dx{x}} = f(x)$. Die Funktion $f(x)$ hat unendlich viele Stammfunktionen, denn wenn $F(x)$ Stammfunktion ist, dann auch $G(x)=F(x)+c$.

\subsubsection{Beziehung zum bestimmten Integral}
\index{Integration!Integral!bestimmtes}
Notation:
\begin{align*}
  \int\limits_{a}^{b} f(x) \dx{x} = F(b) - F(a) = [F(x)]_{a}^{b} = F(x)|_{a}^{b}
\end{align*}

\subsubsection{Wichtige Stammfunktionen}
\index{Integration!Wichtige Integrale}
\stepcounter{equation} % Fehler im Skript
\begin{subequations}
\begin{align}
  \int a \dx{x} &= ax +c \\
  \int a x^n \dx{x} &= \frac{a}{n+1} x^{n+1} +c \\
  \int e^{ax} \dx{x} &= \frac{1}{a} e^{ax} +c \\
  \int \frac{a}{x} \dx{x} &= a \ln(x) +c \\
  \int a \cos(bx) \dx{x} &= \frac{a}{b} \sin(bx) +c \\
  \int a \sin(bx) \dx{x} &= -\frac{a}{b} \cos(bx) +c
\end{align}
\end{subequations}

\subsection{Integrationsregeln}

Integrale können schnell kompliziert werden. Die beiden folgenden Regeln können helfen, das Integral zu vereinfachen um es so besser zu berechnen.

\index{Integration!Integrationsregeln}
\subsubsection{Substitution}
\minisec{\bfseries Beispiel 1:} $\displaystyle I=\int \frac{1}{\sqrt{1-x^2}} \dx{x} $ wird substituiert durch die Regel $y = \arcsin(x)$, $x = \sin(y)$.

Wir wissen, dass $\frac{\dx{x}}{\dx{y}} = \cos(y)$, also $\dx{x}=\cos(y) \dx{y}$
\begin{align}
  I=\int \frac{\cos(y) \dx{y}}{\sqrt{1-\sin^2(y)}} = \int \dx{y} = y+c = \arcsin(x)+c \label{eq:1.21}
\end{align}

\minisec{\bfseries Beispiel 2:}

Wichtig ist, die Grenzen auch zu substituieren.
%
\begin{align*}
  I &= \int\limits_{x_0}^{x_1} x \exp(a x^2) \dx{x} \\
  &\left| \ x^2 = u \Rightarrow x = \sqrt{u} ,\qquad \frac{\dx{u}}{\dx{x}} = 2x \leadsto \dx{x} = \frac{\dx{u}}{2x} = \frac{\dx{u}}{2 \sqrt{u}} \right.\\
  &= \int\limits_{x_0^2}^{x_1^2} \sqrt{u} \exp(a u) \frac{\dx{u}}{2 \sqrt{u}} \\
  &= \frac{1}{2} \int\limits_{x_0^2}^{x_1^2} \exp(a u) \dx{u} \\
  &= \left. \frac{1}{2 a} \exp(a u) \right|_{x_0^2}^{x_1^2} \\
  &= \left. \frac{1}{2 a} \exp(a x^2) \right|_{x_0}^{x_1}
\end{align*}

Formale Regel der Integration durch \emph{Substitution}:
\index{Integration!Integrationsregeln!Substitutionsregel}
\begin{align}
  \int\limits_{a}^{b} \dx{y}\ f(y) \underset{y=g(x)}{=} \int\limits_{g^{-1}(a)}^{g^{-1}(b)} \dx{x}\ g'(x) f(g(x)) \label{eq:1.22}
\end{align}

\subsubsection{Partielle Integration}
\index{Integration!Integrationsregeln!Partielle Integration}
Formale Regel der \emph{partiellen Integration}:
\begin{align}
  \int\limits_{a}^{b} f'(x) g(x) \dx{x} = f(x)g(x)|_{a}^{b} - \int\limits_{a}^{b} f(x) g'(x) \dx{x} \label{eq:1.23}
\end{align}

Herleitung:
\begin{align*}
  f(x)g(x)&=\int (f(x) g(x))' \dx{x} \\
  &=\int (f'(x) g(x) + f(x)g'(x)) \dx{x} \\
  &=\int f'(x) g(x) \dx{x} + \int f(x)g'(x) \dx{x} && \left| -\int f(x)g'(x) \dx{x} \right. \\
  \int f'(x)g(x) \dx{x} &= f(x)g(x) - \int f(x)g'(x) \dx{x}
\end{align*}

\textbf{Beispiele:}
\begin{align*}
  \int \ln x \dx{x} = \int \underset{\color{gray} f'(x)}{1} \underset{\color{gray} g(x)}{\ln x} &= \underset{\color{gray} f(x)}{x} \underset{\color{gray} g(x)}{\ln x} - \int \underset{\color{gray} f(x)}{x} \underset{\color{gray} g'(x)}{\frac{1}{x}} \dx{x} \\
  &= x \ln x - x + c
\end{align*}

\begin{align*}
  \int \underset{\color{gray} f(x)}{x} \underset{\color{gray} g'(x)}{\sin x} \dx{x} &= \underset{\color{gray} f(x)}{x} \underset{\color{gray} g(x)}{(-\cos x)} + \int \underset{\color{gray} f'(x)}{1} \underset{\color{gray} g(x)}{\cos x} \dx{x} \\
  &= -x \cos x + \sin x + c
\end{align*}

\index{Integration|)}
