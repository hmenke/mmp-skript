% % % % % % % % % % % % % % % % % %
% % % DO NOT EDIT THIS FILE % % % %
% % % % % % % % % % % % % % % % % %

\NeedsTeXFormat{LaTeX2e}

\ProvidesClass{mmp-script}

\LoadClassWithOptions{scrbook}

% % %
% % % KOMA Options
% % %

\KOMAoption{open}{right}
\KOMAoption{twoside}{true}
\KOMAoption{toc}{index,listof}
\KOMAoption{listof}{totoc}
\KOMAoption{index}{totoc}
\KOMAoption{numbers}{noenddot}
\KOMAoption{parskip}{half}
\KOMAoption{bibliography}{totoc}

% % %
% % % Common
% % %

\RequirePackage[l2tabu, orthodox]{nag}
\RequirePackage[ngerman]{babel}
\RequirePackage[utf8]{luainputenc}
\RequirePackage[T1]{fontenc}
\RequirePackage{scrhack}
\RequirePackage{geometry}

% % %
% % % Mathematics
% % %

\RequirePackage{mathtools}
\RequirePackage{amsfonts,amssymb}
\RequirePackage{nicefrac}
\RequirePackage{mathrsfs}
\RequirePackage{dsfont}
\RequirePackage{braket}
\allowdisplaybreaks[3]

% % %
% % % Color
% % %

\RequirePackage[dvipsnames,svgnames,x11names]{xcolor}
\definecolor{darkblue}{rgb}{0.09,0.20,0.43}
\definecolor{DarkBlue}{rgb}{0.09,0.20,0.43}
\definecolor{darkgreen}{rgb}{0,0.5,0}
\definecolor{lightgray}{rgb}{0.9,0.9,0.9}
\definecolor{proofgray}{rgb}{0.3,0.3,0.3}
\definecolor{framecolor}{rgb}{0,0,0}
\definecolor{gdarkgray}{rgb}{0.39,0.39,0.40}
\definecolor{glightgray}{rgb}{0.93,0.93,0.93}
\definecolor{glightbrown}{rgb}{0.93,0.93,0.93}
\definecolor{purple}{rgb}{0.55,.16,0.44}
\definecolor{deeppurple}{rgb}{0.55,.16,0.44}
\definecolor{yellow}{rgb}{0.78,.51,0.13}
\definecolor{htmlyellow}{rgb}{1,1,0}
\definecolor{linkred}{rgb}{0.545,0.275,0.29}

\definecolor{maroon}{cmyk}{0,0.492,0.41,0.522}
\definecolor{yelloworange}{cmyk}{0,0.139,0.437,0.067}
%\definecolor{orangered}{cmyk}{0,0.391,0.707,0.118}
\definecolor{orangered}{rgb}{0.78,.51,0.13}
\definecolor{redorange}{cmyk}{0,0.524,0.714,0.275}
\definecolor{blackcurrant}{cmyk}{0.076,0.242,0,0.741}
\definecolor{grape}{cmyk}{0.195,0.659,0,0.678}
\definecolor{deeppink}{cmyk}{0.065,0.642,0,0.518}
\definecolor{pink}{cmyk}{0.055,0.164,0,0.353}
\definecolor{lightpink}{cmyk}{0.045,0.121,0,0.224}

% % %
% % % Index and Table of contents
% % %

\RequirePackage{makeidx}

\RequirePackage{tocstyle}
\usetocstyle{nopagecolumn}
\settocstylefeature{pagenumberhook}{\normalfont \color{gray}}

% % %
% % % hyperref
% % %

\RequirePackage[]{hyperref}
\hypersetup{
  unicode,
  breaklinks=true,
  colorlinks=false,
  pdfborder={0 0 0},
  bookmarksdepth=3,
  pdfdisplaydoctitle = true,
  bookmarksnumbered=true,
  unicode=true
}
\usepackage{breakurl}

\def\figureautorefname{Abbildung}
\def\sectionautorefname{Abschnitt}
\def\subsectionautorefname{Abschnitt}
\def\subsubsectionautorefname{Unterabschnitt}

\numberwithin{equation}{chapter}
\setcounter{secnumdepth}{3}
\setcounter{tocdepth}{1}

% % %
% % % Headers
% % %

\RequirePackage{scrpage2}

\setkomafont{pageheadfoot}{\normalfont \small \scshape \color{gray}}
\setkomafont{pagenumber}{\normalfont \color{gray}}
\renewcommand{\chaptermark}[1]{\markboth{\llap{\thechapter ~ $\Bigg|$} ~ #1}{#1 ~ \rlap{$\Bigg|$ ~ \thechapter}}}
\renewcommand{\sectionmark}[1]{\markleft{\llap{\thesection ~ $\Bigg|$} ~ #1}}
\clearscrheadfoot
\rohead[]{\rightmark}
\lehead[]{\leftmark}
\rofoot[\rlap{$\Bigg|$ ~ \pagemark}]{\rlap{$\Bigg|$ ~ \pagemark}}
\lefoot[\llap{\pagemark ~ $\Bigg|$}]{\llap{\pagemark ~ $\Bigg|$}}
\pagestyle{scrheadings}
\renewcommand{\chapterpagestyle}{scrheadings}
\renewcommand{\indexpagestyle}{scrheadings}

% % %
% % % Graphic
% % %

\RequirePackage{etex}
\RequirePackage{pstricks-add}
\RequirePackage{pst-3dplot}
\RequirePackage{pst-bezier}
\RequirePackage{pst-func}
\RequirePackage{pst-all}
\psset{linecolor=DimGray,tickcolor=DimGray,fillcolor=DimGray}
\RequirePackage{tikz}
\RequirePackage{tikz-3dplot}
\tikzset{>=stealth}
\RequirePackage{graphicx}
\RequirePackage{float}

\RequirePackage[figurewithin=none]{caption}
\DeclareCaptionFont{gray-bold}{\small\bfseries \boldmath\color{gray}}
\DeclareCaptionFont{gray-light}{\small\color{gray}}
\DeclareCaptionLabelFormat{number-only}{#2}
\DeclareCaptionFormat{llap}{\llap{#1#2} #3\par}
\captionsetup{format=llap,labelformat=number-only,labelsep=quad,singlelinecheck=no,labelfont=gray-light,textfont=gray-bold}

% % %
% % % KOMA Fonts
% % %

\renewcommand*{\chapterformat}{\Huge \thechapter\enskip}
\renewcommand{\chapterheadstartvskip}{\vspace*{-2em}}
\addtokomafont{chapter}{\color{MidnightBlue} \bfseries \boldmath \rmfamily}
\addtokomafont{section}{\bfseries \boldmath \rmfamily}
\addtokomafont{subsection}{\bfseries \boldmath \rmfamily}
\addtokomafont{subsubsection}{\bfseries \boldmath \rmfamily}
\setkomafont{chapterentry}{\color{MidnightBlue} \bfseries \boldmath \rmfamily}
\setkomafont{descriptionlabel}{\bfseries \boldmath \rmfamily}
\setkomafont{minisec}{\bfseries \boldmath \rmfamily}
\setkomafont{paragraph}{\bfseries \boldmath \rmfamily}

